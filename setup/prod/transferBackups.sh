#!/bin/bash
set -e

# SHELL=/bin/bash
# 15 0 * * 0 /home/gitlab/transferBackups.sh >> /home/gitlab/jobTransferBackups.log 2>&1
product=scribe
export $(grep -v '^#' /home/gitlab/.secrets/${product}.prod.env | xargs -d '\n')

source_folder="/home/gitlab/backups/${product}"
latest_backup_name=$(ls ${source_folder} -Art | tail -n 1)
latest_backup_path="${source_folder}/${latest_backup_name}"

if [ ! -f "${latest_backup_path}" ]; then
  echo "No backups found!"
  exit 1
fi

unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY
unset AWS_SESSION_TOKEN

temp_role=$(
  aws sts assume-role \
  --role-arn "arn:aws:iam::${AWS_ACCOUNT_ID}:role/glacier-backup-backups" \
  --role-session-name "${product}-backup-session"
)

export AWS_ACCESS_KEY_ID=$(echo "${temp_role}" | jq -r .Credentials.AccessKeyId)
export AWS_SECRET_ACCESS_KEY=$(echo "${temp_role}" | jq -r .Credentials.SecretAccessKey)
export AWS_SESSION_TOKEN=$(echo "${temp_role}" | jq -r .Credentials.SessionToken)
export REGION=$(aws configure get region)

aws s3 cp ${latest_backup_path} s3://backups-${AWS_ACCOUNT_ID}-${REGION}/ --storage-class=DEEP_ARCHIVE

find ${source_folder}/*.sql.gz -mtime +1 -exec rm {} \;
