#! /bin/bash

set -e

sudo vim /home/gitlab/.secrets/scribe.prod.env
sudo chown gitlab:gitlab /home/gitlab/.secrets/scribe.prod.env
sudo chmod 600 /home/gitlab/.secrets/scribe.prod.env

sudo cp /home/gitlab/mordhau-scribe/setup/prod/scribe.service /etc/systemd/system/scribe.service
sudo systemctl daemon-reload
sudo systemctl enable scribe
sudo systemctl start scribe
