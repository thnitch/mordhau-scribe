export $(grep -v '^#' ~/.secrets/scribe.prod.env | xargs -d '\n')
/usr/bin/docker compose -f docker-compose.yml -f docker-compose.prod.yml --env-file setup/prod/.env up
