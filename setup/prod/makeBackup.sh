#!/bin/bash
set -e

# SHELL=/bin/bash
# 0 * * * * /home/gitlab/makeBackup.sh >> /home/gitlab/jobMakeBackup.log 2>&1
product='scribe'
export $(grep -v '^#' /home/gitlab/.secrets/${product}.prod.env | xargs -d '\n')

# Database credentials
user='backupuser'
password=${BACKUP_USER_PASSWORD}

# Other options
backup_path="/home/gitlab/backups/${product}"
mkdir -p "${backup_path}"
prefix=''
# file suffix will have
# format: _DAY_MONTH_YEAR__HOUR.sql.gz
suffix='_'$(date +%d'_'%m'_'%Y'__'%H':'%M).sql.gz

# show all databases and omitting information_schema and performance_schema
# dbs=`/usr/bin/mysql -u$user -p$password -Bse'show databases' \ | egrep -vi 'information_schema|performance_schema'`
declare -a dbs=("${product}")
# Set default file permissions
umask 177

# Dump database into SQL file
for DATABASE in $dbs; do
  if [ "${DATABASE}" != 'Database' ]; then
    FILENAME=$prefix$DATABASE$suffix
    # dump and gzip databases
    docker exec ${product}-db /opt/bitnami/mariadb/bin/mariadb-dump --single-transaction -u$user -p$password --databases $DATABASE |
      /bin/gzip |
      openssl enc -aes-256-cbc -md sha512 -pbkdf2 -iter 200000 -salt -k ${BACKUP_PASSWORD} >$backup_path/$FILENAME
  fi
done
