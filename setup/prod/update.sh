sourceFolder=/home/gitlab/mordhau-scribe/setup/prod
transferSource=${sourceFolder}/transferBackups.sh
transferDest=/home/gitlab/transferBackups.sh
cp ${transferSource} ${transferDest}
chmod +x ${transferDest}

makeBackupSource=${sourceFolder}/makeBackup.sh
makeBackupDest=/home/gitlab/makeBackup.sh
cp ${makeBackupSource} ${makeBackupDest}
chmod +x ${makeBackupDest}
