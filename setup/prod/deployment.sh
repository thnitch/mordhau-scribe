#!/bin/bash
set -e

sourceFolder=/home/gitlab/mordhau-scribe/setup/prod

export $(grep -v '^#' ~/.secrets/scribe.prod.env | xargs -d '\n')
cd /home/gitlab
rm -rf /home/gitlab/mordhau-scribe
echo "${CI_COMMIT_SHORT_SHA}"
git clone --depth 1 "https://${CI_DEPLOY_USER}:${CI_DEPLOY_PASSWORD}@gitlab.com/thnitch/mordhau-scribe.git"
cd /home/gitlab/mordhau-scribe
git reset --hard "${CI_COMMIT_SHORT_SHA}"
docker login "${CI_REGISTRY}" -u "${CI_DEPLOY_USER}" -p "${CI_DEPLOY_PASSWORD}"
docker pull registry.gitlab.com/thnitch/mordhau-scribe/steam-ticket-provider:latest

chmod +x "${sourceFolder}/db/10_createUsers.sh"

if systemctl is-active --quiet scribe; then
  echo "Restarting scribe"
  sudo systemctl restart scribe
fi
# https://stackoverflow.com/questions/40462189/docker-compose-set-user-and-group-on-mounted-volume
