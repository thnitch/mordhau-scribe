# mordhau-scribe

Spring Boot based backend for https://mordhau-scribe.com

See [mordhau-scribe-front-end](https://gitlab.com/thnitch/mordhau-scribe-front-end) for the frontend code.
