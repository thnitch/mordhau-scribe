const express = require('express')
const {log, ExpressAPILogMiddleware} = require('@rama41222/node-logger')
const TicketGenerator = require('./ticket-generator')
const fs = require('fs').promises
const path = require('path')

const config = {
  name: 'ticket-provider',
  port: process.env.TICKET_PROVIDER_PORT || 3000,
  host: process.env.TICKET_PROVIDER_HOST || 'localhost',
  accountName: process.env.TICKET_PROVIDER_ACCOUNT_NAME,
  password: process.env.TICKET_PROVIDER_ACCOUNT_PASSWORD,
  secondFactor: process.env.TICKET_PROVIDER_ACCOUNT_SECOND_FACTOR,
  appId: parseInt(process.env.TICKET_PROVIDER_APP_ID, 10),
  maxTicketAge: process.env.TICKET_PROVIDER_MAX_AGE
    ? parseInt(process.env.TICKET_PROVIDER_MAX_AGE, 10)
    : 30 * 60 * 1000,
  ticketFilePath: path.resolve(__dirname, 'ticket.json'),
  maxTicketUpdateAttempts: process.env.TICKET_PROVIDER_MAX_UPDATE_ATTEMPTS
    ? process.env.TICKET_PROVIDER_MAX_UPDATE_ATTEMPTS
    : 5,
  ticketUpdateAttemptWait: process.env.TICKET_PROVIDER_UPDATE_ATTEMPT_WAIT
    ? process.env.TICKET_PROVIDER_UPDATE_ATTEMPT_WAIT
    : 3000,
}

const app = express()
const logger = log({console: true, file: true, label: config.name})
let steamTicket = {
  timestamp: 0,
  ticket: null,
}

updateTicket().catch((error) => {
  logger.error(`Initial ticket fetch failed: ${error}`)
})

app.use(ExpressAPILogMiddleware(logger, {request: true}))

app.get('/ticket', (req, res) => {
  const now = +new Date()
  const ticketAge = now - steamTicket.timestamp
  const forceUpdate = req.query.hasOwnProperty('forceUpdate')
  if (ticketAge > config.maxTicketAge || forceUpdate) {
    updateTicket()
      .then(() => {
        res.status(200).json(steamTicket)
      })
      .catch((err) => {
        const error = err.message || err
        logger.error(error)
        res.status(500).json({error})
      })
  } else res.status(200).json(steamTicket)
})

app.listen(config.port, config.host, (e) => {
  if (e) {
    throw new Error('Internal Server Error')
  }
  logger.info(`${config.name} running on ${config.host}:${config.port}`)
})

async function updateTicket() {
  for (let attempt = 0; attempt < config.maxTicketUpdateAttempts; attempt++) {
    try {
      const result = await TicketGenerator.generateAuthSessionTicket(
        config.accountName,
        config.password,
        config.secondFactor,
        config.appId
      )
      const ticketHex = result.ticket.toString('hex')
      steamTicket = {
        timestamp: +new Date(),
        ticket: ticketHex,
      }
      logger.info('Updated ticket')
      return
    } catch (error) {
      logger.error(`Attempt ${attempt + 1} failed with error ${error}`)
      if (attempt < config.maxTicketUpdateAttempts - 1)
        await timeout(config.ticketUpdateAttemptWait)
    }
  }

  throw new Error(
    `Could not update ticket. Stopped after ${config.maxTicketUpdateAttempts} attempts.`
  )
}

function timeout(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms))
}
