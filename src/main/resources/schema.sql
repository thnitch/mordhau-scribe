ALTER TABLE player_identity
    MODIFY name VARCHAR(255)
        CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
