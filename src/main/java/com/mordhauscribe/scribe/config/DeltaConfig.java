package com.mordhauscribe.scribe.config;

import java.time.Duration;
import java.util.Comparator;
import java.util.List;

import jakarta.annotation.PostConstruct;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("scribe.delta")
public class DeltaConfig {

  private List<Duration> desiredDeltas;
  private Duration defaultDelta;

  @PostConstruct
  private void setUp() {
    if (!desiredDeltas.contains(defaultDelta)) {
      desiredDeltas.add(defaultDelta);
    }

    desiredDeltas.sort(Comparator.comparing(Duration::getSeconds));
  }

  public List<Duration> getDesiredDeltas() {
    return desiredDeltas;
  }

  public void setDesiredDeltas(List<Duration> desiredDeltas) {
    this.desiredDeltas = desiredDeltas;
  }

  public Duration getDefaultDelta() {
    return defaultDelta;
  }

  public void setDefaultDelta(Duration defaultDelta) {
    this.defaultDelta = defaultDelta;
  }
}
