package com.mordhauscribe.scribe.config;

import static org.springframework.security.config.Customizer.withDefaults;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

  private static final String ACTUATOR_ACCESS_ROLE = "ACTUATOR_ACCESS";

  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    http.csrf(AbstractHttpConfigurer::disable);
    http
            .authorizeHttpRequests((authz) -> authz
                    .requestMatchers("/actuator/**")
                    .hasRole(ACTUATOR_ACCESS_ROLE)
                    .anyRequest()
                    .permitAll()
            )
            .httpBasic(withDefaults());
    return http.build();
  }
}
