package com.mordhauscribe.scribe.controller;

import com.mordhauscribe.scribe.model.Leaderboard;
import com.mordhauscribe.scribe.model.QueueType;
import com.mordhauscribe.scribe.model.SnapshotDeltas;
import com.mordhauscribe.scribe.service.LeaderboardService;
import io.micrometer.core.annotation.Timed;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.SortedSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/leaderboard")
public class LeaderboardController {

  private final LeaderboardService leaderboardService;
  private final boolean accessPastLeaderboardsEnabled;

  @Autowired
  public LeaderboardController(
      final LeaderboardService leaderboard1v1Service,
      @Value("${scribe.access-past-leaderboards-enabled}")
          final boolean accessPastLeaderboardsEnabled) {
    this.leaderboardService = leaderboard1v1Service;
    this.accessPastLeaderboardsEnabled = accessPastLeaderboardsEnabled;
  }

  @GetMapping(value = {"/duel"})
  @Timed(
      value = "scribe.controller.time",
      description = "Time taken for a controller method",
      extraTags = {"service", "leaderboard"})
  public ResponseEntity<Leaderboard> getDuelLeaderboard(
      @RequestParam final Optional<Long> duration,
      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
          final Optional<Instant> lastTimestamp) {
    final Leaderboard leaderboard = leaderboardService.getLeaderboard(QueueType.SOLO);
    return buildLeaderboardResponseEntity(duration, lastTimestamp, leaderboard);
  }

  @GetMapping("/duelAt")
  @Timed(
      value = "scribe.controller.time",
      description = "Time taken for a controller method",
      extraTags = {"service", "leaderboard"})
  public ResponseEntity<Leaderboard> getDuelLeaderboardAt(
      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final Instant at) {
    if (!accessPastLeaderboardsEnabled) return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    final Leaderboard leaderboard = leaderboardService.constructLeaderboard(QueueType.SOLO, at);
    return buildLeaderboardResponseEntity(Optional.empty(), Optional.empty(), leaderboard);
  }

  @GetMapping("/team")
  @Timed(
      value = "scribe.controller.time",
      description = "Time taken for a controller method",
      extraTags = {"service", "leaderboard"})
  public ResponseEntity<Leaderboard> getTeamLeaderboard(
      @RequestParam final Optional<Long> duration,
      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
          final Optional<Instant> lastTimestamp) {
    final Leaderboard leaderboard = leaderboardService.getLeaderboard(QueueType.TEAM);
    return buildLeaderboardResponseEntity(duration, lastTimestamp, leaderboard);
  }

  @GetMapping("/teamAt")
  @Timed(
      value = "scribe.controller.time",
      description = "Time taken for a controller method",
      extraTags = {"service", "leaderboard"})
  public ResponseEntity<Leaderboard> getTeamLeaderboardAt(
      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final Instant at) {
    if (!accessPastLeaderboardsEnabled) return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    final Leaderboard leaderboard = leaderboardService.constructLeaderboard(QueueType.TEAM, at);
    return buildLeaderboardResponseEntity(Optional.empty(), Optional.empty(), leaderboard);
  }

  @GetMapping(value = {"/duelAllTime"})
  @Timed(
      value = "scribe.controller.time",
      description = "Time taken for a controller method",
      extraTags = {"service", "leaderboard"})
  public ResponseEntity<Leaderboard> getDuelAllTimeLeaderboard(
      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
          final Optional<Instant> lastTimestamp) {
    final Leaderboard leaderboard = leaderboardService.getAllTimeLeaderboard(QueueType.SOLO);
    return buildLeaderboardResponseEntity(lastTimestamp, leaderboard);
  }

  @GetMapping("/teamAllTime")
  @Timed(
      value = "scribe.controller.time",
      description = "Time taken for a controller method",
      extraTags = {"service", "leaderboard"})
  public ResponseEntity<Leaderboard> getTeamAllTimeLeaderboard(
      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
          final Optional<Instant> lastTimestamp) {
    final Leaderboard leaderboard = leaderboardService.getAllTimeLeaderboard(QueueType.TEAM);
    return buildLeaderboardResponseEntity(lastTimestamp, leaderboard);
  }

  @GetMapping(value = {"/duelPeakMmr"})
  @Timed(
      value = "scribe.controller.time",
      description = "Time taken for a controller method",
      extraTags = {"service", "leaderboard"})
  public ResponseEntity<Leaderboard> getDuelPeakMmrLeaderboard(
      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
          final Optional<Instant> lastTimestamp) {
    final Leaderboard leaderboard = leaderboardService.getPeakMmrLeaderboard(QueueType.SOLO);
    return buildLeaderboardResponseEntity(lastTimestamp, leaderboard);
  }

  @GetMapping("/teamPeakMmr")
  @Timed(
      value = "scribe.controller.time",
      description = "Time taken for a controller method",
      extraTags = {"service", "leaderboard"})
  public ResponseEntity<Leaderboard> getTeamPeakMmrLeaderboard(
      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
          final Optional<Instant> lastTimestamp) {
    final Leaderboard leaderboard = leaderboardService.getPeakMmrLeaderboard(QueueType.TEAM);
    return buildLeaderboardResponseEntity(lastTimestamp, leaderboard);
  }

  private ResponseEntity<Leaderboard> buildLeaderboardResponseEntity(
      final Optional<Instant> lastTimestamp, final Leaderboard leaderboard) {
    if (leaderboard == null) return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    if (lastTimestamp.isPresent() && !leaderboard.getTimestamp().isAfter(lastTimestamp.get())) {
      return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    return new ResponseEntity<>(leaderboard, HttpStatus.OK);
  }

  private ResponseEntity<Leaderboard> buildLeaderboardResponseEntity(
      final Optional<Long> duration,
      final Optional<Instant> lastTimestamp,
      final Leaderboard leaderboard) {
    if (lastTimestamp.isPresent() && !leaderboard.getTimestamp().isAfter(lastTimestamp.get())) {
      return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    if (duration.isEmpty()) {
      return new ResponseEntity<>(leaderboard, HttpStatus.OK);
    } else {
      final boolean durationAvailable = leaderboard.getDeltaMap().containsKey(duration.get());
      if (durationAvailable)
        return new ResponseEntity<>(
            leaderboard.copyWithDefaultDelta(duration.get()), HttpStatus.OK);
      else return new ResponseEntity<>(leaderboard, HttpStatus.OK);
    }
  }

  @GetMapping(value = {"/delta/duel"})
  @Timed(
      value = "scribe.controller.time",
      description = "Time taken for a controller method",
      extraTags = {"service", "leaderboard"})
  public ResponseEntity<SnapshotDeltas> getDeltaSolo(
      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final Instant lastTimestamp,
      @RequestParam final Long duration) {
    final Leaderboard leaderboard = leaderboardService.getLeaderboard(QueueType.SOLO);
    return buildSnapshotDeltaResponseEntity(lastTimestamp, duration, leaderboard);
  }

  @GetMapping(value = {"/delta/team"})
  @Timed(
      value = "scribe.controller.time",
      description = "Time taken for a controller method",
      extraTags = {"service", "leaderboard"})
  public ResponseEntity<SnapshotDeltas> getDeltaTeam(
      @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final Instant lastTimestamp,
      @RequestParam final Long duration) {
    final Leaderboard leaderboard = leaderboardService.getLeaderboard(QueueType.TEAM);
    return buildSnapshotDeltaResponseEntity(lastTimestamp, duration, leaderboard);
  }

  private ResponseEntity<SnapshotDeltas> buildSnapshotDeltaResponseEntity(
      final Instant lastTimestamp, final Long duration, final Leaderboard leaderboard) {
    if (!leaderboard.getTimestamp().equals(lastTimestamp)) {
      return new ResponseEntity<>(HttpStatus.GONE);
    }

    if (!leaderboard.getDeltaMap().containsKey(duration)) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    return new ResponseEntity<>(leaderboard.getDeltaMap().get(duration), HttpStatus.OK);
  }

  @GetMapping(value = {"/deltas/duel"})
  @Timed(
      value = "scribe.controller.time",
      description = "Time taken for a controller method",
      extraTags = {"service", "leaderboard"})
  public ResponseEntity<Map<String, Object>> getAvailableDeltasSolo() {
    final Leaderboard leaderboard = leaderboardService.getLeaderboard(QueueType.SOLO);
    return buildSnapshotDeltasResponseEntity(leaderboard);
  }

  @GetMapping(value = {"/deltas/team"})
  @Timed(
      value = "scribe.controller.time",
      description = "Time taken for a controller method",
      extraTags = {"service", "leaderboard"})
  public ResponseEntity<Map<String, Object>> getAvailableDeltasTeam() {
    final Leaderboard leaderboard = leaderboardService.getLeaderboard(QueueType.TEAM);
    return buildSnapshotDeltasResponseEntity(leaderboard);
  }

  private ResponseEntity<Map<String, Object>> buildSnapshotDeltasResponseEntity(
      Leaderboard leaderboard) {
    final SortedSet<Long> availableDeltas = leaderboard.getAvailableDeltas();
    final Map<String, Object> result = new HashMap<>();
    result.put("availableDeltas", availableDeltas);
    result.put("defaultDelta", leaderboard.getDefaultDelta().getDuration());

    return new ResponseEntity<>(result, HttpStatus.OK);
  }
}
