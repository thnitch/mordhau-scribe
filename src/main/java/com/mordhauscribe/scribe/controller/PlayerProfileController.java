package com.mordhauscribe.scribe.controller;

import com.mordhauscribe.scribe.dto.PlayerSearchRequest;
import com.mordhauscribe.scribe.dto.PlayerSearchResult;
import com.mordhauscribe.scribe.model.PlayerProfile;
import com.mordhauscribe.scribe.model.entity.PlayerIdentity;
import com.mordhauscribe.scribe.service.PlayerIdentityService;
import com.mordhauscribe.scribe.service.PlayerProfileService;
import io.micrometer.core.annotation.Timed;
import jakarta.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/players")
public class PlayerProfileController {

  private final PlayerIdentityService playerIdentityService;
  private final PlayerProfileService playerProfileService;

  @Autowired
  public PlayerProfileController(
      final PlayerIdentityService playerIdentityService,
      final PlayerProfileService playerProfileService) {
    this.playerIdentityService = playerIdentityService;
    this.playerProfileService = playerProfileService;
  }

  @GetMapping("/{playFabId}")
  @Timed(
      value = "scribe.controller.time",
      description = "Time taken for a controller method",
      extraTags = {"service", "player_profile"})
  public ResponseEntity<PlayerProfile> fetchPlayerProfile(@PathVariable final String playFabId) {
    final Optional<PlayerIdentity> identity = playerIdentityService.findByPlayFabId(playFabId);
    return identity
        .map(
            playerIdentity ->
                new ResponseEntity<>(
                    playerProfileService.fetchPlayerProfile(playerIdentity), HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping("/search")
  @Timed(
      value = "scribe.controller.time",
      description = "Time taken for a controller method",
      extraTags = {"service", "player_profile"})
  public ResponseEntity<PlayerSearchResult> searchPlayer(
      @Valid @RequestBody final PlayerSearchRequest searchRequest) {
    final Optional<PlayerIdentity> identity =
        playerIdentityService.findByAny(searchRequest.getName().trim(), searchRequest.getPlatform(), searchRequest.getPlatformAccountId());
    return identity
        .map(
            playerIdentity ->
                new ResponseEntity<>(
                    new PlayerSearchResult(playerIdentity.getPlayFabId()), HttpStatus.OK))
        .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public Map<String, String> handleValidationExceptions(final MethodArgumentNotValidException ex) {
    final Map<String, String> errors = new HashMap<>();
    ex.getBindingResult()
        .getAllErrors()
        .forEach(
            (error) -> {
              final String fieldName = ((FieldError) error).getField();
              final String errorMessage = error.getDefaultMessage();
              errors.put(fieldName, errorMessage);
            });
    return errors;
  }
}
