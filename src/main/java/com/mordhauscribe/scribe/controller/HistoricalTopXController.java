package com.mordhauscribe.scribe.controller;

import com.mordhauscribe.scribe.model.HistoricalTopX;
import com.mordhauscribe.scribe.model.QueueType;
import com.mordhauscribe.scribe.service.HistoricalTopXService;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HistoricalTopXController {

  private final HistoricalTopXService historicalTopXService;

  @Autowired
  public HistoricalTopXController(final HistoricalTopXService historicalTopXService) {
    this.historicalTopXService = historicalTopXService;
  }

  @GetMapping("/duelHistoricalTopX")
  @Timed(
      value = "scribe.controller.time",
      description = "Time taken for a controller method",
      extraTags = {"service", "historicaltopx"})
  public HistoricalTopX getDuelHistoricalTopX() {
    return historicalTopXService.getHistoricalTopX(QueueType.SOLO);
  }

  @GetMapping("/teamHistoricalTopX")
  @Timed(
      value = "scribe.controller.time",
      description = "Time taken for a controller method",
      extraTags = {"service", "historicaltopx"})
  public HistoricalTopX getTeamHistoricalTopX() {
    return historicalTopXService.getHistoricalTopX(QueueType.TEAM);
  }
}
