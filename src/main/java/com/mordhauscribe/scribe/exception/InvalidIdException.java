package com.mordhauscribe.scribe.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class InvalidIdException extends RuntimeException {
  public InvalidIdException(String id) {
    super("'" + id + "' is not a valid Steam or player ID!");
  }
}
