package com.mordhauscribe.scribe.exception;

public class PlayFabServiceException extends ServiceException {
  public PlayFabServiceException(String message) {
    super(message);
  }

  public PlayFabServiceException(Throwable cause) {
    super(cause);
  }
}
