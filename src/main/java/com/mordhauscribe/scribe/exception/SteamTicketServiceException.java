package com.mordhauscribe.scribe.exception;

public class SteamTicketServiceException extends ServiceException {
  public SteamTicketServiceException(String message) {
    super(message);
  }

  public SteamTicketServiceException(Throwable cause) {
    super(cause);
  }
}
