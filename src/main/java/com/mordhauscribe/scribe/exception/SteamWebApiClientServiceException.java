package com.mordhauscribe.scribe.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class SteamWebApiClientServiceException extends ServiceException {
  public SteamWebApiClientServiceException(final String message) {
    super(message);
  }

  public SteamWebApiClientServiceException(final Throwable cause) {
    super(cause);
  }
}
