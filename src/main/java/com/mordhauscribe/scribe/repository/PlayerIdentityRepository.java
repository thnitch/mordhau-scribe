package com.mordhauscribe.scribe.repository;

import com.mordhauscribe.scribe.model.entity.PlayerIdentity;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface PlayerIdentityRepository extends CrudRepository<PlayerIdentity, String> {
  Optional<PlayerIdentity> findByPlayFabId(String playFabId);

  @Query
  Set<PlayerIdentity> findByPlayFabIdIn(Set<String> playFabIds);

  @Query(
      "update PlayerIdentity set lastSeenRanked = :lastSeen where titlePlayerAccountId in :titlePlayerAccountIds")
  @Modifying
  void updateLastSeenRanked(Set<String> titlePlayerAccountIds, Instant lastSeen);

  @Query(
      "update PlayerIdentity set lastSeenCasual = :lastSeen where titlePlayerAccountId in :titlePlayerAccountIds")
  @Modifying
  void updateLastSeenCasual(Set<String> titlePlayerAccountIds, Instant lastSeen);

  Optional<PlayerIdentity> findByPlatformAndPlatformAccountId(
      String platform, String platformAccountId);

  @Query(
      "select titlePlayerAccountId from PlayerIdentity where titlePlayerAccountId in :titlePlayerAccountIds")
  Set<String> findExistingPlayerAccountIds(Set<String> titlePlayerAccountIds);

  @Query("select playFabId from PlayerIdentity where titlePlayerAccountId is null")
  Set<String> findPlayFabIdsWithoutTitlePlayerAccountId();

  @Query
  Set<PlayerIdentity> findByTitlePlayerAccountIdIn(Set<String> titlePlayerAccountIds);

  @Query(nativeQuery = true, name = "PlayerIdentity.determinePlatformDistribution")
  List<PlayerIdentity.PlatformDistributionResult> determinePlatformDistribution(
      @Param("withinLastXMinutes") int withinLastXMinutes);

  @Query(
      nativeQuery = true,
      value =
          "select * "
            + "from player_identity "
            + "where (last_seen_ranked >= (DATE_ADD((select max(last_seen_ranked) from player_identity), INTERVAL -5 MINUTE))) "
            + "  AND player_id NOT IN (select unique(player_id) "
            + "              from report "
            + "              where "
            + "              timestamp > DATE_ADD((select max(last_seen_ranked) from player_identity), INTERVAL -5 MINUTE));")
  List<PlayerIdentity> findPlayerIdentitiesInNeedOfUpdate();
}
