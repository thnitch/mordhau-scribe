package com.mordhauscribe.scribe.repository;

import com.mordhauscribe.scribe.model.PlayerHistoryEntry;
import com.mordhauscribe.scribe.model.QueueType;
import com.mordhauscribe.scribe.model.entity.PlayerIdentity;
import com.mordhauscribe.scribe.model.entity.Report;
import com.mordhauscribe.scribe.model.nativequeryresult.ReconstructedReport;
import java.time.Instant;
import java.util.List;
import java.util.Set;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ReportRepository extends CrudRepository<Report, Long> {
  List<Report> findByPlayerIdentityOrderByGameTimestamp(PlayerIdentity identity);

  @Query
  List<PlayerHistoryEntry> findByPlayerIdentityAndQueueType(
      PlayerIdentity playerIdentity, QueueType queueType);

  @Query(name = "Report.constructLeaderboard", nativeQuery = true)
  List<ReconstructedReport> constructLeaderboard(
      @Param("startDate") Instant startDate,
      @Param("endDate") Instant endDate,
      @Param("queueType") String queueType,
      @Param("limit") Integer limit);

  @Query(name = "Report.constructAllTimeLeaderboard", nativeQuery = true)
  List<ReconstructedReport> constructAllTimeLeaderboard(
      @Param("queueType") String queueType, @Param("limit") Integer limit);

  @Query(name = "Report.constructPeakMmrLeaderboard", nativeQuery = true)
  List<ReconstructedReport> constructPeakMmrLeaderboard(
      @Param("queueType") String queueType, @Param("limit") Integer limit);

  @Query(name = "Report.closestBeforeOrFirstSince", nativeQuery = true)
  List<Report> closestBeforeOrFirstSince(
      @Param("startDate") Instant startDate,
      @Param("playerIds") Set<String> playerIds,
      @Param("queueType") String queueType);

  @Query(name = "Report.latest", nativeQuery = true)
  List<Report> latest(@Param("playerId") String playerId);

  @Query(
      value =
          "SELECT min(report.gameTimestamp) FROM Report report WHERE report.queueType = :queueType")
  Instant findEarliestTimestamp(QueueType queueType);

  @Query(name = "Report.determineDuelRankDistribution", nativeQuery = true)
  List<Report.RankDistributionResult> determineDuelRankDistribution();

  @Query(name = "Report.determineTeamfightRankDistribution", nativeQuery = true)
  List<Report.RankDistributionResult> determineTeamfightRankDistribution();

  @Query(name = "Report.determineWinLoss", nativeQuery = true)
  List<Report.WinLossResult> determineWinLoss(
      @Param("playerIds") Set<String> playerIds,
      @Param("queueType") String queueType,
      @Param("beforeTimestamp") Instant beforeTimestamp);
}
