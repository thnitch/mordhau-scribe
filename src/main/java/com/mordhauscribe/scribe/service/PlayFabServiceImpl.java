package com.mordhauscribe.scribe.service;

import static com.playfab.PlayFabMultiplayerAPI.GetQueueStatistics;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.mordhauscribe.scribe.exception.PlayFabServiceException;
import com.mordhauscribe.scribe.exception.SteamTicketServiceException;
import com.mordhauscribe.scribe.model.Game;
import com.mordhauscribe.scribe.model.Leaderboard;
import com.mordhauscribe.scribe.model.LeaderboardEntry;
import com.mordhauscribe.scribe.model.PlayFabAccountInfo;
import com.mordhauscribe.scribe.model.PlayFabOfficialGamesFilter;
import com.mordhauscribe.scribe.model.PlayerStatisticsResult;
import com.mordhauscribe.scribe.model.QueueType;
import com.mordhauscribe.scribe.model.TitleData;
import com.mordhauscribe.scribe.model.entity.LatestPlayerReport;
import com.mordhauscribe.scribe.model.entity.PlayerIdentity;
import com.playfab.PlayFabClientAPI;
import com.playfab.PlayFabClientModels.*;
import com.playfab.PlayFabClientModels.ExecuteCloudScriptRequest;
import com.playfab.PlayFabClientModels.ExecuteCloudScriptResult;
import com.playfab.PlayFabClientModels.GetPlayFabIDsFromSteamIDsRequest;
import com.playfab.PlayFabClientModels.GetPlayFabIDsFromSteamIDsResult;
import com.playfab.PlayFabClientModels.GetPlayerCombinedInfoRequest;
import com.playfab.PlayFabClientModels.GetPlayerCombinedInfoRequestParams;
import com.playfab.PlayFabClientModels.GetPlayerCombinedInfoResult;
import com.playfab.PlayFabClientModels.LoginWithSteamRequest;
import com.playfab.PlayFabClientModels.StatisticValue;
import com.playfab.PlayFabClientModels.SteamPlayFabIdPair;
import com.playfab.PlayFabErrors;
import com.playfab.PlayFabErrors.PlayFabErrorCode;
import com.playfab.PlayFabErrors.PlayFabResult;
import com.playfab.PlayFabMultiplayerModels.GetQueueStatisticsRequest;
import com.playfab.PlayFabMultiplayerModels.GetQueueStatisticsResult;
import com.playfab.PlayFabProfilesAPI;
import com.playfab.PlayFabProfilesModels;
import com.playfab.PlayFabProfilesModels.GetEntityProfilesRequest;
import com.playfab.PlayFabProfilesModels.GetEntityProfilesResponse;
import com.playfab.PlayFabSettings;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import jakarta.annotation.PostConstruct;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class PlayFabServiceImpl implements HealthIndicator, PlayFabService {
  public static final String FUNCTION_NAME = "getLeaderboard";
  public static final String CLOUD_FUNCTION_PARAM_VERSION_KEY = "Version";
  public static final String CLOUD_FUNCTION_PARAM_LEADERBOARD_KEY = "Leaderboard";
  public static final String CLOUD_FUNCTION_PARAM_MAX_RESULTS_KEY = "MaxResults";
  public static final int CLOUD_FUNCTION_PARAM_MAX_RESULTS_VALUE = 100;
  public static final String CLOUD_FUNCTION_RESULT_ENTRIES = "Entries";
  public static final String PLAYER_ID = "PlayFabId";
  public static final String NAME = "Name";
  public static final String PLATFORM = "Platform";
  public static final String PLATFORM_ACCOUNT_ID = "PlatformAccountId";
  public static final String POSITION = "Position";
  public static final String MMR = "Value";
  public static final String SESSION_TICKET_AVAILABLE_KEY = "sessionTicketAvailable";
  public static final String SESSION_TICKET_AVAILABLE_ERROR_KEY = "sessionTicketAvailableError";
  public static final Set<String> EXPECTED_STATISTICS;
  public static final String STAT_DUELRANK = "DuelRank";
  public static final String STAT_DUELRANKSAMPLES = "DuelRankSamples";
  public static final String STAT_DUELRANKTIMESTAMP = "DuelRankTimestamp";
  public static final String STAT_DUELRANKLEADERBOARD = "DuelRankLeaderboard";
  public static final String STAT_TEAMFIGHTRANK = "TeamfightRank";
  public static final String STAT_TEAMFIGHTRANKSAMPLES = "TeamfightRankSamples";
  public static final String STAT_TEAMFIGHTRANKTIMESTAMP = "TeamfightRankTimestamp";
  public static final String STAT_TEAMFIGHTRANKLEADERBOARD = "TeamfightRankLeaderboard";
  public static final String STAT_CASUALRANK = "CasualRank";
  public static final String STAT_CASUALRANKSAMPLES = "CasualRankSamples";
  public static final String STAT_CASUALRANKTIMESTAMP = "CasualRankTimestamp";
  public static final String GLOBAL_BANNED_PLAYERS = "GlobalBannedPlayers";
  public static final String OFFICIAL_BANNED_PLAYERS = "OfficialBannedPlayers";
  public static final String GLOBAL_MUTED_PLAYERS = "GlobalMutedPlayers";
  public static final String OFFICIAL_MUTED_PLAYERS = "OfficialMutedPlayers";
  private static final Logger LOG = LogManager.getLogger();
  public static final String PLAY_FAB_API_TYPE_CLIENT = "Client";
  public static final String PLAY_FAB_API_TYPE_MATCHMAKING = "Matchmaking";

  static {
    EXPECTED_STATISTICS =
        new HashSet<>(
            Arrays.asList(
                STAT_DUELRANK,
                STAT_DUELRANKSAMPLES,
                STAT_DUELRANKTIMESTAMP,
                STAT_DUELRANKLEADERBOARD,
                STAT_TEAMFIGHTRANK,
                STAT_TEAMFIGHTRANKSAMPLES,
                STAT_TEAMFIGHTRANKTIMESTAMP,
                STAT_TEAMFIGHTRANKLEADERBOARD,
                STAT_CASUALRANK,
                STAT_CASUALRANKSAMPLES,
                STAT_CASUALRANKTIMESTAMP));
  }

  private final SteamTicketService steamTicketService;
  private final String playFabTitleId;
  private final Counter counterClientApiCalls;
  private final Counter counterMatchmakingApiCalls;
  private final Map<QueueType, ExecuteCloudScriptRequest> executionRequests;
  private final ObjectMapper objectMapper = new ObjectMapper();
  private String playFabSessionTicket;
  private String playFabEntityToken;
  private int gameVersion;

  private final PlayFabOfficialGamesFilter officialGamesFilter = new PlayFabOfficialGamesFilter();
  private final MeterRegistry meterRegistry;
  private final Map<PlayFabErrorCode, Counter> apiErrorCounters;
  private Health health;
  private boolean invalidSteamTicket = false;

  @Autowired
  public PlayFabServiceImpl(
      @Value("${scribe.play-fab.title-id}") final String playFabTitleId,
      final SteamTicketService steamTicketService,
      final MeterRegistry meterRegistry) {
    this.playFabTitleId = playFabTitleId;
    this.steamTicketService = steamTicketService;
    this.executionRequests = new HashMap<>();

    PlayFabSettings.TitleId = this.playFabTitleId;

    this.meterRegistry = meterRegistry;
    this.apiErrorCounters = new HashMap<>();
    final Tags commonApiCallTags =
        Tags.of("api", "playfab", "service", "playfab", "exception", "none");
    counterClientApiCalls =
        Counter.builder("scribe.api.calls")
            .description("calls to a downstream api")
            .tags(commonApiCallTags.and("api_type", PLAY_FAB_API_TYPE_CLIENT))
            .register(meterRegistry);
    counterMatchmakingApiCalls =
        Counter.builder("scribe.api.calls")
            .description("calls to a downstream api")
            .tags(commonApiCallTags.and("api_type", PLAY_FAB_API_TYPE_MATCHMAKING))
            .register(meterRegistry);

    health = Health.down().build();
  }

  @PostConstruct
  protected void initialize() {
    try {
      updateSessionTicket();
      updateGameVersion();
    } catch (final PlayFabServiceException e) {
      LOG.error("Failed to update session ticket: " + e.getMessage());
    }
  }

  @Override
  @Async
  @Scheduled(
      fixedRate = SCHEDULED_SESSION_TICKET_UPDATE_DELAY_HOURS,
      initialDelay = SCHEDULED_SESSION_TICKET_UPDATE_DELAY_HOURS,
      timeUnit = TimeUnit.HOURS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  public void updateSessionTicket() throws PlayFabServiceException {
    final LoginWithSteamRequest loginRequest = new LoginWithSteamRequest();
    loginRequest.CreateAccount = false;
    loginRequest.TitleId = this.playFabTitleId;

    try {
      loginRequest.SteamTicket = steamTicketService.generateTicket(invalidSteamTicket).ticket();
    } catch (final SteamTicketServiceException e) {
      throw new PlayFabServiceException(e);
    }

    counterClientApiCalls.increment();
    final PlayFabResult<LoginResult> result = PlayFabClientAPI.LoginWithSteam(loginRequest);

    final PlayFabErrors.PlayFabError error = result.Error;
    final LoginResult loginResult = result.Result;

    if (loginResult != null) {
      playFabSessionTicket = loginResult.SessionTicket;
      playFabEntityToken = loginResult.EntityToken.EntityToken;
      invalidSteamTicket = false;
      LOG.debug("Updated PlayFab session ticket");
    } else {
      final String errorMessage;
      if (error != null) {
        if (error.pfErrorCode.equals(PlayFabErrorCode.InvalidSteamTicket) && !invalidSteamTicket) {
          invalidSteamTicket = true;
          updateSessionTicket();
        }
        // in case of Downstream Service errors (Steam Web API) retry authentication
        else if (error.pfErrorCode.equals(PlayFabErrorCode.DownstreamServiceUnavailable)) {
          invalidSteamTicket = false;
          updateSessionTicket();
        }
        
        errorMessage = String.format("%s: %s", error.pfErrorCode, error.errorMessage);
      } else {
        errorMessage = "Neither error or result present in PlayFab response";
      }
      health =
          Health.down()
              .withDetail(SESSION_TICKET_AVAILABLE_KEY, false)
              .withDetail(SESSION_TICKET_AVAILABLE_ERROR_KEY, errorMessage)
              .build();
      LOG.error(errorMessage);
    }
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  public List<PlayerIdentity> fetchProfilesByTitlePlayerAccountIds(
      final Set<String> titlePlayerAccountIds) throws PlayFabServiceException {
    ensureTicket();

    final GetEntityProfilesRequest request = new GetEntityProfilesRequest();
    request.Entities =
        titlePlayerAccountIds.stream()
            .map(
                id -> {
                  final PlayFabProfilesModels.EntityKey entityKey =
                      new PlayFabProfilesModels.EntityKey();
                  entityKey.Id = id;
                  entityKey.Type = "title_player_account";
                  return entityKey;
                })
            .collect(Collectors.toCollection(ArrayList::new));

    counterClientApiCalls.increment();
    final PlayFabResult<GetEntityProfilesResponse> playFabResult =
        PlayFabProfilesAPI.GetProfiles(request);

    final GetEntityProfilesResponse result =
        processPlayFabResult(
            playFabResult, "fetchProfilesByTitlePlayerAccountIds", PLAY_FAB_API_TYPE_CLIENT);

    return extractPlayerIdentitiesFromGetProfilesResult(result);
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  public Optional<PlayerIdentity> checkPlayerExists(final String playFabId)
      throws PlayFabServiceException {
    ensureTicket();

    final GetPlayerCombinedInfoRequest request = new GetPlayerCombinedInfoRequest();
    final GetPlayerCombinedInfoRequestParams params = new GetPlayerCombinedInfoRequestParams();
    params.GetUserReadOnlyData = true;
    params.GetUserAccountInfo = true;

    request.PlayFabId = playFabId;
    request.InfoRequestParameters = params;

    counterClientApiCalls.increment();
    final PlayFabResult<GetPlayerCombinedInfoResult> playFabResult =
        PlayFabClientAPI.GetPlayerCombinedInfo(request);

    if (playFabResult.Error != null) {
      final PlayFabErrorCode pfErrorCode = playFabResult.Error.pfErrorCode;
      if ((pfErrorCode.equals(PlayFabErrorCode.AccountNotFound)
          || pfErrorCode.equals(PlayFabErrorCode.PlayerNotInGame)
          || pfErrorCode.equals(PlayFabErrorCode.InvalidParams))) {
        return Optional.empty();
      }
    }

    final GetPlayerCombinedInfoResult result =
        processPlayFabResult(
            playFabResult, String.format("fetchPlayerStatistics %s", playFabId), "Client");

    if (result.PlayFabId.equals(playFabId)) {
      return extractPlayerIdentityFromGetPlayerCombinedInfoResult(result);
    } else return Optional.empty();
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  public PlayerStatisticsResult fetchPlayerStatistics(final String playFabId)
      throws PlayFabServiceException {
    ensureTicket();

    final Instant now = Instant.now();

    final GetPlayerCombinedInfoRequest request = new GetPlayerCombinedInfoRequest();
    final GetPlayerCombinedInfoRequestParams params = new GetPlayerCombinedInfoRequestParams();
    params.GetPlayerStatistics = true;
    params.GetUserReadOnlyData = true;
    params.GetUserAccountInfo = true;

    request.PlayFabId = playFabId;
    request.InfoRequestParameters = params;

    counterClientApiCalls.increment();
    final PlayFabResult<GetPlayerCombinedInfoResult> playFabResult =
        PlayFabClientAPI.GetPlayerCombinedInfo(request);

    final GetPlayerCombinedInfoResult result =
        processPlayFabResult(
            playFabResult, String.format("fetchPlayerStatistics %s", playFabId), "Client");

    final Optional<PlayerIdentity> playerIdentity =
        extractPlayerIdentityFromGetPlayerCombinedInfoResult(result);

    final ArrayList<StatisticValue> playerStatistics = result.InfoResultPayload.PlayerStatistics;

    final Map<String, Integer> stats = mapStatistics(playerStatistics);
    final LatestPlayerReport playerReport =
        new LatestPlayerReport(
            now,
            stats.getOrDefault(STAT_DUELRANK, null),
            stats.getOrDefault(STAT_DUELRANKSAMPLES, 0),
            stats.containsKey(STAT_DUELRANKTIMESTAMP)
                ? Instant.ofEpochSecond(stats.get(STAT_DUELRANKTIMESTAMP))
                : null,
            stats.getOrDefault(STAT_TEAMFIGHTRANK, null),
            stats.getOrDefault(STAT_TEAMFIGHTRANKSAMPLES, 0),
            stats.containsKey(STAT_TEAMFIGHTRANKTIMESTAMP)
                ? Instant.ofEpochSecond(stats.get(STAT_TEAMFIGHTRANKTIMESTAMP))
                : null);

    return new PlayerStatisticsResult(playerIdentity, playerReport);
  }

  private Optional<PlayerIdentity> extractPlayerIdentityFromGetPlayerCombinedInfoResult(
      GetPlayerCombinedInfoResult result) throws PlayFabServiceException {
    final UserDataRecord accountInfoRecord =
        result.InfoResultPayload.UserReadOnlyData.get("AccountInfo");
    if (accountInfoRecord == null) {
      // there is a special case where for whatever reason the players account info is not part of
      // their UserReadOnlyData payload, in these rare cases the account information has to be
      // retrieved through the GetProfiles endpoint using the players TitlePlayerAccountId
      final EntityKey titlePlayerAccount =
          result.InfoResultPayload.AccountInfo.TitleInfo.TitlePlayerAccount;
      if (!titlePlayerAccount.Type.equals("title_player_account")) {
        LOG.warn(
            "Attempted to resolve non-player account PlayFabID '{}', TitlePlayerAccountID '{}'",
            result.PlayFabId,
            titlePlayerAccount.Id);
        return Optional.empty();
      }

      final List<PlayerIdentity> identities =
          fetchProfilesByTitlePlayerAccountIds(
              new HashSet<>(Collections.singletonList(titlePlayerAccount.Id)));
      if (identities.isEmpty()) {
        LOG.warn(
            "GetProfiles failed for PlayFabID '{}', TitlePlayerAccountID '{}'",
            result.PlayFabId,
            titlePlayerAccount.Id);
        return Optional.empty();
      }

      return Optional.of(identities.get(0));
    }

    final String accountInfoJson = accountInfoRecord.Value;
    final PlayFabAccountInfo accountInfo =
        new Gson().fromJson(accountInfoJson, PlayFabAccountInfo.class);

    if (!accountInfo.Type().equals("Player")) return Optional.empty();

    return Optional.of(
        new PlayerIdentity(
            accountInfo.PlayFabId(),
            accountInfo.EntityId(),
            accountInfo.Platform(),
            accountInfo.PlatformAccountId(),
            accountInfo.Name()));
  }

  private List<PlayerIdentity> extractPlayerIdentitiesFromGetProfilesResult(
      GetEntityProfilesResponse result) {
    List<PlayerIdentity> identities = new ArrayList<>();

    for (PlayFabProfilesModels.EntityProfileBody profile : result.Profiles) {
      if (profile.Objects == null) {
        LOG.debug(
            "profile.Objects is null for PlayFabID: {}, TitlePlayerAccountId: {}",
            profile.Lineage.MasterPlayerAccountId,
            profile.Entity.Id);
        identities.add(
            new PlayerIdentity(profile.Lineage.MasterPlayerAccountId, profile.Entity.Id));
        continue;
      }
      final PlayFabAccountInfo accountInfo =
          new Gson()
              .fromJson(
                  profile.Objects.get("AccountInfo").EscapedDataObject, PlayFabAccountInfo.class);
      identities.add(
          new PlayerIdentity(
              accountInfo.PlayFabId(),
              accountInfo.EntityId(),
              accountInfo.Platform(),
              accountInfo.PlatformAccountId(),
              accountInfo.Name()));
    }

    return identities;
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  public TitleData fetchTitleData() throws PlayFabServiceException {
    ensureTicket();

    final GetPlayerCombinedInfoRequest request = new GetPlayerCombinedInfoRequest();
    final GetPlayerCombinedInfoRequestParams params = new GetPlayerCombinedInfoRequestParams();
    params.GetTitleData = true;
    params.TitleDataKeys =
        new ArrayList<>(
            Arrays.asList(
                GLOBAL_BANNED_PLAYERS,
                "OfficialBannedPlayers",
                "GlobalMutedPlayers",
                "OfficialMutedPlayers"));
    request.InfoRequestParameters = params;

    counterClientApiCalls.increment();
    final PlayFabResult<GetPlayerCombinedInfoResult> playFabResult =
        PlayFabClientAPI.GetPlayerCombinedInfo(request);

    final GetPlayerCombinedInfoResult result =
        processPlayFabResult(playFabResult, "fetchTitleData %s", "Client");

    final Map<String, String> unmappedTitleData = result.InfoResultPayload.TitleData;

    return mapTitleData(unmappedTitleData);
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  public Map<String, Game> fetchCurrentGames(CollectionFilter filter)
      throws PlayFabServiceException {
    ensureTicket();

    final CurrentGamesRequest currentGamesRequest = new CurrentGamesRequest();
    currentGamesRequest.TagFilter = filter;

    counterClientApiCalls.increment();

    final PlayFabResult<CurrentGamesResult> playFabResult =
        PlayFabClientAPI.GetCurrentGames(currentGamesRequest);

    final CurrentGamesResult result =
        processPlayFabResult(playFabResult, "fetchCurrentGames", "Client");

    return mapCurrentGames(result);
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  public GetQueueStatisticsResult fetchQueueStatistics(QueueType queueType)
      throws PlayFabServiceException {
    ensureTicket();
    final GetQueueStatisticsRequest request = new GetQueueStatisticsRequest();
    request.QueueName = queueType.getQueueName();

    counterMatchmakingApiCalls.increment();
    final PlayFabResult<GetQueueStatisticsResult> playFabResult = GetQueueStatistics(request);

    return processPlayFabResult(
        playFabResult, "fetchQueueStatistics %s", PLAY_FAB_API_TYPE_MATCHMAKING);
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  public Leaderboard fetchLeaderboard(final QueueType queueType) throws PlayFabServiceException {
    ensureTicket();

    final Instant timestamp = Instant.now();

    counterClientApiCalls.increment();
    final PlayFabResult<ExecuteCloudScriptResult> cloudScriptResult =
        PlayFabClientAPI.ExecuteCloudScript(executionRequests.get(queueType));

    final ExecuteCloudScriptResult result =
        processPlayFabResult(cloudScriptResult, queueType.name(), "Client");

    final List<LeaderboardEntry> entries;

    if (result.FunctionResult != null) {
      final List<Map<String, Object>> resultEntries =
          (List<Map<String, Object>>)
              ((Map<String, Object>) result.FunctionResult).get(CLOUD_FUNCTION_RESULT_ENTRIES);
      entries =
          resultEntries.stream()
              .map(
                  map ->
                      new LeaderboardEntry(
                          (String) map.get(PLAYER_ID),
                          (String) map.get(NAME),
                          (String) map.get(PLATFORM),
                          (String) map.get(PLATFORM_ACCOUNT_ID),
                          ((Double) map.get(POSITION)).intValue(),
                          ((Double) map.get(MMR)).intValue()))
              .collect(toList());
    } else {
      LOG.warn("No FunctionResult for {}!", queueType);
      entries = new ArrayList<>();
    }

    LOG.trace("Fetched {} leaderboard from PlayFab", queueType);
    health = Health.up().build();
    return new Leaderboard(entries, timestamp, queueType);
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  public List<GenericPlayFabIdPair> fetchPlayFabIdsByGenericIds(
      final ArrayList<GenericServiceId> genericIds) throws PlayFabServiceException {
    ensureTicket();

    final GetPlayFabIDsFromGenericIDsRequest request = new GetPlayFabIDsFromGenericIDsRequest();
    request.GenericIDs = genericIds;

    counterClientApiCalls.increment();
    final PlayFabResult<GetPlayFabIDsFromGenericIDsResult> playFabResult =
        PlayFabClientAPI.GetPlayFabIDsFromGenericIDs(request);
    final GetPlayFabIDsFromGenericIDsResult result =
        processPlayFabResult(playFabResult, "fetchPlayFabIdsByGenericIds", "Client");
    return result.Data;
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  public List<SteamPlayFabIdPair> fetchPlayFabIdsBySteamIds(final List<String> steamIds)
      throws PlayFabServiceException {
    ensureTicket();

    final GetPlayFabIDsFromSteamIDsRequest request = new GetPlayFabIDsFromSteamIDsRequest();
    request.SteamStringIDs = new ArrayList<>(steamIds);
    counterClientApiCalls.increment();
    final PlayFabResult<GetPlayFabIDsFromSteamIDsResult> playFabResult =
        PlayFabClientAPI.GetPlayFabIDsFromSteamIDs(request);
    final GetPlayFabIDsFromSteamIDsResult result =
        processPlayFabResult(playFabResult, "fetchPlayFabIdsBySteamIds", "Client");
    return result.Data;
  }

  private Map<String, Integer> mapStatistics(final ArrayList<StatisticValue> playerStatistics) {
    final Map<String, Integer> statistics =
        playerStatistics.stream()
            .collect(toMap(statistic -> statistic.StatisticName, statistic -> statistic.Value));
    final Set<String> unexpectedStatistics =
        statistics.keySet().stream()
            .filter(x -> !EXPECTED_STATISTICS.contains(x))
            .collect(Collectors.toSet());
    if (!unexpectedStatistics.isEmpty()) {
      LOG.warn(
          String.format(
              "Unexpected statistics retrieved!\n%s",
              Arrays.toString(unexpectedStatistics.toArray())));
    }

    return statistics;
  }

  private TitleData mapTitleData(final Map<String, String> titleData)
      throws PlayFabServiceException {
    try {
      final Map<String, Long> globalBannedPlayers =
          objectMapper.readValue(titleData.get(GLOBAL_BANNED_PLAYERS), new TypeReference<>() {});
      final Map<String, Long> officialBannedPlayers =
          objectMapper.readValue(titleData.get(OFFICIAL_BANNED_PLAYERS), new TypeReference<>() {});
      final Map<String, Long> globalMutedPlayers =
          objectMapper.readValue(titleData.get(GLOBAL_MUTED_PLAYERS), new TypeReference<>() {});
      final Map<String, Long> officialMutedPlayers =
          objectMapper.readValue(titleData.get(OFFICIAL_MUTED_PLAYERS), new TypeReference<>() {});

      return new TitleData(
          globalBannedPlayers, officialBannedPlayers, globalMutedPlayers, officialMutedPlayers);
    } catch (final JsonProcessingException e) {
      LOG.error(e);
      throw new PlayFabServiceException("Could not map title data");
    }
  }

  private Map<String, Game> mapCurrentGames(CurrentGamesResult result) {
    Map<String, Game> games = new HashMap<>();

    for (GameInfo gameInfo : result.Games) {
      final String playersString = gameInfo.Tags.get("Players");

      if (playersString.isBlank()) continue;
      final List<String> players = List.of(gameInfo.Tags.get("Players").split(","));

      final Game game =
          new Game(
              gameInfo.Tags.get("Region"),
              gameInfo.LobbyID,
              gameInfo.Tags.get("Version"),
              gameInfo.RunTime,
              gameInfo.Tags.get("ServerName"),
              gameInfo.ServerIPV4Address,
              gameInfo.Tags.get("MapName"),
              Boolean.parseBoolean(gameInfo.Tags.get("IsOfficial")),
              Boolean.parseBoolean(gameInfo.Tags.get("IsModded")),
              gameInfo.Tags.get("GameMode"),
              gameInfo.Tags.get("QueueName"),
              gameInfo.Tags.get("Location"),
              Integer.parseInt(gameInfo.Tags.get("MaxPlayers")),
              Integer.parseInt(gameInfo.Tags.get("ReservedSlots")),
              players);

      games.put(game.getLobbyID(), game);
    }

    return games;
  }

  private void ensureTicket() throws PlayFabServiceException {
    if (playFabSessionTicket == null) {
      final String errorMessage = "PlayFab session ticket not present.";
      LOG.error(errorMessage);
      final PlayFabServiceException e = new PlayFabServiceException(errorMessage);
      LOG.error(errorMessage);
      health = Health.down().withException(e).build();
      throw e;
    }
  }

  @Override
  @Async
  @Scheduled(
      fixedRate = SCHEDULED_GAME_VERSION_UPDATE_DELAY_HOURS,
      initialDelay = SCHEDULED_GAME_VERSION_UPDATE_DELAY_HOURS,
      timeUnit = TimeUnit.HOURS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  public void updateGameVersion() throws PlayFabServiceException {
    ensureTicket();

    final Collection<Game> games = fetchCurrentGames(officialGamesFilter).values();
    final Map<String, List<Game>> serversByVersion =
        games.stream()
            .filter(game -> !game.getServerName().contains(" Dev "))
            .collect(Collectors.groupingBy(Game::getVersion));

    int highestServerCountVersionCount = 0;
    String highestServerCountVersion = "";
    for (Map.Entry<String, List<Game>> entry : serversByVersion.entrySet()) {
      if (entry.getValue().size() > highestServerCountVersionCount) {
        highestServerCountVersion = entry.getKey();
        highestServerCountVersionCount = entry.getValue().size();
      }
    }

    final int newGameVersion = Integer.parseInt(highestServerCountVersion);

    if (gameVersion != newGameVersion) {
      LOG.info("Game version changed '{}' -> '{}'", gameVersion, newGameVersion);
      executionRequests.put(QueueType.SOLO, createExecutionRequest(QueueType.SOLO, newGameVersion));
      executionRequests.put(QueueType.TEAM, createExecutionRequest(QueueType.TEAM, newGameVersion));
    }

    gameVersion = newGameVersion;
  }

  private <T> T processPlayFabResult(
      final PlayFabResult<T> playFabResult, final String context, final String apiType)
      throws PlayFabServiceException {
    final PlayFabErrors.PlayFabError error = playFabResult.Error;
    final T result = playFabResult.Result;

    if (result != null) {
      return result;
    } else {
      String errorMessage = String.format("[%s] ", context);
      if (error != null) {
        errorMessage += error.errorMessage + ": " + error.pfErrorCode;
        if (error.pfErrorCode.equals(PlayFabErrorCode.NotAuthenticated)) {
          playFabSessionTicket = null;
          playFabEntityToken = null;
          updateSessionTicket();
        }

        if (!apiErrorCounters.containsKey(error.pfErrorCode)) {
          apiErrorCounters.put(
              error.pfErrorCode,
              Counter.builder("scribe.api.calls.errors")
                  .description("errors in calls to a downstream api")
                  .tags(
                      Tags.of(
                          "api",
                          "playfab",
                          "api_type",
                          apiType,
                          "service",
                          "playfab",
                          "exception",
                          error.pfErrorCode.name()))
                  .register(meterRegistry));
        }

        apiErrorCounters.get(error.pfErrorCode).increment();
      } else {
        errorMessage += "Neither error or result present in PlayFab response";
      }
      final PlayFabServiceException e = new PlayFabServiceException(errorMessage);
      LOG.error(errorMessage);
      health = Health.down().withException(e).build();
      throw e;
    }
  }

  private ExecuteCloudScriptRequest createExecutionRequest(
      final QueueType queueType, final int cloudFunctionVersion) {
    final ExecuteCloudScriptRequest executionRequest = new ExecuteCloudScriptRequest();
    executionRequest.FunctionName = FUNCTION_NAME;
    executionRequest.FunctionParameter =
        createExecutionRequestParameters(queueType, cloudFunctionVersion);
    executionRequest.GeneratePlayStreamEvent = false;
    executionRequest.RevisionSelection = CloudScriptRevisionOption.Live;

    return executionRequest;
  }

  private Map<String, Object> createExecutionRequestParameters(
      final QueueType queueType, final int cloudFunctionVersion) {
    final HashMap<String, Object> functionParameter = new HashMap<>();
    functionParameter.put(CLOUD_FUNCTION_PARAM_VERSION_KEY, cloudFunctionVersion);
    functionParameter.put(CLOUD_FUNCTION_PARAM_LEADERBOARD_KEY, queueType.getCloudFunctionParam());
    functionParameter.put(
        CLOUD_FUNCTION_PARAM_MAX_RESULTS_KEY, CLOUD_FUNCTION_PARAM_MAX_RESULTS_VALUE);

    return functionParameter;
  }

  @Override
  public Health health() {
    return this.health;
  }
}
