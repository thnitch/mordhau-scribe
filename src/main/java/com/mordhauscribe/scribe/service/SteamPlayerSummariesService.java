package com.mordhauscribe.scribe.service;

import static java.util.stream.Collectors.*;

import com.lukaspradel.steamapi.core.exception.SteamApiException;
import com.lukaspradel.steamapi.data.json.playersummaries.GetPlayerSummaries;
import com.lukaspradel.steamapi.data.json.playersummaries.Player;
import com.lukaspradel.steamapi.webapi.client.SteamWebApiClient;
import com.lukaspradel.steamapi.webapi.request.GetPlayerSummariesRequest;
import com.lukaspradel.steamapi.webapi.request.builders.SteamWebApiRequestFactory;
import com.mordhauscribe.scribe.model.PlayerSummary;
import com.mordhauscribe.scribe.model.PlayerSummaryCacheEntry;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class SteamPlayerSummariesService {
  public static final int SCHEDULED_CACHE_EVICTION_DELAY_HOURS = 24;
  public static final int TOO_MANY_API_REQUESTS_COOLDOWN_MINUTES = 120;
  public static final int MAX_SUMMARIES_PER_REQUEST = 100; // Hard limit of the Steam API
  private static final Logger LOG = LogManager.getLogger();
  private final SteamWebApiClient client;
  private final Map<String, PlayerSummaryCacheEntry> playerSummaryCache;
  private final Counter counterApiCalls;
  private final Counter counterApiCallsErrors;
  private Instant apiRequestCooldownUntil;

  @Autowired
  public SteamPlayerSummariesService(
      @Value("${scribe.steam-web-api.key}") final String apiKey, MeterRegistry meterRegistry) {
    client = new SteamWebApiClient.SteamWebApiClientBuilder(apiKey).build();

    final Tags commonApiCallsTags =
        Tags.of("api", "steam", "api_type", "web", "service", "steam_player_summaries");
    counterApiCalls =
        Counter.builder("scribe.api.calls")
            .description("calls to a downstream api")
            .tags(commonApiCallsTags.and("exception", "none"))
            .register(meterRegistry);
    counterApiCallsErrors =
        Counter.builder("scribe.api.calls.errors")
            .description("errors in calls to a downstream api")
            .tags(commonApiCallsTags.and("exception", "SteamApiException"))
            .register(meterRegistry);
    playerSummaryCache =
        meterRegistry.gaugeMapSize(
            "scribe.cache",
            Tags.of("service", "steam_player_summaries", "type", "player_summaries"),
            new ConcurrentHashMap<>(2048));
  }

  private static <T> List<List<T>> partition(final List<T> list) {
    final List<List<T>> parts = new ArrayList<>();
    final int N = list.size();
    for (int i = 0; i < N; i += MAX_SUMMARIES_PER_REQUEST) {
      parts.add(new ArrayList<>(list.subList(i, Math.min(N, i + MAX_SUMMARIES_PER_REQUEST))));
    }
    return parts;
  }

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "steam_player_summaries"})
  @Async
  public CompletableFuture<PlayerSummary> retrievePlayerSummary(final String steamId) {
    final List<PlayerSummary> playerSummaries =
        retrievePlayerSummaries(Collections.singletonList(steamId), false);

    if (playerSummaries.isEmpty()) return CompletableFuture.completedFuture(null);
    return CompletableFuture.completedFuture(playerSummaries.get(0));
  }

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "steam_player_summaries"})
  public List<PlayerSummary> retrievePlayerSummaries(final List<String> steamIds, boolean hasPriority) {
    if (steamIds.size() > MAX_SUMMARIES_PER_REQUEST) {
      return partition(steamIds).stream()
          .map(x -> retrievePlayerSummaries(x, hasPriority))
          .flatMap(Collection::stream)
          .collect(toList());
    }

    final Map<Boolean, List<String>> isCached =
        steamIds.stream()
            .collect(
                partitioningBy(
                    x ->
                        playerSummaryCache.containsKey(x)
                            && !playerSummaryCache.get(x).canBeUpdated()));

    final List<PlayerSummary> summaries = fetchPlayerSummaries(isCached.get(false), hasPriority);
    summaries.addAll(
        isCached.get(true).stream().map(id -> playerSummaryCache.get(id).playerSummary()).toList());
    return summaries;
  }

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "steam_player_summaries"})
  private List<PlayerSummary> fetchPlayerSummaries(
      final List<String> steamIds, boolean hasPriority) {
    if (steamIds.isEmpty()) return new ArrayList<>();
    if (!hasPriority) {
      if (apiRequestCooldownUntil != null && apiRequestCooldownUntil.isAfter(Instant.now()))
        return new ArrayList<>();
      else apiRequestCooldownUntil = null;
    }

    counterApiCalls.increment();
    final GetPlayerSummariesRequest request =
        SteamWebApiRequestFactory.createGetPlayerSummariesRequest(steamIds);

    final GetPlayerSummaries response;
    try {
      response = client.processRequest(request);
    } catch (final SteamApiException e) {
      counterApiCallsErrors.increment();
      LOG.error("Steam Web API error: {}", Strings.join(steamIds, ','), e);

      if (e.getMessage()
          .contains(
              "The Web API request failed with the following HTTP error: Too Many Requests (status code: 429)")) {
        apiRequestCooldownUntil =
            Instant.now().plus(TOO_MANY_API_REQUESTS_COOLDOWN_MINUTES, ChronoUnit.MINUTES);
      }
      return new ArrayList<>();
    }
    LOG.trace("Requested {} new player summaries", response.getResponse().getPlayers().size());

    final Instant timestamp = Instant.now();
    final List<Player> playerSummaries = response.getResponse().getPlayers();

    final Map<String, PlayerSummaryCacheEntry> newCacheEntries =
        playerSummaries.stream()
            .collect(
                toMap(
                    Player::getSteamid,
                    summary ->
                        new PlayerSummaryCacheEntry(
                            PlayerSummary.fromSteamPlayerSummary(summary), timestamp)));

    playerSummaryCache.putAll(newCacheEntries);
    LOG.trace("Added {} player summaries to cache", newCacheEntries.size());
    return newCacheEntries.values().stream()
        .map(PlayerSummaryCacheEntry::playerSummary)
        .collect(toList());
  }

  @Scheduled(
      fixedDelay = SCHEDULED_CACHE_EVICTION_DELAY_HOURS,
      initialDelay = SCHEDULED_CACHE_EVICTION_DELAY_HOURS,
      timeUnit = TimeUnit.HOURS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "steam_player_summaries"})
  void evictStaleCacheEntries() {
    final int sizePreRemove = playerSummaryCache.size();
    playerSummaryCache.values().removeIf(PlayerSummaryCacheEntry::mustBeRemoved);
    LOG.trace(
        "Evicted {} stale PlayerSummary cache entries", sizePreRemove - playerSummaryCache.size());
  }
}
