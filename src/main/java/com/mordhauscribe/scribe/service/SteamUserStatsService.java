package com.mordhauscribe.scribe.service;

import com.lukaspradel.steamapi.core.exception.SteamApiException;
import com.lukaspradel.steamapi.data.json.playerstats.GetUserStatsForGame;
import com.lukaspradel.steamapi.data.json.playerstats.Playerstats;
import com.lukaspradel.steamapi.webapi.client.SteamWebApiClient;
import com.lukaspradel.steamapi.webapi.request.GetUserStatsForGameRequest;
import com.lukaspradel.steamapi.webapi.request.builders.SteamWebApiRequestFactory;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class SteamUserStatsService {
  private static final Logger LOG = LogManager.getLogger();
  private static final int APP_ID = 629760;

  private final SteamWebApiClient client;
  private final Counter counterApiCalls;
  private final Counter counterApiCallErrors;

  @Autowired
  public SteamUserStatsService(
      @Value("${scribe.steam-web-api.key}") final String apiKey, MeterRegistry meterRegistry) {
    client = new SteamWebApiClient.SteamWebApiClientBuilder(apiKey).build();
    final Tags commonApiCallsTags =
        Tags.of("api", "steam", "api_type", "web", "service", "steam_user_stats");
    counterApiCalls =
        Counter.builder("scribe.api.calls")
            .description("calls to a downstream api")
            .tags(commonApiCallsTags.and("exception", "none"))
            .register(meterRegistry);
    counterApiCallErrors =
        Counter.builder("scribe.api.calls.errors")
            .description("errors in calls to a downstream api")
            .tags(commonApiCallsTags.and("exception", "SteamApiException"))
            .register(meterRegistry);
  }

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "steam_user_stats"})
  @Async
  public CompletableFuture<Optional<Playerstats>> retrievePlayerStats(final String steamId) {
    counterApiCalls.increment();
    final GetUserStatsForGameRequest request =
        SteamWebApiRequestFactory.createGetUserStatsForGameRequest(APP_ID, steamId);

    final GetUserStatsForGame response;
    try {
      response = client.processRequest(request);
    } catch (final SteamApiException e) {
      // forbidden is returned in case steam user game details are private
      if (!e.getMessage().contains("Forbidden (status code: 403)")) {
        counterApiCallErrors.increment();
        LOG.error("Steam Web API error, {}", steamId, e);
      }
      return CompletableFuture.completedFuture(Optional.empty());
    }
    
    return CompletableFuture.completedFuture(Optional.of(response.getPlayerstats()));
  }
}
