package com.mordhauscribe.scribe.service;

import com.mordhauscribe.scribe.model.Game;
import com.mordhauscribe.scribe.model.QueueType;
import io.micrometer.core.annotation.Timed;
import java.util.concurrent.TimeUnit;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

public interface CurrentGamesService {
  int SCHEDULED_CURRENT_RANKED_GAMES_UPDATE_DELAY_SECONDS = 5;
  int SCHEDULED_CURRENT_CASUAL_GAMES_UPDATE_DELAY_SECONDS = 55;

  int SCHEDULED_ONLINE_PLAYERS_UPDATE_DELAY_SECONDS = 60;
  int SCHEDULED_ONLINE_PLAYERS_UPDATE_INITIAL_DELAY_SECONDS = 10;

  int STALE_UNRESOLVED_RANKED_PLAYER_MINUTES = 5;

  @Async
  @Scheduled(
      fixedDelay = SCHEDULED_CURRENT_RANKED_GAMES_UPDATE_DELAY_SECONDS,
      timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "current_games"})
  void updateCurrentRankedGames();

  @Async
  @Scheduled(
      fixedDelay = SCHEDULED_CURRENT_CASUAL_GAMES_UPDATE_DELAY_SECONDS,
      timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "current_games"})
  void updateCurrentCasualGames();

  @Async
  @Scheduled(
      fixedDelay = SCHEDULED_ONLINE_PLAYERS_UPDATE_DELAY_SECONDS,
      initialDelay = SCHEDULED_ONLINE_PLAYERS_UPDATE_INITIAL_DELAY_SECONDS,
      timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "current_games"})
  void updateOnlinePlayers();

  Game lookupGameForPlayer(String titlePlayerAccountId);

  long currentPlayerCountByQueueType(QueueType queueType);

  void register(CurrentRankedGamesListener listener);

  interface CurrentRankedGamesListener {
    default void onCurrentRankedGamesUpdate() {}

    default void onOnlineRankedPlayersUpdate() {}
  }
}
