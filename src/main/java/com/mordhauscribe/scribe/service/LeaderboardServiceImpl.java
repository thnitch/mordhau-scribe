package com.mordhauscribe.scribe.service;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.*;

import com.mordhauscribe.scribe.config.DeltaConfig;
import com.mordhauscribe.scribe.exception.PlayFabServiceException;
import com.mordhauscribe.scribe.model.ChangedEntry;
import com.mordhauscribe.scribe.model.Leaderboard;
import com.mordhauscribe.scribe.model.LeaderboardEntry;
import com.mordhauscribe.scribe.model.Outcome;
import com.mordhauscribe.scribe.model.Platform;
import com.mordhauscribe.scribe.model.PlayerSummary;
import com.mordhauscribe.scribe.model.QueueType;
import com.mordhauscribe.scribe.model.SnapshotDeltas;
import com.mordhauscribe.scribe.model.entity.PlayerIdentity;
import com.mordhauscribe.scribe.model.nativequeryresult.ReconstructedReport;
import io.micrometer.core.annotation.Timed;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.boot.availability.ReadinessState;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class LeaderboardServiceImpl
    implements LeaderboardService,
        HealthIndicator,
        QueueStatisticsService.QueueStatisticsListener,
        CurrentGamesService.CurrentRankedGamesListener {

  private static final Logger LOG = LogManager.getLogger();

  private final Map<QueueType, Leaderboard> leaderboards = new HashMap<>();
  private final Map<QueueType, Leaderboard> allTimeLeaderboards = new HashMap<>();
  private final Map<QueueType, Leaderboard> peakMmrLeaderboards = new HashMap<>();
  private final SteamPlayerSummariesService steamPlayerSummariesService;
  private final DeltaService deltaService;
  private final ReportService reportService;
  private final TitleDataService titleDataService;
  private final CurrentGamesService currentGamesService;
  private final QueueStatisticsService queueStatisticsService;
  private final DeltaConfig deltaConfig;
  private final List<Long> configuredDeltas;
  private final boolean updatesEnabled;
  private final Map<QueueType, SortedSet<Long>> availableDeltasByQueueType = new HashMap<>();
  private Health health;

  @Autowired
  public LeaderboardServiceImpl(
      final ApplicationEventPublisher eventPublisher,
      final PlayFabService playFabService,
      final SteamPlayerSummariesService steamPlayerSummariesService,
      final DeltaService deltaService,
      final ReportService reportService,
      final ReportUpdateService reportUpdateService,
      final PlayerIdentityService playerIdentityService,
      final TitleDataService titleDataService,
      final CurrentGamesService currentGamesService,
      QueueStatisticsService queueStatisticsService,
      final DeltaConfig deltaConfig,
      @Value("${scribe.updates-enabled}") final boolean updatesEnabled) {
    this.steamPlayerSummariesService = steamPlayerSummariesService;
    this.deltaService = deltaService;
    this.reportService = reportService;
    this.titleDataService = titleDataService;
    this.currentGamesService = currentGamesService;
    this.queueStatisticsService = queueStatisticsService;
    this.deltaConfig = deltaConfig;
    configuredDeltas =
        deltaConfig.getDesiredDeltas().stream().map(Duration::getSeconds).collect(toList());
    this.updatesEnabled = updatesEnabled;

    updateAvailableDeltas();

    currentGamesService.register(this);
    queueStatisticsService.register(this);

    LOG.debug("Initializing leaderboards from database");
    for (QueueType queueType : QueueType.values()) {
      updateLeaderboard(queueType);
    }
    
    Set<String> playerIdsToUpdate = new HashSet<>();
    for (QueueType queueType : QueueType.values()) {
      try {
        final Leaderboard leaderboard = playFabService.fetchLeaderboard(queueType);
        playerIdsToUpdate.addAll(
            leaderboard.getEntries().stream().map(LeaderboardEntry::getPlayFabId).collect(toSet()));
      } catch (PlayFabServiceException e) {
        LOG.error("Failed to initially fetch {} leaderboard: {}", queueType, e);
      }
    }

    final Set<PlayerIdentity> playerIdentitiesToUpdate = playerIdentityService.findByPlayFabIdIn(playerIdsToUpdate);
    LOG.debug("Queued updates for {} players", playerIdentitiesToUpdate.size());
    reportUpdateService.queueUpdates(playerIdentitiesToUpdate, ReportUpdateService.LEADERBOARD_INIT_UPDATE_PRIORITY, 0);

    AvailabilityChangeEvent.publish(
        eventPublisher, "Initialized", ReadinessState.ACCEPTING_TRAFFIC);
  }

  @Override
  public Leaderboard getLeaderboard(final QueueType queueType) {
    return this.leaderboards.get(queueType);
  }

  @Override
  @Async
  @Scheduled(
      fixedDelay = SCHEDULED_LEADERBOARD_UPDATE_DELAY_SECONDS,
      initialDelay = SCHEDULED_LEADERBOARD_UPDATE_DELAY_SECONDS,
      timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "leaderboard"})
  public void updateDuelLeaderboard() {
    if (updatesEnabled) {
      final QueueType queueType = QueueType.SOLO;
      updateLeaderboard(queueType);
    }
  }

  @Override
  @Async
  @Scheduled(
      fixedDelay = SCHEDULED_LEADERBOARD_UPDATE_DELAY_SECONDS,
      initialDelay = SCHEDULED_LEADERBOARD_UPDATE_DELAY_SECONDS,
      timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "leaderboard"})
  public void updateTeamLeaderboard() {
    if (updatesEnabled) {
      final QueueType queueType = QueueType.TEAM;
      updateLeaderboard(queueType);
    }
  }

  @Override
  public Leaderboard getAllTimeLeaderboard(final QueueType queueType) {
    return this.allTimeLeaderboards.get(queueType);
  }

  @Override
  @Async
  @Scheduled(
      initialDelay = SCHEDULED_DUEL_ALL_TIME_LEADERBOARD_INITIAL_DELAY_MILLIS,
      fixedDelay = SCHEDULED_UNOFFICIAL_LEADERBOARD_UPDATE_DELAY_MILLIS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "leaderboard"})
  public void updateDuelAllTimeLeaderboard() {
    final QueueType queueType = QueueType.SOLO;
    updateAllTimeLeaderboard(queueType);
  }

  @Override
  @Async
  @Scheduled(
      initialDelay = SCHEDULED_TEAM_ALL_TIME_LEADERBOARD_INITIAL_DELAY_MILLIS,
      fixedDelay = SCHEDULED_UNOFFICIAL_LEADERBOARD_UPDATE_DELAY_MILLIS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "leaderboard"})
  public void updateTeamAllTimeLeaderboard() {
    final QueueType queueType = QueueType.TEAM;
    updateAllTimeLeaderboard(queueType);
  }

  @Override
  public Leaderboard getPeakMmrLeaderboard(final QueueType queueType) {
    return this.peakMmrLeaderboards.get(queueType);
  }

  @Override
  @Async
  @Scheduled(
      initialDelay = SCHEDULED_DUEL_PEAK_MMR_LEADERBOARD_INITIAL_DELAY_MILLIS,
      fixedDelay = SCHEDULED_UNOFFICIAL_LEADERBOARD_UPDATE_DELAY_MILLIS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "leaderboard"})
  public void updateDuelPeakMmrLeaderboard() {
    final QueueType queueType = QueueType.SOLO;
    updatePeakMmrLeaderboard(queueType);
  }

  @Override
  @Async
  @Scheduled(
      initialDelay = SCHEDULED_TEAM_PEAK_MMR_LEADERBOARD_INITIAL_DELAY_MILLIS,
      fixedDelay = SCHEDULED_UNOFFICIAL_LEADERBOARD_UPDATE_DELAY_MILLIS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "leaderboard"})
  public void updateTeamPeakMmrLeaderboard() {
    final QueueType queueType = QueueType.TEAM;
    updatePeakMmrLeaderboard(queueType);
  }

  private void updateLeaderboard(final QueueType queueType) {
    final List<ReconstructedReport> reconstructedReports =
        reportService.constructLeaderboard(Instant.now(), queueType);

    final Leaderboard newLeaderboard =
        new Leaderboard(mapToLeaderboardEntries(reconstructedReports), Instant.now(), queueType);

    processLeaderboard(newLeaderboard, reconstructedReports);
    health = Health.up().build();
  }

  private void updateAllTimeLeaderboard(final QueueType queueType) {
    final Instant now = Instant.now();

    final List<ReconstructedReport> allTimeLeaderboard = reportService.constructAllTime(queueType);

    final Leaderboard leaderboard =
        processUnofficialLeaderboard(queueType, now, allTimeLeaderboard);
    allTimeLeaderboards.put(queueType, leaderboard);
  }

  private void updatePeakMmrLeaderboard(final QueueType queueType) {
    final Instant now = Instant.now();

    final List<ReconstructedReport> peakMmrLeaderboard = reportService.constructPeakMmr(queueType);

    final Leaderboard leaderboard =
        processUnofficialLeaderboard(queueType, now, peakMmrLeaderboard);
    peakMmrLeaderboards.put(queueType, leaderboard);
  }

  private Leaderboard processUnofficialLeaderboard(
      final QueueType queueType, final Instant now, final List<ReconstructedReport> reports) {
    final List<LeaderboardEntry> entries = mapToLeaderboardEntries(reports);
    final Leaderboard leaderboard = new Leaderboard(entries, now, queueType);
    leaderboard.setLastChange(leaderboards.get(queueType).getLastChange());
    leaderboard.setPlayerCountInGame(currentGamesService.currentPlayerCountByQueueType(queueType));
    leaderboard.setPlayerCountInQueue(
        queueStatisticsService.getQueueStatistics(queueType).NumberOfPlayersMatching);
    addPlayerSummaries(leaderboard);
    return leaderboard;
  }

  private List<LeaderboardEntry> mapToLeaderboardEntries(
      final List<ReconstructedReport> leaderboard) {
    return leaderboard.stream()
        .map(
            entry ->
                new LeaderboardEntry(
                    entry.getPlayFabId(),
                    entry.getName(),
                    entry.getPlatform(),
                    entry.getPlatformAccountId(),
                    entry.getPosition(),
                    entry.getMmr(),
                    entry.getGameTimestamp(),
                    titleDataService.getPlayerBanStatus(entry.getPlayFabId())))
        .sorted(Comparator.comparingInt(LeaderboardEntry::getPosition))
        .collect(toList());
  }

  private void processLeaderboard(
      final Leaderboard newLeaderboard, final List<ReconstructedReport> reports) {
    final QueueType queueType = newLeaderboard.getQueueType();
    addPlayerSummaries(newLeaderboard);

    newLeaderboard
        .getEntries()
        .forEach(
            leaderboardEntry ->
                leaderboardEntry.setBanned(
                    titleDataService.getPlayerBanStatus(leaderboardEntry.getPlayFabId())));

    addSnapshotDeltas(newLeaderboard, reports);

    final boolean previousLeaderboardExists = leaderboards.containsKey(queueType);
    final Leaderboard previousLeaderboard = leaderboards.get(queueType);

    final Instant lastChange =
        reports.stream()
            .map(ReconstructedReport::getGameTimestamp)
            .max(Instant::compareTo)
            .orElse(Instant.now());
    newLeaderboard.setLastChange(lastChange);

    if (previousLeaderboardExists) {
      newLeaderboard.setPlayerCountInGame(previousLeaderboard.getPlayerCountInGame());
      newLeaderboard.setPlayerCountInQueue(previousLeaderboard.getPlayerCountInQueue());
    }

    leaderboards.put(queueType, newLeaderboard);
  }

  List<ChangedEntry> determineChanges(
      final Leaderboard newLeaderboard, final Leaderboard currentLeaderboard) {
    final Map<String, LeaderboardEntry> previousEntries =
        currentLeaderboard.getEntries().stream()
            .collect(toMap(LeaderboardEntry::getPlayFabId, identity()));

    final List<ChangedEntry> changedEntries =
        newLeaderboard.getEntries().stream()
            .filter(entry -> hasEntryChanged(entry, previousEntries.get(entry.getPlayFabId())))
            .map(
                entry ->
                    ChangedEntry.of(
                        entry, determineOutcome(entry, previousEntries.get(entry.getPlayFabId()))))
            .collect(toList());

    newLeaderboard.getEntries().stream()
        .map(LeaderboardEntry::getPlayFabId)
        .toList()
        .forEach(previousEntries.keySet()::remove);

    final List<ChangedEntry> dropouts =
        previousEntries.values().stream()
            .map(
                entry ->
                    ChangedEntry.ofDropout(
                        new PlayerIdentity(
                            entry.getPlayFabId(),
                            entry.getPlatform(),
                            entry.getPlatformAccountId())))
            .toList();

    changedEntries.addAll(dropouts);
    return changedEntries;
  }

  private boolean hasEntryChanged(
      final LeaderboardEntry entry, final LeaderboardEntry previousEntry) {
    if (previousEntry == null) return true;

    return !entry.getMmr().equals(previousEntry.getMmr());
  }

  private Outcome determineOutcome(
      final LeaderboardEntry entry, final LeaderboardEntry previousEntry) {
    if (previousEntry == null) return Outcome.UNDEFINED;

    if (entry.getMmr() > previousEntry.getMmr()) {
      return Outcome.WIN;
    } else if (entry.getMmr() < previousEntry.getMmr()) {
      return Outcome.LOSS;
    }

    return Outcome.UNDEFINED;
  }

  @Override
  @Scheduled(
      fixedRate = SCHEDULED_EARLIEST_DELTA_UPDATE_DELAY_MINUTES,
      initialDelay = SCHEDULED_EARLIEST_DELTA_UPDATE_DELAY_MINUTES,
      timeUnit = TimeUnit.MINUTES)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "leaderboard"})
  public void updateAvailableDeltas() {
    Arrays.stream(QueueType.values())
        .forEach(
            queueType -> {
              final Instant earliestTimestamp = reportService.findEarliestTimestamp(queueType);

              if (earliestTimestamp != null) {
                final Instant now = Instant.now();

                final long daysBetween = DAYS.between(earliestTimestamp, now);
                final long longestPossibleDelta;
                if (daysBetween > 0) {
                  longestPossibleDelta =
                      Duration.ofDays(DAYS.between(earliestTimestamp, now)).getSeconds();
                } else {
                  longestPossibleDelta = SECONDS.between(earliestTimestamp, now);
                }

                SortedSet<Long> updatedAvailableDeltas = new TreeSet<>(this.configuredDeltas);
                updatedAvailableDeltas.add(longestPossibleDelta);
                updatedAvailableDeltas =
                    updatedAvailableDeltas.stream()
                        .filter(delta -> delta <= longestPossibleDelta)
                        .collect(toCollection(TreeSet::new));
                availableDeltasByQueueType.put(queueType, updatedAvailableDeltas);
              } else {
                availableDeltasByQueueType.put(queueType, new TreeSet<>());
              }
            });
  }

  private void addSnapshotDeltas(
      final Leaderboard leaderboard, final List<ReconstructedReport> reports) {
    final SortedSet<Long> availableDeltas =
        availableDeltasByQueueType.get(leaderboard.getQueueType());
    final List<SnapshotDeltas> deltas =
        availableDeltas.stream()
            .map(
                duration ->
                    deltaService.calculateDelta(
                        reports, leaderboard.getTimestamp(), duration, leaderboard.getQueueType()))
            .filter(Optional::isPresent)
            .map(Optional::get)
            .collect(toList());

    leaderboard.setDeltas(deltas);
    leaderboard.setDeltaMap(
        deltas.stream().collect(toMap(SnapshotDeltas::getDuration, delta -> delta)));
    leaderboard.setAvailableDeltas(availableDeltas);
    final long seconds = deltaConfig.getDefaultDelta().getSeconds();

    if (!deltas.isEmpty()) {
      leaderboard.setDefaultDelta(
          deltas.stream()
              .filter(delta -> delta.getDuration() == seconds)
              .findAny()
              .orElse(deltas.get(0)));
    } else {
      leaderboard.setDefaultDelta(new SnapshotDeltas(Map.of(), null, null, 0));
    }
  }

  private void addPlayerSummaries(final Leaderboard leaderboard) {
    final List<String> steamIds =
        leaderboard.getEntries().stream()
            .filter(entry -> Platform.STEAM.getName().equals(entry.getPlatform()))
            .map(LeaderboardEntry::getPlatformAccountId)
            .collect(toList());
    final Map<String, PlayerSummary> allSummaries =
        steamPlayerSummariesService.retrievePlayerSummaries(steamIds, true).stream()
            .collect(toMap(PlayerSummary::steamId, summary -> summary));
    leaderboard
        .getEntries()
        .forEach(
            entry -> {
              final PlayerSummary playerSummary = allSummaries.get(entry.getPlatformAccountId());

              if (playerSummary != null) {
                entry.setPlayerSummary(playerSummary);
                entry.setName(playerSummary.name());
                entry.setAvatar(playerSummary.avatarMedium());
                entry.setCountryCode(playerSummary.countryCode());
              }
            });
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "leaderboard"})
  public Leaderboard constructLeaderboard(final QueueType queueType, final Instant at) {
    final List<ReconstructedReport> reconstructedLeaderboard =
        reportService.constructLeaderboard(at, queueType);
    final Leaderboard leaderboard =
        new Leaderboard(mapToLeaderboardEntries(reconstructedLeaderboard), at, queueType);
    leaderboard.setLastChange(leaderboard.getTimestamp());
    addPlayerSummaries(leaderboard);
    return leaderboard;
  }

  @Override
  public Health health() {
    return this.health;
  }

  @Override
  public void onQueueStatisticsUpdate() {
    for (QueueType queueType : QueueType.values()) {
      long newPlayerCountInQueue =
          queueStatisticsService.getQueueStatistics(queueType).NumberOfPlayersMatching;

      if (leaderboards.containsKey(queueType))
        leaderboards.get(queueType).setPlayerCountInQueue(newPlayerCountInQueue);
      if (allTimeLeaderboards.containsKey(queueType))
        allTimeLeaderboards.get(queueType).setPlayerCountInQueue(newPlayerCountInQueue);
      if (peakMmrLeaderboards.containsKey(queueType))
        peakMmrLeaderboards.get(queueType).setPlayerCountInQueue(newPlayerCountInQueue);
    }
  }

  @Override
  public void onOnlineRankedPlayersUpdate() {
    for (QueueType queueType : QueueType.values()) {
      long newPlayerCountInGame = currentGamesService.currentPlayerCountByQueueType(queueType);

      if (leaderboards.containsKey(queueType))
        leaderboards.get(queueType).setPlayerCountInGame(newPlayerCountInGame);
      if (allTimeLeaderboards.containsKey(queueType))
        allTimeLeaderboards.get(queueType).setPlayerCountInGame(newPlayerCountInGame);
      if (peakMmrLeaderboards.containsKey(queueType))
        peakMmrLeaderboards.get(queueType).setPlayerCountInGame(newPlayerCountInGame);
    }
  }
}
