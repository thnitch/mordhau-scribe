package com.mordhauscribe.scribe.service;

import com.mordhauscribe.scribe.dto.SteamTicketProviderResponse;
import com.mordhauscribe.scribe.exception.SteamTicketServiceException;
import com.mordhauscribe.scribe.model.SteamSessionTicket;
import io.micrometer.core.annotation.Timed;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.ResponseSpec;

@Service
public class SteamTicketService implements HealthIndicator {
  public static final String TICKET_ENDPOINT = "/ticket";
  private static final String FORCE_UPDATE_PARAM = "?forceUpdate";
  private static final int RETRY_COUNT = 3;
  private static final Logger LOG = LogManager.getLogger();
  private final WebClient client;
  private Health health;

  @Autowired
  public SteamTicketService(
      @Value("${scribe.steam-ticket-provider.host}") final String ticketProviderHost,
      @Value("${scribe.steam-ticket-provider.port}") final String ticketProviderPort) {
    client =
        WebClient.builder()
            .baseUrl(ticketProviderHost + ":" + ticketProviderPort)
            .defaultHeader("Accept", "application/json")
            .build();
  }

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "steam_ticket"})
  public SteamSessionTicket generateTicket(final boolean forceUpdate)
      throws SteamTicketServiceException {
    int retries = RETRY_COUNT;
    while (retries > 0) {
      try {
        return fetchTicket(retries <= 1 || forceUpdate);
      } catch (final SteamTicketServiceException e) {
        LOG.warn("Failed to fetch steam session ticket. {} retries remaining", retries);
      } finally {
        retries--;
      }
    }
    throw new SteamTicketServiceException(
        "Failed to fetch steam session ticket. Stopped after " + RETRY_COUNT + " retries.");
  }

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "steam_ticket"})
  private SteamSessionTicket fetchTicket(final boolean forceUpdate)
      throws SteamTicketServiceException {
    final String uri = forceUpdate ? TICKET_ENDPOINT + FORCE_UPDATE_PARAM : TICKET_ENDPOINT;
    try {
      final ResponseSpec responseSpec =
          client.get().uri(uri).accept(MediaType.APPLICATION_JSON).retrieve();
      final SteamTicketProviderResponse response =
          responseSpec.bodyToMono(SteamTicketProviderResponse.class).block();
      if (response == null) {
        final String errorMessage = "No response";
        LOG.error(errorMessage);
        throw new IllegalStateException(errorMessage);
      } else if (response.getError() != null) {
        final String error = response.getError();
        LOG.error(error);
        throw new IllegalStateException(error);
      } else if (response.getTicket() != null) {
        final SteamSessionTicket ticket =
            new SteamSessionTicket(response.getTicket(), response.getTimestamp().toInstant());
        LOG.debug("Updated steam session ticket");
        health = Health.up().build();
        return ticket;
      } else {
        final String errorMessage = "Neither ticket nor error message in response";
        LOG.error(errorMessage);
        throw new IllegalStateException(errorMessage);
      }
    } catch (final Exception e) {
      health = Health.down().withException(e).build();
      throw new SteamTicketServiceException(e);
    }
  }

  @Override
  public Health health() {
    return this.health;
  }
}
