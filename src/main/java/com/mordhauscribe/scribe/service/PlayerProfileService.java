package com.mordhauscribe.scribe.service;

import com.lukaspradel.steamapi.data.json.playerstats.Playerstats;
import com.lukaspradel.steamapi.data.json.playerstats.Stat;
import com.mordhauscribe.scribe.model.Game;
import com.mordhauscribe.scribe.model.LeaderboardEntry;
import com.mordhauscribe.scribe.model.Platform;
import com.mordhauscribe.scribe.model.PlayerProfile;
import com.mordhauscribe.scribe.model.PlayerSummary;
import com.mordhauscribe.scribe.model.QueueType;
import com.mordhauscribe.scribe.model.SnapshotDelta;
import com.mordhauscribe.scribe.model.entity.LatestPlayerReport;
import com.mordhauscribe.scribe.model.entity.PlayerIdentity;
import com.mordhauscribe.scribe.model.entity.Report;
import com.mordhauscribe.scribe.service.ReportUpdateServiceImpl.FetchReportsResult;
import io.micrometer.core.annotation.Timed;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class PlayerProfileService {
  private static final Logger LOG = LogManager.getLogger();
  private final SteamPlayerSummariesService steamPlayerSummariesService;
  private final SteamUserStatsService steamUserStatsService;
  private final LeaderboardService leaderboardService;
  private final ReportUpdateService reportUpdateService;
  private final RankDistributionService rankDistributionService;
  private final TitleDataService titleDataService;
  private final CurrentGamesService currentGamesService;
  private final boolean profileReportsEnabled;

  @Autowired
  public PlayerProfileService(
      final SteamPlayerSummariesService steamPlayerSummariesService,
      final SteamUserStatsService steamUserStatsService,
      final LeaderboardService leaderboardService,
      final ReportUpdateService reportUpdateService,
      RankDistributionService rankDistributionService,
      final TitleDataService titleDataService,
      CurrentGamesService currentGamesService,
      @Value("${scribe.profile-reports.enabled}") final boolean profileReportsEnabled) {
    this.steamPlayerSummariesService = steamPlayerSummariesService;
    this.steamUserStatsService = steamUserStatsService;
    this.leaderboardService = leaderboardService;
    this.reportUpdateService = reportUpdateService;
    this.rankDistributionService = rankDistributionService;
    this.titleDataService = titleDataService;
    this.currentGamesService = currentGamesService;
    this.profileReportsEnabled = profileReportsEnabled;
  }

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_profile"})
  public PlayerProfile fetchPlayerProfile(final PlayerIdentity identity) {
    LatestPlayerReport latestPlayerReport = null;
    List<Report> reports = Collections.emptyList();
    CompletableFuture<FetchReportsResult> asyncPlayerReports = null;
    CompletableFuture<PlayerSummary> asyncPlayerSummary = null;
    CompletableFuture<Optional<Playerstats>> asyncPlayerstats = null;

    if (profileReportsEnabled) {
      asyncPlayerReports = reportUpdateService.fetchReports(identity);
    }

    if (Platform.STEAM.getName().equals(identity.getPlatform())) {
      final String steamId = identity.getPlatformAccountId();
      asyncPlayerSummary = steamPlayerSummariesService.retrievePlayerSummary(steamId);
      asyncPlayerstats = steamUserStatsService.retrievePlayerStats(steamId);
    }

    final Map<Long, SnapshotDelta> duelLeaderboardDeltas = new HashMap<>();
    leaderboardService
        .getLeaderboard(QueueType.SOLO)
        .getDeltas()
        .forEach(
            snapshotDeltas ->
                duelLeaderboardDeltas.put(
                    snapshotDeltas.getDuration(),
                    snapshotDeltas.getDeltas().get(identity.getPlayFabId())));

    final Map<Long, SnapshotDelta> teamfightLeaderboardDeltas = new HashMap<>();
    leaderboardService
        .getLeaderboard(QueueType.TEAM)
        .getDeltas()
        .forEach(
            snapshotDeltas ->
                teamfightLeaderboardDeltas.put(
                    snapshotDeltas.getDuration(),
                    snapshotDeltas.getDeltas().get(identity.getPlayFabId())));

    final Integer duelLeaderboardPosition =
        leaderboardService.getLeaderboard(QueueType.SOLO).getEntries().stream()
            .filter(entry -> entry.getPlayFabId().equals(identity.getPlayFabId()))
            .findAny()
            .map(LeaderboardEntry::getPosition)
            .orElse(null);

    final Integer teamfightLeaderboardPosition =
        leaderboardService.getLeaderboard(QueueType.TEAM).getEntries().stream()
            .filter(entry -> entry.getPlayFabId().equals(identity.getPlayFabId()))
            .findAny()
            .map(LeaderboardEntry::getPosition)
            .orElse(null);

    if (profileReportsEnabled) {
      try {
        final FetchReportsResult fetchReportsResult = asyncPlayerReports.get();
        reports = fetchReportsResult.reports();
        latestPlayerReport = fetchReportsResult.latestPlayerReport();
      } catch (InterruptedException | ExecutionException e) {
        LOG.error("Error in async call fetchReports", e);
      }
    }

    String name = identity.getName();
    String avatarUrl = null;
    String countryCode = null;
    Map<String, Long> steamPlayerStats = null;
    if (Platform.STEAM.getName().equals(identity.getPlatform())) {
      try {
        if (asyncPlayerSummary != null) {
          final PlayerSummary summary = asyncPlayerSummary.get();
          if (summary != null) {
            name = summary.name();
            avatarUrl = summary.avatarFull();
            countryCode = summary.countryCode();
          }
        }
      } catch (InterruptedException | ExecutionException e) {
        LOG.error("Error in async call retrievePlayerSummary", e);
      }

      try {
        if (asyncPlayerstats != null) {
          final Optional<Playerstats> playerstats = asyncPlayerstats.get();
          if (playerstats.isPresent()) {
            steamPlayerStats =
                playerstats.get().getStats().stream()
                    .collect(Collectors.toMap(Stat::getName, Stat::getValue));
          }
        }
      } catch (InterruptedException | ExecutionException e) {
        LOG.error("Error in async call retrievePlayerStats", e);
      }
    }

    Game game =
        identity.getTitlePlayerAccountId() != null
            ? currentGamesService.lookupGameForPlayer(identity.getTitlePlayerAccountId())
            : null;

    Double topXPercentDuel = null;
    Double topXPercentTeamfight = null;
    if (latestPlayerReport != null) {
      final Integer currentDuelRank = latestPlayerReport.getDuelRank();
      if (currentDuelRank != null)
        topXPercentDuel =
            rankDistributionService.getTopXPercentForRank(currentDuelRank, QueueType.SOLO);

      final Integer currentTeamfightRank = latestPlayerReport.getTeamfightRank();
      if (currentTeamfightRank != null)
        topXPercentTeamfight =
            rankDistributionService.getTopXPercentForRank(currentTeamfightRank, QueueType.TEAM);
    }

    return new PlayerProfile(
        identity.getPlayFabId(),
        identity.getPlatform(),
        identity.getPlatformAccountId(),
        name,
        avatarUrl,
        game,
        reports,
        steamPlayerStats,
        duelLeaderboardPosition,
        topXPercentDuel,
        teamfightLeaderboardPosition,
        topXPercentTeamfight,
        duelLeaderboardDeltas,
        teamfightLeaderboardDeltas,
        countryCode,
        titleDataService.getPlayerGlobalBanStatus(identity.getPlayFabId()),
        titleDataService.getPlayerOfficialBanStatus(identity.getPlayFabId()),
        titleDataService.getPlayerGlobalMuteStatus(identity.getPlayFabId()),
        titleDataService.getPlayerOfficialMuteStatus(identity.getPlayFabId()));
  }
}
