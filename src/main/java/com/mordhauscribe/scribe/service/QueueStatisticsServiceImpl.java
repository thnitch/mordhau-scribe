package com.mordhauscribe.scribe.service;

import com.google.common.util.concurrent.AtomicDouble;
import com.mordhauscribe.scribe.exception.PlayFabServiceException;
import com.mordhauscribe.scribe.model.QueueType;
import com.playfab.PlayFabMultiplayerModels;
import com.playfab.PlayFabMultiplayerModels.GetQueueStatisticsResult;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class QueueStatisticsServiceImpl implements QueueStatisticsService {

  public static final String QUEUE_STAT_AVERAGE = "average";
  public static final String QUEUE_STAT_PERCENTILE_50 = "percentile50";
  public static final String QUEUE_STAT_PERCENTILE_90 = "percentile90";
  public static final String QUEUE_STAT_PERCENTILE_99 = "percentile99";
  private final PlayFabService playFabService;

  private final GetQueueStatisticsResult defaultQueueStatistics;

  private final Map<QueueType, GetQueueStatisticsResult> queueStatisticsResultMap;

  private final List<QueueStatisticsService.QueueStatisticsListener> listeners;
  private final Map<QueueType, Map<String, AtomicDouble>> queueTimesMetrics;
  private final Map<QueueType, AtomicLong> queueSizesMetrics;
  private final boolean enabled;

  @Autowired
  public QueueStatisticsServiceImpl(
      final PlayFabService playFabService,
      final MeterRegistry meterRegistry,
      @Value("${scribe.queue-statistics.enabled}") final boolean enabled) {
    this.playFabService = playFabService;
    this.enabled = enabled;
    this.queueStatisticsResultMap = new HashMap<>();
    this.queueTimesMetrics = new HashMap<>();
    this.queueSizesMetrics = new HashMap<>();
    for (QueueType queueType : QueueType.values()) {
      this.queueTimesMetrics.put(queueType, new HashMap<>());
      for (String stat :
          List.of(
              QUEUE_STAT_AVERAGE,
              QUEUE_STAT_PERCENTILE_50,
              QUEUE_STAT_PERCENTILE_90,
              QUEUE_STAT_PERCENTILE_99)) {
        this.queueTimesMetrics
            .get(queueType)
            .put(
                stat,
                meterRegistry.gauge(
                    "scribe.queue.statistics.time",
                    Tags.of("queue_type", queueType.getQueueName(), "stat", stat),
                    new AtomicDouble(0)));
      }
      this.queueSizesMetrics.put(
          queueType,
          meterRegistry.gauge(
              "scribe.queue.statistics.size",
              Tags.of("queue_type", queueType.getQueueName()),
              new AtomicLong(0)));
    }

    defaultQueueStatistics = new GetQueueStatisticsResult();
    defaultQueueStatistics.NumberOfPlayersMatching = 0L;
    listeners = new ArrayList<>();
  }

  @Override
  @Scheduled(
      fixedRate = SCHEDULED_QUEUE_STATISTICS_UPDATE_RATE_SECONDS,
      timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "queue_statistics"})
  public void updateDuelQueueStatistics() {
    if (!enabled) return;
    updateQueueStatistics(QueueType.SOLO);
  }

  @Override
  @Scheduled(
      fixedRate = SCHEDULED_QUEUE_STATISTICS_UPDATE_RATE_SECONDS,
      initialDelay = SCHEDULED_QUEUE_STATISTICS_UPDATE_INITIAL_DELAY_SECONDS,
      timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "queue_statistics"})
  public void updateTeamfightQueueStatistics() {
    if (!enabled) return;
    updateQueueStatistics(QueueType.TEAM);
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "queue_statistics"})
  public GetQueueStatisticsResult getQueueStatistics(QueueType queueType) {
    return this.queueStatisticsResultMap.getOrDefault(queueType, defaultQueueStatistics);
  }

  private void updateQueueStatistics(QueueType queueType) {
    try {
      final GetQueueStatisticsResult queueStatisticsResult =
          playFabService.fetchQueueStatistics(queueType);
      queueStatisticsResultMap.put(queueType, queueStatisticsResult);

      queueSizesMetrics.get(queueType).set(queueStatisticsResult.NumberOfPlayersMatching);

      if (queueStatisticsResult.TimeToMatchStatisticsInSeconds != null) {
        final PlayFabMultiplayerModels.Statistics timeToMatchStatisticsInSeconds =
            queueStatisticsResult.TimeToMatchStatisticsInSeconds;
        queueTimesMetrics
            .get(queueType)
            .get(QUEUE_STAT_AVERAGE)
            .set(timeToMatchStatisticsInSeconds.Average);
        queueTimesMetrics
            .get(queueType)
            .get(QUEUE_STAT_PERCENTILE_50)
            .set(timeToMatchStatisticsInSeconds.Percentile50);
        queueTimesMetrics
            .get(queueType)
            .get(QUEUE_STAT_PERCENTILE_90)
            .set(timeToMatchStatisticsInSeconds.Percentile90);
        queueTimesMetrics
            .get(queueType)
            .get(QUEUE_STAT_PERCENTILE_99)
            .set(timeToMatchStatisticsInSeconds.Percentile99);
      }

      listeners.forEach(QueueStatisticsListener::onQueueStatisticsUpdate);
    } catch (PlayFabServiceException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void register(QueueStatisticsListener listener) {
    listeners.add(listener);
  }
}
