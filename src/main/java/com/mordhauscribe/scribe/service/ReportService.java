package com.mordhauscribe.scribe.service;

import com.mordhauscribe.scribe.model.PlayerHistoryEntry;
import com.mordhauscribe.scribe.model.QueueType;
import com.mordhauscribe.scribe.model.entity.PlayerIdentity;
import com.mordhauscribe.scribe.model.entity.Report;
import com.mordhauscribe.scribe.model.nativequeryresult.ReconstructedReport;
import com.mordhauscribe.scribe.repository.ReportRepository;
import io.micrometer.core.annotation.Timed;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ReportService {

  private final ReportRepository repository;
  private final int leaderboardSize;
  private final int allTimeLeaderboardSize;
  private final int peakMmrLeaderboardSize;

  @Autowired
  public ReportService(
      final ReportRepository repository,
      @Value("${scribe.report.leaderboard-size}") final int leaderboardSize,
      @Value("${scribe.report.all-time-leaderboard-size}") final int allTimeLeaderboardSize,
      @Value("${scribe.report.peak-mmr-leaderboard-size}") final int peakMmrLeaderboardSize) {
    this.repository = repository;
    this.leaderboardSize = leaderboardSize;
    this.allTimeLeaderboardSize = allTimeLeaderboardSize;
    this.peakMmrLeaderboardSize = peakMmrLeaderboardSize;
  }

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "report"})
  public List<PlayerHistoryEntry> findByPlayerIdentityAndQueueType(
      final PlayerIdentity playerIdentity, final QueueType queueType) {
    return repository.findByPlayerIdentityAndQueueType(playerIdentity, queueType);
  }

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "report"})
  public List<Report> closestBeforeOrFirstSince(
      final Instant startTime, final Set<String> playerIds, final QueueType queueType) {
    return repository.closestBeforeOrFirstSince(startTime, playerIds, queueType.name());
  }

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "report"})
  public List<ReconstructedReport> constructLeaderboard(
      final Instant date, final QueueType queueType) {
    return repository.constructLeaderboard(
        date.minus(7, ChronoUnit.DAYS), date, queueType.name(), leaderboardSize);
  }

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "report"})
  public List<ReconstructedReport> constructAllTime(final QueueType queueType) {
    return repository.constructAllTimeLeaderboard(queueType.name(), allTimeLeaderboardSize);
  }

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "report"})
  public List<ReconstructedReport> constructPeakMmr(final QueueType queueType) {
    return repository.constructPeakMmrLeaderboard(queueType.name(), peakMmrLeaderboardSize);
  }

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "report"})
  public Instant findEarliestTimestamp(final QueueType queueType) {
    return repository.findEarliestTimestamp(queueType);
  }

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "report"})
  public Map<String, Report.WinLossResult> determineWinLoss(
      Set<String> playerIds, QueueType queueType, Instant before) {
    return repository.determineWinLoss(playerIds, queueType.name(), before).stream()
        .collect(Collectors.toMap(Report.WinLossResult::playerId, Function.identity()));
  }
}
