package com.mordhauscribe.scribe.service;

import com.mordhauscribe.scribe.dto.ResolveVanityUrlResponse;
import com.mordhauscribe.scribe.exception.PlayerNotFoundException;
import com.mordhauscribe.scribe.exception.SteamWebApiClientServiceException;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClient.ResponseSpec;

@Service
public class SteamVanityUrlService implements HealthIndicator {
  public static final String RESOLVE_VANITY_URL_ENDPOINT = "ISteamUser/ResolveVanityURL/v1/";
  private static final Logger LOG = LogManager.getLogger();
  private final WebClient client;
  private final String apiKey;
  private final Counter counterApiCallsVanitySearch;
  private final Counter counterApiCallsErrorsVanitySearch;
  private Health health;

  @Autowired
  public SteamVanityUrlService(
      @Value("${scribe.steam-web-api.key}") final String apiKey, MeterRegistry meterRegistry) {
    this.apiKey = apiKey;

    client =
        WebClient.builder()
            .baseUrl("https://api.steampowered.com/")
            .defaultHeader("Accept", "application/json")
            .build();

    final Tags commonApiCallsTags =
        Tags.of("api", "steam", "api_type", "web", "service", "steam_vanity_id_search");
    counterApiCallsVanitySearch =
        Counter.builder("scribe.api.calls")
            .description("calls to a downstream api")
            .tags(commonApiCallsTags.and("exception", "none"))
            .register(meterRegistry);
    counterApiCallsErrorsVanitySearch =
        Counter.builder("scribe.api.calls.errors")
            .description("errors in calls to a downstream api")
            .tags(commonApiCallsTags.and("exception", "SteamApiException"))
            .register(meterRegistry);
  }

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "steam_vanity_id_search"})
  public String resolveVanityUrl(final String playerName) {
    final ResponseSpec responseSpec =
        client
            .get()
            .uri(
                uriBuilder ->
                    uriBuilder
                        .path(RESOLVE_VANITY_URL_ENDPOINT)
                        .queryParam("key", this.apiKey)
                        .queryParam("vanityurl", playerName)
                        .build())
            .accept(MediaType.APPLICATION_JSON)
            .retrieve();

    final ResolveVanityUrlResponse response;
    try {
      counterApiCallsVanitySearch.increment();
      response = responseSpec.bodyToMono(ResolveVanityUrlResponse.class).block();
    } catch (final Exception e) {
      health = Health.down().withException(e).build();
      LOG.error(e.getStackTrace());
      throw new PlayerNotFoundException(playerName);
    }

    if (response.getResponse().getSuccess() == 1) {
      health = Health.up().build();
      return response.getResponse().getSteamid();
    } else if (response.getResponse().getSuccess() == 42) {
      health = Health.up().build();
      throw new PlayerNotFoundException(playerName);
    }

    final String errorMessage = String.format("Unexpected response: %s", response);
    final SteamWebApiClientServiceException e = new SteamWebApiClientServiceException(errorMessage);
    health = Health.down().withException(e).build();
    LOG.error(e);
    counterApiCallsErrorsVanitySearch.increment();
    throw new IllegalStateException();
  }

  @Override
  public Health health() {
    return this.health;
  }
}
