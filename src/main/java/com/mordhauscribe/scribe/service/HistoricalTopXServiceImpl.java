package com.mordhauscribe.scribe.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.mordhauscribe.scribe.model.HistoricalTopX;
import com.mordhauscribe.scribe.model.Leaderboard;
import com.mordhauscribe.scribe.model.LeaderboardEntry;
import com.mordhauscribe.scribe.model.PlayerHistoryEntry;
import com.mordhauscribe.scribe.model.QueueType;
import com.mordhauscribe.scribe.model.entity.PlayerIdentity;
import io.micrometer.core.annotation.Timed;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@DependsOn({"leaderboardServiceImpl"})
public class HistoricalTopXServiceImpl implements HistoricalTopXService {
  private static final Logger LOG = LogManager.getLogger();

  private final Map<QueueType, HistoricalTopX> historicalTopXs = new HashMap<>();

  private final ReportService reportService;
  private final PlayerIdentityService playerIdentityService;
  private final LeaderboardService leaderboardService;
  private final int topXCount;

  @Autowired
  public HistoricalTopXServiceImpl(
      final ReportService reportService,
      final PlayerIdentityService playerIdentityService,
      final LeaderboardService leaderboardService,
      @Value("${scribe.historical-top-x-count}") final int topXCount) {
    this.reportService = reportService;
    this.playerIdentityService = playerIdentityService;
    this.leaderboardService = leaderboardService;
    this.topXCount = topXCount;
  }

  @Override
  @Async
  @Scheduled(fixedDelay = SCHEDULED_TOP_X_UPDATE_DELAY_MINUTES, timeUnit = TimeUnit.MINUTES)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "historicaltopx"})
  public void updateDuelHistoricalTopX() {
    updateHistoricalTopX(QueueType.SOLO);
  }

  @Override
  @Async
  @Scheduled(fixedDelay = SCHEDULED_TOP_X_UPDATE_DELAY_MINUTES, timeUnit = TimeUnit.MINUTES)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "historicaltopx"})
  public void updateTeamHistoricalTopX() {
    updateHistoricalTopX(QueueType.TEAM);
  }

  private void updateHistoricalTopX(final QueueType queueType) {
    LOG.trace(String.format("Updating %s historical top X", queueType));
    final Leaderboard latestLeaderboard = leaderboardService.getLeaderboard(queueType);
    final List<LeaderboardEntry> latestLeaderboardTopX =
        latestLeaderboard
            .getEntries()
            .subList(0, Math.min(latestLeaderboard.getEntries().size(), topXCount));

    final List<String> names =
        latestLeaderboardTopX.stream()
            .map(
                entry -> {
                  if (entry.getPlayerSummary() != null) return entry.getPlayerSummary().name();
                  else return "";
                })
            .collect(Collectors.toList());

    final List<PlayerIdentity> identities =
        latestLeaderboardTopX.stream()
            .map(entry -> new PlayerIdentity(entry.getPlayFabId(), null))
            .toList();

    final List<List<PlayerHistoryEntry>> leaderboardSnapshots =
        identities.stream()
            .map(identity -> reportService.findByPlayerIdentityAndQueueType(identity, queueType))
            .collect(Collectors.toList());

    final HistoricalTopX historicalTopX =
        new HistoricalTopX(latestLeaderboard.getTimestamp(), names, leaderboardSnapshots);
    this.historicalTopXs.put(queueType, historicalTopX);
    LOG.trace(String.format("Updated %s historical top X", queueType));
  }

  @Override
  public HistoricalTopX getHistoricalTopX(final QueueType queueType) {
    return historicalTopXs.get(queueType);
  }
}
