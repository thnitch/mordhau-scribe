package com.mordhauscribe.scribe.service;

import com.mordhauscribe.scribe.exception.PlayFabServiceException;
import com.mordhauscribe.scribe.model.Game;
import com.mordhauscribe.scribe.model.Leaderboard;
import com.mordhauscribe.scribe.model.PlayerStatisticsResult;
import com.mordhauscribe.scribe.model.QueueType;
import com.mordhauscribe.scribe.model.TitleData;
import com.mordhauscribe.scribe.model.entity.PlayerIdentity;
import com.playfab.PlayFabClientModels;
import com.playfab.PlayFabClientModels.GenericPlayFabIdPair;
import com.playfab.PlayFabMultiplayerModels;
import io.micrometer.core.annotation.Timed;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

public interface PlayFabService {
  int SCHEDULED_SESSION_TICKET_UPDATE_DELAY_HOURS = 23;
  int SCHEDULED_GAME_VERSION_UPDATE_DELAY_HOURS = 24;

  @Async
  @Scheduled(
      fixedRate = SCHEDULED_SESSION_TICKET_UPDATE_DELAY_HOURS,
      initialDelay = SCHEDULED_SESSION_TICKET_UPDATE_DELAY_HOURS,
      timeUnit = TimeUnit.HOURS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  void updateSessionTicket() throws PlayFabServiceException;

  @Async
  @Scheduled(
      fixedRate = SCHEDULED_GAME_VERSION_UPDATE_DELAY_HOURS,
      initialDelay = SCHEDULED_GAME_VERSION_UPDATE_DELAY_HOURS,
      timeUnit = TimeUnit.HOURS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  void updateGameVersion() throws PlayFabServiceException;

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  TitleData fetchTitleData() throws PlayFabServiceException;

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  Map<String, Game> fetchCurrentGames(PlayFabClientModels.CollectionFilter filter)
      throws PlayFabServiceException;

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  PlayFabMultiplayerModels.GetQueueStatisticsResult fetchQueueStatistics(QueueType queueType)
      throws PlayFabServiceException;

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  Leaderboard fetchLeaderboard(QueueType queueType) throws PlayFabServiceException;

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  PlayerStatisticsResult fetchPlayerStatistics(String playFabId) throws PlayFabServiceException;

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  List<PlayerIdentity> fetchProfilesByTitlePlayerAccountIds(Set<String> titlePlayerAccountIds)
      throws PlayFabServiceException;

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  Optional<PlayerIdentity> checkPlayerExists(String playFabId) throws PlayFabServiceException;

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  List<GenericPlayFabIdPair> fetchPlayFabIdsByGenericIds(
      ArrayList<PlayFabClientModels.GenericServiceId> genericIds) throws PlayFabServiceException;

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "playfab"})
  List<PlayFabClientModels.SteamPlayFabIdPair> fetchPlayFabIdsBySteamIds(List<String> steamIds)
      throws PlayFabServiceException;
}
