package com.mordhauscribe.scribe.service;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import com.mordhauscribe.scribe.exception.PlayFabServiceException;
import com.mordhauscribe.scribe.model.TitleData;
import io.micrometer.core.annotation.Timed;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class TitleDataServiceImpl implements TitleDataService, HealthIndicator {

  private static final Logger LOG = LogManager.getLogger();
  private final PlayFabService playFabService;
  private TitleData titleData =
      new TitleData(new HashMap<>(), new HashMap<>(), new HashMap<>(), new HashMap<>());
  private Health health;

  @Autowired
  public TitleDataServiceImpl(final PlayFabService playFabService) {
    this.playFabService = playFabService;
  }

  @Override
  @Async
  @Scheduled(fixedDelay = SCHEDULED_TITLE_DATA_UPDATE_DELAY_MINUTES, timeUnit = TimeUnit.MINUTES)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "title_data"})
  public void updateTitleData() {
    try {
      this.titleData = playFabService.fetchTitleData();
      health = Health.up().build();
    } catch (final PlayFabServiceException e) {
      LOG.error(e);
      health = Health.down().withException(e).build();
    }
  }

  @Override
  public boolean getPlayerBanStatus(final String playFabId) {
    return this.getPlayerGlobalBanStatus(playFabId) != null
        || this.getPlayerOfficialBanStatus(playFabId) != null;
  }

  @Override
  public Long getPlayerGlobalBanStatus(final String playFabId) {
    return titleData.globalBannedPlayers().get(playFabId);
  }

  @Override
  public Long getPlayerOfficialBanStatus(final String playFabId) {
    return titleData.officialBannedPlayers().get(playFabId);
  }

  @Override
  public Long getPlayerGlobalMuteStatus(final String playFabId) {
    return titleData.globalMutedPlayers().get(playFabId);
  }

  @Override
  public Long getPlayerOfficialMuteStatus(final String playFabId) {
    return titleData.officialMutedPlayers().get(playFabId);
  }

  @Override
  public Health health() {
    return this.health;
  }
}
