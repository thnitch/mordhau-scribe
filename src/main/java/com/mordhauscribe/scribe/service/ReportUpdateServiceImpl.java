package com.mordhauscribe.scribe.service;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import com.mordhauscribe.scribe.exception.PlayFabServiceException;
import com.mordhauscribe.scribe.model.PlayerStatisticsResult;
import com.mordhauscribe.scribe.model.QueueType;
import com.mordhauscribe.scribe.model.UniquePriorityBlockingQueue;
import com.mordhauscribe.scribe.model.UniquePriorityBlockingQueue.UniquePriorityBlockingQueueItem;
import com.mordhauscribe.scribe.model.entity.LatestPlayerReport;
import com.mordhauscribe.scribe.model.entity.PlayerIdentity;
import com.mordhauscribe.scribe.model.entity.Report;
import com.mordhauscribe.scribe.repository.PlayerIdentityRepository;
import com.mordhauscribe.scribe.repository.ReportRepository;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import jakarta.annotation.PostConstruct;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.InvalidDataAccessResourceUsageException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

@Service
public class ReportUpdateServiceImpl implements ReportUpdateService {
  private static final Logger LOG = LogManager.getLogger();
  private final PlayFabService playFabService;
  private final TransactionTemplate template;
  private final Instant serviceStartTime;
  private final ReportRepository reportRepository;
  private final PlayerIdentityRepository playerIdentityRepository;
  private final Map<String, Counter> playerReportSavedCounters;
  private final Map<Integer, Counter> updateRetryCounters;
  private final Map<Integer, Counter> successfulUpdateCounters;
  private final Map<Integer, Counter> cancelledRetryCounters;
  private final UniquePriorityBlockingQueue<UniquePriorityBlockingQueueItem<PlayerIdentity>>
      updateQueue;
  private final Map<String, UniquePriorityBlockingQueueItem<PlayerIdentity>>
      updateQueueRetryBacklog;
  private final boolean profileReportsEnabled;
  private final int updateQueueProcessingBatchSize;
  private final MeterRegistry meterRegistry;

  @Autowired
  public ReportUpdateServiceImpl(
      final PlayFabService playFabService,
      TransactionTemplate template,
      final ReportRepository reportRepository,
      PlayerIdentityRepository playerIdentityRepository,
      final MeterRegistry meterRegistry,
      @Value("${scribe.profile-reports.enabled}") final boolean profileReportsEnabled,
      @Value("${scribe.profile-reports.update-per-second-batch-size}")
          final int updateQueueProcessingBatchSize) {
    this.playFabService = playFabService;
    this.template = template;
    this.profileReportsEnabled = profileReportsEnabled;
    this.updateQueueProcessingBatchSize = updateQueueProcessingBatchSize;
    this.serviceStartTime = Instant.now();
    this.reportRepository = reportRepository;
    this.playerIdentityRepository = playerIdentityRepository;

    this.meterRegistry = meterRegistry;

    updateQueue =
        meterRegistry.gaugeCollectionSize(
            "scribe.queue",
            Tags.of("service", "report_update", "type", "new_reports"),
            new UniquePriorityBlockingQueue<>(
                new PriorityBlockingQueue<>(
                    500, new UniquePriorityBlockingQueue.PriorityComparator())));
    updateQueueRetryBacklog =
        meterRegistry.gaugeMapSize(
            "scribe.queue",
            Tags.of("service", "report_update", "type", "new_reports_retry_backlog"),
            new HashMap<>());

    updateRetryCounters = new HashMap<>();
    successfulUpdateCounters = new HashMap<>();
    cancelledRetryCounters = new HashMap<>();
    playerReportSavedCounters = new HashMap<>();

    playerReportSavedCounters.put(
        "has_gaps_" + QueueType.SOLO.getQueueName(),
        Counter.builder("scribe.player.report.saved")
            .tags("accuracy", "has_gaps", "queue_type", QueueType.SOLO.getQueueName())
            .description("player reports saved")
            .register(meterRegistry));
    playerReportSavedCounters.put(
        "gapless_" + QueueType.SOLO.getQueueName(),
        Counter.builder("scribe.player.report.saved")
            .tags("accuracy", "gapless", "queue_type", QueueType.SOLO.getQueueName())
            .description("player reports saved")
            .register(meterRegistry));
    playerReportSavedCounters.put(
        "has_gaps_" + QueueType.TEAM.getQueueName(),
        Counter.builder("scribe.player.report.saved")
            .tags("accuracy", "has_gaps", "queue_type", QueueType.TEAM.getQueueName())
            .description("player reports saved")
            .register(meterRegistry));
    playerReportSavedCounters.put(
        "gapless_" + QueueType.TEAM.getQueueName(),
        Counter.builder("scribe.player.report.saved")
            .tags("accuracy", "gapless", "queue_type", QueueType.TEAM.getQueueName())
            .description("player reports saved")
            .register(meterRegistry));
  }

  @PostConstruct
  protected void initialize() {
    final List<PlayerIdentity> identitiesInNeedOfUpdate =
        playerIdentityRepository.findPlayerIdentitiesInNeedOfUpdate();

    updateQueue.addAll(
        identitiesInNeedOfUpdate.stream()
            .map(item -> new UniquePriorityBlockingQueueItem<>(INITIAL_UPDATE_PRIORITY, 0, item))
            .collect(Collectors.toSet()));
    LOG.debug("Queued updates for {} players", identitiesInNeedOfUpdate.size());
  }

  @Override
  @Async
  @Scheduled(fixedDelay = SCHEDULED_QUEUE_PROCESSING_DELAY_SECONDS, timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "report_update"})
  public void processQueueItems() throws InterruptedException {
    if (!profileReportsEnabled) return;

    final int batchSize = Math.min(updateQueue.size(), updateQueueProcessingBatchSize);
    for (int i = 0; i < batchSize; i++) {
      final UniquePriorityBlockingQueueItem<PlayerIdentity> queueItem =
          updateQueue.poll(10, TimeUnit.SECONDS);
      if (queueItem == null) return;

      final PlayerIdentity item = queueItem.getItem();
      try {
        final Optional<Map<QueueType, Report>> updateResult =
            fetchReport(item, queueItem.getPriority());

        if (!updateRetryCounters.containsKey(queueItem.getTimesRetried()))
          updateRetryCounters.put(
              queueItem.getTimesRetried(),
              Counter.builder("scribe.player.report.updates")
                  .description("attempted updates")
                  .tags("retries", Integer.toString(queueItem.getTimesRetried()))
                  .register(meterRegistry));
        updateRetryCounters.get(queueItem.getTimesRetried()).increment();

        // requeue if retry is desired and there was an update error
        // (or no change, this covers cases where the PlayFab API is lagging behind)
        if (queueItem.getRetries() > 0
            && (updateResult.isEmpty()
                || (updateResult.get().isEmpty()
                    && !updateQueueRetryBacklog.containsKey(item.getPlayFabId())))) {
          queueItem.decrementRetries();
          queueItem.setPriority(RETRIED_UPDATE_PRIORITY);

          final Duration retryInXSeconds =
              Duration.ofSeconds(
                  (long) queueItem.getTimesRetried()
                      * queueItem.getTimesRetried()
                      * RETRY_EXPONENTIAL_BACKOFF_WAIT_SECONDS);
          queueItem.setRetryAfter(Instant.now().plus(retryInXSeconds));
          updateQueueRetryBacklog.put(item.getPlayFabId(), queueItem);
        } else if (updateResult.isPresent() && !updateResult.get().isEmpty()) {
          if (!successfulUpdateCounters.containsKey(queueItem.getTimesRetried()))
            successfulUpdateCounters.put(
                queueItem.getTimesRetried(),
                Counter.builder("scribe.player.report.updates.successful")
                    .description("successful updates")
                    .tags("retries", Integer.toString(queueItem.getTimesRetried()))
                    .register(meterRegistry));
          successfulUpdateCounters.get(queueItem.getTimesRetried()).increment();
        }
      } catch (Exception e) {
        LOG.error("Failed to process player in queue '{}'", item.getPlayFabId(), e);
        updateQueue.add(queueItem);
      }
    }
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "report_update"})
  public void queueUpdates(Set<PlayerIdentity> identities, int priority, int retries) {
    final Instant now = Instant.now();
    for (PlayerIdentity identity : identities) {
      final UniquePriorityBlockingQueueItem<PlayerIdentity> cancelledQueueItem =
          updateQueueRetryBacklog.remove(identity.getPlayFabId());
      if (cancelledQueueItem != null) {
        final int timesRetried = cancelledQueueItem.getTimesRetried();
        if (!cancelledRetryCounters.containsKey(timesRetried))
          cancelledRetryCounters.put(
              timesRetried,
              Counter.builder("scribe.player.report.updates.cancelled")
                  .description("updates cancelled in favor of new queued update")
                  .tags("retries", Integer.toString(timesRetried))
                  .register(meterRegistry));
        cancelledRetryCounters.get(timesRetried).increment();
      }

      final UniquePriorityBlockingQueueItem<PlayerIdentity> newQueueItem =
          new UniquePriorityBlockingQueueItem<>(priority, retries, identity);
      newQueueItem.setRetryAfter(
          now.plus(Duration.of(INITIAL_REPORT_UPDATE_WAIT_SECONDS, ChronoUnit.SECONDS)));
      updateQueueRetryBacklog.put(identity.getPlayFabId(), newQueueItem);
    }
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "report_update"})
  public void queueUpdates(Set<PlayerIdentity> identities) {
    queueUpdates(identities, REGULAR_UPDATE_PRIORITY, REGULAR_UDPATE_RETRIES);
  }

  @Override
  @Async
  @Scheduled(fixedDelay = RETRY_CHECK_RATE_SECONDS, timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "report_update"})
  public void processRetryBacklogQueueItems() {
    final Instant now = Instant.now();

    final Set<UniquePriorityBlockingQueueItem<PlayerIdentity>> itemsToReaddToQueue =
        this.updateQueueRetryBacklog.values().stream()
            .filter(item -> now.isAfter(item.getRetryAfter()))
            .collect(Collectors.toSet());

    for (UniquePriorityBlockingQueueItem<PlayerIdentity> queueItem : itemsToReaddToQueue) {
      updateQueueRetryBacklog.remove(queueItem.getItem().getPlayFabId());
      updateQueue.add(queueItem);
    }
  }

  public record FetchReportsResult(LatestPlayerReport latestPlayerReport, List<Report> reports) {}

  @Override
  @Async
  @Transactional
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "report_update"})
  public CompletableFuture<FetchReportsResult> fetchReports(final PlayerIdentity identity) {

    final LatestPlayerReport latestPlayerReport = identity.getLatestPlayerReport();
    final List<Report> reports =
        reportRepository.findByPlayerIdentityOrderByGameTimestamp(identity);

    if (latestPlayerReport == null
        || latestPlayerReport.getUpdatedPlayerReportAt().isBefore(serviceStartTime)) {

      updateQueue.remove(identity);
      final Optional<Map<QueueType, Report>> playerReportUpdateResult = fetchReport(identity, 0);

      if (playerReportUpdateResult.isPresent() && !playerReportUpdateResult.get().isEmpty()) {
        reports.addAll(playerReportUpdateResult.get().values());
      }
    }

    return CompletableFuture.completedFuture(new FetchReportsResult(latestPlayerReport, reports));
  }

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "report_update"})
  public Optional<Map<QueueType, Report>> fetchReport(
      final PlayerIdentity identity, final int priority) {
    return template.execute(
        status -> {
          final PlayerStatisticsResult playerStatisticsResult;
          try {
            playerStatisticsResult = playFabService.fetchPlayerStatistics(identity.getPlayFabId());

            final Map<QueueType, Report> latestReports =
                reportRepository.latest(identity.getPlayFabId()).stream()
                    .collect(toMap(Report::getQueueType, identity()));
            LatestPlayerReport latestPlayerReport = playerStatisticsResult.latestPlayerReport();

            if (playerStatisticsResult.updatedPlayerIdentity().isPresent())
              identity.applyUpdateFromIdentiy(playerStatisticsResult.updatedPlayerIdentity().get());

            LatestPlayerReport previous = identity.getLatestPlayerReport();
            identity.setLatestPlayerReport(latestPlayerReport);
            try {
              playerIdentityRepository.save(identity);
            } catch (InvalidDataAccessResourceUsageException e) {
              LOG.error(
                  "Failed to persist player {} name: {}",
                  identity.getPlayFabId(),
                  identity.getName());
            }

            Map<QueueType, Report> newReports =
                latestPlayerReport.generateReports(identity, previous, latestReports);
            reportRepository.saveAll(newReports.values());
            if (newReports.isEmpty()) {
              return Optional.of(newReports);
            }

            if (previous != null) {
              final int duelGamesGapSize =
                  latestPlayerReport.getDuelRankSamples() - previous.getDuelRankSamples();
              final int teamfightGamesGapSize =
                  latestPlayerReport.getTeamfightRankSamples() - previous.getTeamfightRankSamples();

              if (duelGamesGapSize > 0) {
                boolean duelTrackingFailure =
                    hasTrackingFailed(duelGamesGapSize, previous.getDuelRankTimestamp());
                playerReportSavedCounters
                    .get(
                        (duelTrackingFailure ? "has_gaps_" : "gapless_")
                            + QueueType.SOLO.getQueueName())
                    .increment();
              }
              if (teamfightGamesGapSize > 0) {
                boolean teamfightTrackingFailure =
                    hasTrackingFailed(teamfightGamesGapSize, previous.getTeamfightRankTimestamp());
                playerReportSavedCounters
                    .get(
                        (teamfightTrackingFailure ? "has_gaps_" : "gapless_")
                            + QueueType.TEAM.getQueueName())
                    .increment();
              }

              return Optional.of(newReports);
            }
          } catch (PlayFabServiceException e) {
            LOG.error(e);
          }
          return Optional.empty();
        });
  }

  private boolean hasTrackingFailed(final int gapSize, final Instant previous) {
    // if previous report is no older than 30m and more than one game has taken place
    // consider this a tracking failure
    return gapSize > 1
        && previous != null
        && Duration.between(Instant.now(), previous).compareTo(Duration.of(30, ChronoUnit.MINUTES))
            < 0;
  }
}
