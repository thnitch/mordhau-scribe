package com.mordhauscribe.scribe.service;

import com.mordhauscribe.scribe.model.QueueType;
import com.playfab.PlayFabMultiplayerModels;
import io.micrometer.core.annotation.Timed;
import java.util.concurrent.TimeUnit;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

public interface QueueStatisticsService {
  int SCHEDULED_QUEUE_STATISTICS_UPDATE_RATE_SECONDS = 180;
  int SCHEDULED_QUEUE_STATISTICS_UPDATE_INITIAL_DELAY_SECONDS = 90;

  @Async
  @Scheduled(
      fixedRate = SCHEDULED_QUEUE_STATISTICS_UPDATE_RATE_SECONDS,
      timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "queue_statistics"})
  void updateDuelQueueStatistics();

  @Async
  @Scheduled(
      fixedRate = SCHEDULED_QUEUE_STATISTICS_UPDATE_RATE_SECONDS,
      initialDelay = SCHEDULED_QUEUE_STATISTICS_UPDATE_INITIAL_DELAY_SECONDS,
      timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "queue_statistics"})
  void updateTeamfightQueueStatistics();

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "queue_statistics"})
  PlayFabMultiplayerModels.GetQueueStatisticsResult getQueueStatistics(QueueType queueType);
  
  void register(QueueStatisticsListener listener);
  interface QueueStatisticsListener {
    void onQueueStatisticsUpdate();
  }
}
