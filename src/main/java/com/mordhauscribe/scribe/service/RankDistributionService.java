package com.mordhauscribe.scribe.service;

import com.mordhauscribe.scribe.model.QueueType;
import io.micrometer.core.annotation.Timed;
import java.util.concurrent.TimeUnit;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

public interface RankDistributionService {
  int SCHEDULED_RANK_DISTRIBUTION_UPDATE_RATE_MINUTES = 60;

  double getTopXPercentForRank(int rank, QueueType queueType);

  @Async
  @Scheduled(
      fixedRate = SCHEDULED_RANK_DISTRIBUTION_UPDATE_RATE_MINUTES,
      timeUnit = TimeUnit.MINUTES)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "rank_distribution"})
  void updateDuelRankDistribution();

  @Async
  @Scheduled(
      fixedRate = SCHEDULED_RANK_DISTRIBUTION_UPDATE_RATE_MINUTES,
      timeUnit = TimeUnit.MINUTES)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "rank_distribution"})
  void updateTeamfightRankDistribution();
}
