package com.mordhauscribe.scribe.service;

import java.util.concurrent.TimeUnit;

import com.mordhauscribe.scribe.model.HistoricalTopX;
import com.mordhauscribe.scribe.model.QueueType;
import io.micrometer.core.annotation.Timed;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

public interface HistoricalTopXService {
  int SCHEDULED_TOP_X_UPDATE_DELAY_MINUTES = 3;

  @Async
  @Scheduled(fixedDelay = SCHEDULED_TOP_X_UPDATE_DELAY_MINUTES, timeUnit = TimeUnit.MINUTES)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "historicaltopx"})
  void updateDuelHistoricalTopX();

  @Async
  @Scheduled(fixedDelay = SCHEDULED_TOP_X_UPDATE_DELAY_MINUTES, timeUnit = TimeUnit.MINUTES)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "historicaltopx"})
  void updateTeamHistoricalTopX();

  HistoricalTopX getHistoricalTopX(QueueType queueType);
}
