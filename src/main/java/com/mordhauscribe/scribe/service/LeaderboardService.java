package com.mordhauscribe.scribe.service;

import com.mordhauscribe.scribe.model.Leaderboard;
import com.mordhauscribe.scribe.model.QueueType;
import io.micrometer.core.annotation.Timed;
import java.time.Instant;
import java.util.concurrent.TimeUnit;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

public interface LeaderboardService {
  int SCHEDULED_LEADERBOARD_UPDATE_DELAY_SECONDS = 60;
  int SCHEDULED_EARLIEST_DELTA_UPDATE_DELAY_MINUTES = 60;

  int SCHEDULED_UNOFFICIAL_LEADERBOARD_INITIAL_DELAY_OFFSET_SECONDS = 5;

  int SCHEDULED_DUEL_ALL_TIME_LEADERBOARD_INITIAL_DELAY_SECONDS = 20;
  int SCHEDULED_DUEL_ALL_TIME_LEADERBOARD_INITIAL_DELAY_MILLIS =
      SCHEDULED_DUEL_ALL_TIME_LEADERBOARD_INITIAL_DELAY_SECONDS * 1000;

  int SCHEDULED_TEAM_ALL_TIME_LEADERBOARD_INITIAL_DELAY_SECONDS =
      SCHEDULED_DUEL_ALL_TIME_LEADERBOARD_INITIAL_DELAY_SECONDS
          + SCHEDULED_UNOFFICIAL_LEADERBOARD_INITIAL_DELAY_OFFSET_SECONDS;
  int SCHEDULED_TEAM_ALL_TIME_LEADERBOARD_INITIAL_DELAY_MILLIS =
      SCHEDULED_TEAM_ALL_TIME_LEADERBOARD_INITIAL_DELAY_SECONDS * 1000;

  int SCHEDULED_DUEL_PEAK_MMR_LEADERBOARD_INITIAL_DELAY_SECONDS =
      SCHEDULED_TEAM_ALL_TIME_LEADERBOARD_INITIAL_DELAY_SECONDS
          + SCHEDULED_UNOFFICIAL_LEADERBOARD_INITIAL_DELAY_OFFSET_SECONDS;
  int SCHEDULED_DUEL_PEAK_MMR_LEADERBOARD_INITIAL_DELAY_MILLIS =
      SCHEDULED_DUEL_PEAK_MMR_LEADERBOARD_INITIAL_DELAY_SECONDS * 1000;

  int SCHEDULED_TEAM_PEAK_MMR_LEADERBOARD_INITIAL_DELAY_SECONDS =
      SCHEDULED_DUEL_PEAK_MMR_LEADERBOARD_INITIAL_DELAY_SECONDS
          + SCHEDULED_UNOFFICIAL_LEADERBOARD_INITIAL_DELAY_OFFSET_SECONDS;
  int SCHEDULED_TEAM_PEAK_MMR_LEADERBOARD_INITIAL_DELAY_MILLIS =
      SCHEDULED_TEAM_PEAK_MMR_LEADERBOARD_INITIAL_DELAY_SECONDS * 1000;

  int SCHEDULED_UNOFFICIAL_LEADERBOARD_UPDATE_DELAY_SECONDS = 1800;
  int SCHEDULED_UNOFFICIAL_LEADERBOARD_UPDATE_DELAY_MILLIS =
      SCHEDULED_UNOFFICIAL_LEADERBOARD_UPDATE_DELAY_SECONDS * 1000;

  Leaderboard getLeaderboard(QueueType queueType);

  @Async
  @Scheduled(
      fixedDelay = SCHEDULED_LEADERBOARD_UPDATE_DELAY_SECONDS,
      initialDelay = SCHEDULED_LEADERBOARD_UPDATE_DELAY_SECONDS,
      timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "leaderboard"})
  void updateDuelLeaderboard();

  @Async
  @Scheduled(
      fixedDelay = SCHEDULED_LEADERBOARD_UPDATE_DELAY_SECONDS,
      initialDelay = SCHEDULED_LEADERBOARD_UPDATE_DELAY_SECONDS,
      timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "leaderboard"})
  void updateTeamLeaderboard();

  Leaderboard getAllTimeLeaderboard(QueueType queueType);

  @Async
  @Scheduled(
      initialDelay = SCHEDULED_DUEL_ALL_TIME_LEADERBOARD_INITIAL_DELAY_MILLIS,
      fixedDelay = SCHEDULED_UNOFFICIAL_LEADERBOARD_UPDATE_DELAY_MILLIS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "leaderboard"})
  void updateDuelAllTimeLeaderboard();

  @Async
  @Scheduled(
      initialDelay = SCHEDULED_TEAM_ALL_TIME_LEADERBOARD_INITIAL_DELAY_MILLIS,
      fixedDelay = SCHEDULED_UNOFFICIAL_LEADERBOARD_UPDATE_DELAY_MILLIS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "leaderboard"})
  void updateTeamAllTimeLeaderboard();

  Leaderboard getPeakMmrLeaderboard(QueueType queueType);

  @Async
  @Scheduled(
      initialDelay = SCHEDULED_DUEL_PEAK_MMR_LEADERBOARD_INITIAL_DELAY_MILLIS,
      fixedDelay = SCHEDULED_UNOFFICIAL_LEADERBOARD_UPDATE_DELAY_MILLIS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "leaderboard"})
  void updateDuelPeakMmrLeaderboard();

  @Async
  @Scheduled(
      initialDelay = SCHEDULED_TEAM_PEAK_MMR_LEADERBOARD_INITIAL_DELAY_MILLIS,
      fixedDelay = SCHEDULED_UNOFFICIAL_LEADERBOARD_UPDATE_DELAY_MILLIS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "leaderboard"})
  void updateTeamPeakMmrLeaderboard();

  @Scheduled(
      fixedRate = SCHEDULED_EARLIEST_DELTA_UPDATE_DELAY_MINUTES,
      initialDelay = SCHEDULED_EARLIEST_DELTA_UPDATE_DELAY_MINUTES,
      timeUnit = TimeUnit.MINUTES)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "leaderboard"})
  void updateAvailableDeltas();

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "leaderboard"})
  Leaderboard constructLeaderboard(QueueType queueType, Instant at);
}
