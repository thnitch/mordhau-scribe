package com.mordhauscribe.scribe.service;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

import com.mordhauscribe.scribe.model.QueueType;
import com.mordhauscribe.scribe.model.SnapshotDelta;
import com.mordhauscribe.scribe.model.SnapshotDeltas;
import com.mordhauscribe.scribe.model.entity.Report;
import com.mordhauscribe.scribe.model.entity.Report.WinLossResult;
import com.mordhauscribe.scribe.model.nativequeryresult.ReconstructedReport;
import io.micrometer.core.annotation.Timed;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeltaService {

  public static final int LEADERBOARD_SIZE = 100;
  private final ReportService reportService;

  @Autowired
  public DeltaService(final ReportService reportService) {
    this.reportService = reportService;
  }

  @Timed(
    value = "scribe.service.time",
    description = "Time taken for a service method",
    extraTags = {"service", "delta"})
  public Optional<SnapshotDeltas> calculateDelta(
      final List<ReconstructedReport> current,
      final Instant currentTime,
      final long duration,
      final QueueType queueType) {

    final Instant referenceTime = currentTime.minusSeconds(duration);
    final Set<String> playerIds =
        current.stream().map(ReconstructedReport::getPlayFabId).collect(toSet());

    final Map<String, WinLossResult> winLossAtReferenceTime =
        reportService.determineWinLoss(playerIds, queueType, referenceTime);
    final Map<String, WinLossResult> winLossNow =
        reportService.determineWinLoss(playerIds, queueType, currentTime);

    final Map<String, ReconstructedReport> previousLeaderboard =
        reportService.constructLeaderboard(referenceTime, queueType).stream()
            .collect(toMap(ReconstructedReport::getPlayFabId, identity()));

    // Return delta with empty entries if no snapshot within delta time frame is available
    if (previousLeaderboard.isEmpty()) {
      return Optional.of(new SnapshotDeltas(referenceTime, currentTime, duration));
    }

    final Set<String> playersNotInReference =
        playerIds.stream()
            .filter(playFabId -> !previousLeaderboard.containsKey(playFabId))
            .collect(toSet());

    final Map<String, Report> closestEntriesBeforeOrFirstSince =
        reportService
            .closestBeforeOrFirstSince(referenceTime, playersNotInReference, queueType)
            .stream()
            .collect(toMap(entry -> entry.getPlayerIdentity().getPlayFabId(), identity()));

    final Map<String, SnapshotDelta> deltas = new HashMap<>();

    current.stream()
        .filter(entry -> entry.getMmr() != null)
        .forEach(
            entry -> {
              final String playerId = entry.getPlayFabId();

              boolean newEntry = true;

              int winsBefore = 0;
              int lossesBefore = 0;
              int winsNow = 0;
              int lossesNow = 0;

              final WinLossResult winLossBefore = winLossAtReferenceTime.get(playerId);
              if (winLossBefore != null) {
                winsBefore = winLossBefore.wins();
                lossesBefore = winLossBefore.losses();
              }

              final WinLossResult winLossAfter = winLossNow.get(playerId);
              if (winLossAfter != null) {
                winsNow = winLossAfter.wins();
                lossesNow = winLossAfter.losses();
              }

              final int wins = winsNow - winsBefore;
              final int losses = lossesNow - lossesBefore;
              int mmrDelta = 0;
              int positionDelta;

              final ReconstructedReport previousLeaderboardEntry =
                  previousLeaderboard.get(playerId);
              if (previousLeaderboardEntry != null) {
                mmrDelta = entry.getMmr() - previousLeaderboardEntry.getMmr();
                positionDelta = entry.getPosition() - previousLeaderboardEntry.getPosition();
                newEntry = false;
              } else {
                final Report previousReport = closestEntriesBeforeOrFirstSince.get(playerId);
                positionDelta = entry.getPosition() - LEADERBOARD_SIZE;
                if (previousReport != null) {
                  mmrDelta = entry.getMmr() - previousReport.getMmr();
                  newEntry = false;
                }
              }

              deltas.put(
                  playerId, new SnapshotDelta(newEntry, positionDelta, mmrDelta, wins, losses));
            });

    return Optional.of(new SnapshotDeltas(deltas, referenceTime, currentTime, duration));
  }
}
