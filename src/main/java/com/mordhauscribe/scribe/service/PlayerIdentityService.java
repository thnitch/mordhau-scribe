package com.mordhauscribe.scribe.service;

import com.mordhauscribe.scribe.model.entity.PlayerIdentity;
import io.micrometer.core.annotation.Timed;
import java.time.Instant;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

public interface PlayerIdentityService {

  int SCHEDULED_TITLE_PLAYER_ACCOUNT_IDS_RESOLUTION_SECONDS = 1;
  int SCHEDULED_TITLE_PLAYER_ACCOUNT_IDS_RESOLUTION_INITIAL_DELAY_SECONDS = 5;

  int SCHEDULED_PLAY_FAB_IDS_WITHOUT_TITLE_PLAYER_ACCOUNT_IDS_RESOLUTION_SECONDS = 10;
  int SCHEDULED_PLAY_FAB_IDS_WITHOUT_TITLE_PLAYER_ACCOUNT_IDS_RESOLUTION_INITIAL_DELAY_SECONDS = 60;

  int SCHEDULED_PLAY_FAB_IDS_WITHOUT_TITLE_PLAYER_ACCOUNT_IDS_CHECK_SECONDS = 10 * 60;
  int SCHEDULED_PLAY_FAB_IDS_WITHOUT_TITLE_PLAYER_ACCOUNT_IDS_CHECK_INITIAL_DELAY_SECONDS = 5;
  
  int SCHEDULED_ONLINE_PLAYERS_BY_PLATFORM_UPDATE_MINUTES = 1;

  // priority for id resolution
  // players who finished a ranked game
  int CONCLUDED_RANKED_TITLE_PLAYER_ACCOUNT_ID_RESOLUTION_PRIORITY = 1;
  // players who are in a ranked game
  int RANKED_TITLE_PLAYER_ACCOUNT_ID_RESOLUTION_PRIORITY = 2;
  // players who are in a casual game
  int CASUAL_TITLE_PLAYER_ACCOUNT_ID_RESOLUTION_PRIORITY = 3;

  @Async
  @Scheduled(
      fixedDelay = SCHEDULED_TITLE_PLAYER_ACCOUNT_IDS_RESOLUTION_SECONDS,
      initialDelay = SCHEDULED_TITLE_PLAYER_ACCOUNT_IDS_RESOLUTION_INITIAL_DELAY_SECONDS,
      timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  void resolveTitlePlayerAccountIds();

  @Async
  @Scheduled(
    fixedDelay = SCHEDULED_PLAY_FAB_IDS_WITHOUT_TITLE_PLAYER_ACCOUNT_IDS_RESOLUTION_SECONDS,
    initialDelay = SCHEDULED_PLAY_FAB_IDS_WITHOUT_TITLE_PLAYER_ACCOUNT_IDS_RESOLUTION_INITIAL_DELAY_SECONDS,
    timeUnit = TimeUnit.SECONDS)
  @Timed(
    value = "scribe.service.time",
    description = "Time taken for a service method",
    extraTags = {"service", "player_identity"})
  void resolvePlayFabIdsWithoutTitlePlayerAccountIds();

  @Async
  @Scheduled(
    fixedDelay = SCHEDULED_PLAY_FAB_IDS_WITHOUT_TITLE_PLAYER_ACCOUNT_IDS_CHECK_SECONDS,
    initialDelay =
      SCHEDULED_PLAY_FAB_IDS_WITHOUT_TITLE_PLAYER_ACCOUNT_IDS_CHECK_INITIAL_DELAY_SECONDS,
    timeUnit = TimeUnit.SECONDS)
  @Timed(
    value = "scribe.service.time",
    description = "Time taken for a service method",
    extraTags = {"service", "player_identity"})
  void checkPlayFabIdsWithoutTitlePlayerAccountIds();

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  Optional<PlayerIdentity> findByAny(String any, String platform, String platformAccountId);

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  Optional<PlayerIdentity> findByPlayFabId(String playFabId);
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  Set<PlayerIdentity> findByPlayFabIdIn(Set<String> playFabIds);

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  Optional<PlayerIdentity> findByPlatformAccountId(String platform, String platformAccountId);

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  Optional<PlayerIdentity> findBySteamId(String steamId);

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  PlayerIdentityServiceImpl.TitlePlayerAccountIdResolutionResult findByTitlePlayerAccountIds(
      Set<String> titlePlayerAccountIds, int priority);

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  @Transactional
  void updateLastSeenRanked(Set<String> titlePlayerAccountIds, Instant lastSeen);

  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  @Transactional
  Set<String> updateLastSeenCasual(Set<String> titlePlayerAccountIds, Instant lastSeen);

  @Async
  @Scheduled(
      fixedRate = SCHEDULED_ONLINE_PLAYERS_BY_PLATFORM_UPDATE_MINUTES,
      timeUnit = TimeUnit.MINUTES)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  void updateOnlinePlayersByPlatform();
}
