package com.mordhauscribe.scribe.service;

import com.mordhauscribe.scribe.model.QueueType;
import com.mordhauscribe.scribe.model.entity.Report;
import com.mordhauscribe.scribe.repository.ReportRepository;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class RankDistributionServiceImpl implements RankDistributionService {

  private final ReportRepository repository;
  private final Map<QueueType, Map<Integer, EnhancedRankDistribution>> rankDistribution;
  private final Map<QueueType, Map<Integer, AtomicInteger>> rankDistributionMetric;
  private final MeterRegistry meterRegistry;

  @Autowired
  public RankDistributionServiceImpl(
    final ReportRepository repository,
    MeterRegistry meterRegistry) {
    this.repository = repository;
    this.meterRegistry = meterRegistry;

    this.rankDistribution = new HashMap<>();
    this.rankDistributionMetric = new HashMap<>();

    for (QueueType queueType : QueueType.values()) {
      rankDistribution.put(queueType, new HashMap<>());
      rankDistributionMetric.put(queueType, new HashMap<>());
    }
  }

  @Override
  public double getTopXPercentForRank(final int rank, final QueueType queueType) {
    int baseRank = rank - rank % 100;
    if (this.rankDistribution.get(queueType).containsKey(baseRank))
      return this.rankDistribution.get(queueType).get(baseRank).topXPercent;
    return 0D;
  }

  @Override
  @Async
  @Scheduled(
    fixedRate = SCHEDULED_RANK_DISTRIBUTION_UPDATE_RATE_MINUTES,
    timeUnit = TimeUnit.MINUTES)
  @Timed(
    value = "scribe.service.time",
    description = "Time taken for a service method",
    extraTags = {"service", "rank_distribution"})
  public void updateDuelRankDistribution() {
    final List<Report.RankDistributionResult> rankDistributions =
      repository.determineDuelRankDistribution();

    enhanceRankDistribution(rankDistributions, QueueType.SOLO);
  }

  @Override
  @Async
  @Scheduled(
    fixedRate = SCHEDULED_RANK_DISTRIBUTION_UPDATE_RATE_MINUTES,
    timeUnit = TimeUnit.MINUTES)
  @Timed(
    value = "scribe.service.time",
    description = "Time taken for a service method",
    extraTags = {"service", "rank_distribution"})
  public void updateTeamfightRankDistribution() {
    final List<Report.RankDistributionResult> rankDistributions =
      repository.determineTeamfightRankDistribution();
    enhanceRankDistribution(rankDistributions, QueueType.TEAM);
  }

  private void enhanceRankDistribution(
    List<Report.RankDistributionResult> rankDistributions, QueueType queueType) {
    List<Report.RankDistributionResult> filteredRankDistributions = rankDistributions.stream().filter(rankDistribution -> rankDistribution.rank() != null).toList();

    final Integer sum =
      filteredRankDistributions.stream()
        .map(Report.RankDistributionResult::playerCount)
        .reduce(Integer::sum)
        .get();

    int playersBelowRank = 0;
    final Map<Integer, Integer> playersBelowRankByRanks = new HashMap<>();
    for (Report.RankDistributionResult rankDistribution : filteredRankDistributions) {
      playersBelowRankByRanks.put(rankDistribution.rank(), playersBelowRank);
      playersBelowRank += rankDistribution.playerCount();
    }

    final List<EnhancedRankDistribution> enhancedRankDistributions =
      filteredRankDistributions.stream()
        .map(
          rankDistribution -> {
            double topXPercent =
              (double) (sum - playersBelowRankByRanks.get(rankDistribution.rank())) / sum;
            if (topXPercent < 0.001D) topXPercent = round(topXPercent, 5);
            else if (topXPercent < 0.01D) topXPercent = round(topXPercent, 4);
            else if (topXPercent < 0.1D) topXPercent = round(topXPercent, 3);
            else topXPercent = round(topXPercent, 2);
            return new EnhancedRankDistribution(
              rankDistribution.rank(), rankDistribution.playerCount(), topXPercent);
          })
        .toList();

    for (EnhancedRankDistribution enhancedRankDistribution : enhancedRankDistributions) {
      rankDistribution
        .get(queueType)
        .put(enhancedRankDistribution.rank(), enhancedRankDistribution);
      final Map<Integer, AtomicInteger> rankDistributionGauges =
        rankDistributionMetric.get(queueType);
      if (!rankDistributionGauges.containsKey(enhancedRankDistribution.rank()))
        rankDistributionGauges.put(
          enhancedRankDistribution.rank(),
          meterRegistry.gauge(
            "scribe.rank.distribution",
            Tags.of(
              "queue",
              queueType.getQueueName(),
              "rank",
              enhancedRankDistribution.rank().toString()),
            new AtomicInteger(0)));
      rankDistributionGauges
        .get(enhancedRankDistribution.rank())
        .set(enhancedRankDistribution.playerCount);
    }
  }

  public static double round(double value, int places) {
    if (places < 0) throw new IllegalArgumentException();

    BigDecimal bd = BigDecimal.valueOf(value);
    bd = bd.setScale(places, RoundingMode.HALF_UP);
    return bd.doubleValue();
  }

  public record EnhancedRankDistribution(Integer rank, Integer playerCount, double topXPercent) {
  }
}
