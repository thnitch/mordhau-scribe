package com.mordhauscribe.scribe.service;

import com.mordhauscribe.scribe.exception.PlayFabServiceException;
import com.mordhauscribe.scribe.model.Game;
import com.mordhauscribe.scribe.model.PlayFabCasualGamesFilter;
import com.mordhauscribe.scribe.model.PlayFabRankedGamesFilter;
import com.mordhauscribe.scribe.model.QueueType;
import com.mordhauscribe.scribe.service.PlayerIdentityServiceImpl.TitlePlayerAccountIdResolutionResult;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class CurrentGamesServiceImpl implements CurrentGamesService, HealthIndicator {

  private static final Logger LOG = LogManager.getLogger();

  private static final PlayFabRankedGamesFilter RANKED_GAMES_FILTER =
      new PlayFabRankedGamesFilter();
  private static final PlayFabCasualGamesFilter CASUAL_GAMES_FILTER =
      new PlayFabCasualGamesFilter();
  private final PlayFabService playFabService;
  private final PlayerIdentityService playerIdentityService;
  private final ReportUpdateService reportUpdateService;
  private Map<String, Game> currentRankedGames;
  private Map<String, Game> currentCasualGames;
  private Map<OnlinePlayer, Game> onlineRankedPlayers;
  private Map<String, Game> onlineCasualPlayers;
  private long onlineDuelPlayerCount;
  private long onlineTeamfightPlayerCount;
  private final MeterRegistry meterRegistry;
  private final Map<List<Tag>, AtomicInteger> currentGamesGauges;
  private final AtomicLong staleUnresolvedRankedPlayersGauge;
  private boolean rankedGamesInitialized = false;
  private final List<CurrentRankedGamesListener> listeners;
  private Health health;

  @Autowired
  public CurrentGamesServiceImpl(
      final PlayFabService playFabService,
      PlayerIdentityService playerIdentityService,
      ReportUpdateService reportUpdateService,
      MeterRegistry meterRegistry) {
    this.playFabService = playFabService;
    this.playerIdentityService = playerIdentityService;
    this.reportUpdateService = reportUpdateService;
    this.meterRegistry = meterRegistry;

    onlineRankedPlayers = new HashMap<>();
    onlineCasualPlayers = new HashMap<>();
    currentRankedGames = new HashMap<>();
    currentCasualGames = new HashMap<>();
    currentGamesGauges = new HashMap<>();
    listeners = new ArrayList<>();
    staleUnresolvedRankedPlayersGauge =
        meterRegistry.gauge(
            "scribe.queue",
            Tags.of("service", "current_games", "type", "stale_unresolved_online_ranked_players"),
            new AtomicLong(0));
  }

  @Override
  @Async
  @Scheduled(
      fixedDelay = SCHEDULED_CURRENT_RANKED_GAMES_UPDATE_DELAY_SECONDS,
      timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "current_games"})
  public void updateCurrentRankedGames() {
    try {
      Map<OnlinePlayer, Game> newOnlineRankedPlayers = new HashMap<>();
      final Map<String, Game> newCurrentRankedGames =
          playFabService.fetchCurrentGames(RANKED_GAMES_FILTER);
      Instant rankedUpdateTime = Instant.now();

      newCurrentRankedGames
          .values()
          .forEach(
              game ->
                  game.getPlayers()
                      .forEach(
                          titlePlayerAccountId ->
                              newOnlineRankedPlayers.put(
                                  new OnlinePlayer(titlePlayerAccountId), game)));

      final Set<String> playerIdsToUpdate;

      TitlePlayerAccountIdResolutionResult playerIdentityResolutionResult = null;
      if (!rankedGamesInitialized) {
        playerIdsToUpdate =
            newOnlineRankedPlayers.keySet().stream()
                .map(OnlinePlayer::getTitlePlayerAccountId)
                .collect(Collectors.toSet());
        playerIdentityResolutionResult =
            playerIdentityService.findByTitlePlayerAccountIds(
                playerIdsToUpdate,
                PlayerIdentityService.CONCLUDED_RANKED_TITLE_PLAYER_ACCOUNT_ID_RESOLUTION_PRIORITY);

        // explicitly queue updates for players currently in game without retries
        reportUpdateService.queueUpdates(
            playerIdentityResolutionResult.resolved(),
            ReportUpdateService.REGULAR_UPDATE_PRIORITY,
            0);
      } else {
        playerIdsToUpdate = new HashSet<>();
        for (OnlinePlayer player : onlineRankedPlayers.keySet()) {
          final Game previousGame = onlineRankedPlayers.get(player);
          final Game newGame = newOnlineRankedPlayers.get(player);

          // if different or no game with player -> player must have finished a ranked game
          if (newGame == null
              || (previousGame != null
                  && !newGame.getServerIPV4Address().equals(previousGame.getServerIPV4Address())))
            playerIdsToUpdate.add(player.getTitlePlayerAccountId());
        }

        playerIdentityResolutionResult =
            playerIdentityService.findByTitlePlayerAccountIds(
                playerIdsToUpdate,
                PlayerIdentityService.CONCLUDED_RANKED_TITLE_PLAYER_ACCOUNT_ID_RESOLUTION_PRIORITY);

        reportUpdateService.queueUpdates(playerIdentityResolutionResult.resolved());
      }

      // we keep considering unresolved identities as playing, this way eventually a player report
      // will be generated for them
      for (String titlePlayerAccountId : playerIdentityResolutionResult.unresolved()) {
        final OnlinePlayer unresolvedPlayer = new OnlinePlayer(titlePlayerAccountId, true);
        newOnlineRankedPlayers.put(unresolvedPlayer, onlineRankedPlayers.get(unresolvedPlayer));
      }

      final Instant staleThreshold =
          Instant.now().minus(Duration.ofMinutes(STALE_UNRESOLVED_RANKED_PLAYER_MINUTES));
      final List<OnlinePlayer> staleOnlineRankedPlayers =
          newOnlineRankedPlayers.keySet().stream()
              .filter(
                  player -> player.isUnresolved() && player.getReadded().isAfter(staleThreshold))
              .toList();
      for (OnlinePlayer staleOnlineRankedPlayer : staleOnlineRankedPlayers) {
        newOnlineRankedPlayers.remove(staleOnlineRankedPlayer);
      }
      staleUnresolvedRankedPlayersGauge.set(staleOnlineRankedPlayers.size());

      onlineRankedPlayers = newOnlineRankedPlayers;
      currentRankedGames = newCurrentRankedGames;

      playerIdentityService.updateLastSeenRanked(
          new HashSet<>(
              onlineRankedPlayers.keySet().stream()
                  .map(OnlinePlayer::getTitlePlayerAccountId)
                  .collect(Collectors.toSet())),
          rankedUpdateTime);

      rankedGamesInitialized = true;

      health = Health.up().build();
    } catch (final PlayFabServiceException e) {
      LOG.error(e);
      health = Health.down().withException(e).build();
    }

    listeners.forEach(CurrentRankedGamesListener::onCurrentRankedGamesUpdate);
  }

  @Override
  @Async
  @Scheduled(
      fixedDelay = SCHEDULED_CURRENT_CASUAL_GAMES_UPDATE_DELAY_SECONDS,
      timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "current_games"})
  public void updateCurrentCasualGames() {
    try {
      final Map<String, Game> newOnlineCasualPlayers = new HashMap<>();
      final Map<String, Game> newCurrentCasualGames =
          playFabService.fetchCurrentGames(CASUAL_GAMES_FILTER);
      Instant casualUpdateTime = Instant.now();

      newCurrentCasualGames
        .values()
        .forEach(
          game ->
            game.getPlayers().forEach(player -> newOnlineCasualPlayers.put(player, game)));

      final Set<String> resolvedPlayerIdentities =
          playerIdentityService.updateLastSeenCasual(
              new HashSet<>(newOnlineCasualPlayers.keySet()), casualUpdateTime);

      // filter out any unresolved title player account ids on unofficial servers
      newCurrentCasualGames.values().stream()
          .filter(game -> !game.isOfficial())
          .forEach(
              game ->
                  game.setPlayers(
                      game.getPlayers().stream()
                          .filter(resolvedPlayerIdentities::contains)
                          .collect(Collectors.toList())));

      currentCasualGames = newCurrentCasualGames;

      newOnlineCasualPlayers.clear();
      newCurrentCasualGames
          .values()
          .forEach(
              game ->
                  game.getPlayers().forEach(player -> newOnlineCasualPlayers.put(player, game)));

      onlineCasualPlayers = newOnlineCasualPlayers;

      health = Health.up().build();
    } catch (final PlayFabServiceException e) {
      LOG.error(e);
      health = Health.down().withException(e).build();
    }
  }

  @Async
  @Scheduled(
      fixedDelay = SCHEDULED_ONLINE_PLAYERS_UPDATE_DELAY_SECONDS,
      initialDelay = SCHEDULED_ONLINE_PLAYERS_UPDATE_INITIAL_DELAY_SECONDS,
      timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "current_games"})
  public void updateOnlinePlayers() {
    Map<List<Tag>, Integer> newValues = new HashMap<>();
    Stream.concat(currentRankedGames.values().stream(), currentCasualGames.values().stream())
        .forEach(
            game -> {
              List<Tag> tags =
                  List.of(
                      Tag.of("queue_name", Optional.ofNullable(game.getQueueName()).orElse("")),
                      Tag.of("game_mode", game.getGameMode()),
                      Tag.of("map_name", game.getMapName()),
                      Tag.of("is_official", String.valueOf(game.isOfficial())),
                      Tag.of("is_modded", String.valueOf(game.isModded())),
                      Tag.of("region", game.getRegion()),
                      Tag.of("location", game.getLocation()));
              final Integer count = newValues.getOrDefault(tags, 0);
              newValues.put(tags, count + game.getPlayers().size());
            });

    currentGamesGauges
        .keySet()
        .forEach(
            key -> {
              if (!newValues.containsKey(key)) currentGamesGauges.get(key).set(0);
            });

    newValues.forEach(
        (key, value) -> {
          if (currentGamesGauges.containsKey(key)) currentGamesGauges.get(key).set(value);
          else
            currentGamesGauges.put(
                key, meterRegistry.gauge("scribe.current.games", key, new AtomicInteger(value)));
        });

    long newOnlineDuelPlayerCount = 0;
    long newOnlineTeamfightPlayerCount = 0;
    for (Game game : currentRankedGames.values()) {
      if (game.getQueueName().equals(QueueType.SOLO.getQueueName()))
        newOnlineDuelPlayerCount += game.getPlayers().size();
      else if (game.getQueueName().equals(QueueType.TEAM.getQueueName()))
        newOnlineTeamfightPlayerCount += game.getPlayers().size();
    }

    onlineDuelPlayerCount = newOnlineDuelPlayerCount;
    onlineTeamfightPlayerCount = newOnlineTeamfightPlayerCount;

    listeners.forEach(CurrentRankedGamesListener::onOnlineRankedPlayersUpdate);
  }

  @Override
  public Game lookupGameForPlayer(String titlePlayerAccountId) {
    return onlineRankedPlayers.getOrDefault(
        new OnlinePlayer(titlePlayerAccountId), onlineCasualPlayers.get(titlePlayerAccountId));
  }

  @Override
  public long currentPlayerCountByQueueType(QueueType queueType) {
    if (queueType.equals(QueueType.SOLO)) return onlineDuelPlayerCount;
    else if (queueType.equals(QueueType.TEAM)) return onlineTeamfightPlayerCount;
    return 0;
  }

  @Override
  public void register(CurrentRankedGamesListener listener) {
    listeners.add(listener);
  }

  @Override
  public Health health() {
    return this.health;
  }

  private static class OnlinePlayer {
    private final String titlePlayerAccountId;
    private boolean isUnresolved = false;
    private Instant readded;

    public OnlinePlayer(String titlePlayerAccountId) {
      this.titlePlayerAccountId = titlePlayerAccountId;
    }

    public OnlinePlayer(String titlePlayerAccountId, boolean isUnresolved) {
      this.titlePlayerAccountId = titlePlayerAccountId;
      if (isUnresolved) {
        this.isUnresolved = true;
        this.readded = Instant.now();
      }
    }

    public String getTitlePlayerAccountId() {
      return titlePlayerAccountId;
    }

    public boolean isUnresolved() {
      return isUnresolved;
    }

    public Instant getReadded() {
      return readded;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      OnlinePlayer that = (OnlinePlayer) o;

      return titlePlayerAccountId.equals(that.titlePlayerAccountId);
    }

    @Override
    public int hashCode() {
      return titlePlayerAccountId.hashCode();
    }
  }
}
