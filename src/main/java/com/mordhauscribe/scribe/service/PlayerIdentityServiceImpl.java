package com.mordhauscribe.scribe.service;

import static java.util.stream.Collectors.toSet;

import com.google.common.collect.Iterables;
import com.mordhauscribe.scribe.exception.PlayFabServiceException;
import com.mordhauscribe.scribe.exception.PlayerNotFoundException;
import com.mordhauscribe.scribe.model.Platform;
import com.mordhauscribe.scribe.model.UniquePriorityBlockingQueue;
import com.mordhauscribe.scribe.model.UniquePriorityBlockingQueue.PriorityComparator;
import com.mordhauscribe.scribe.model.UniquePriorityBlockingQueue.UniquePriorityBlockingQueueItem;
import com.mordhauscribe.scribe.model.entity.PlayerIdentity;
import com.mordhauscribe.scribe.repository.PlayerIdentityRepository;
import com.playfab.PlayFabClientModels;
import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PlayerIdentityServiceImpl implements PlayerIdentityService {
  public static final int PROFILE_BATCH_LOOKUP_SIZE = 25;
  private static final Logger LOG = LogManager.getLogger();
  private final PlayerIdentityRepository repository;
  private final PlayFabService playFabService;
  private final SteamVanityUrlService steamVanityUrlService;
  private final UniquePriorityBlockingQueue<UniquePriorityBlockingQueueItem<String>>
      unresolvedTitlePlayerAccountIds;
  private final Set<String> unresolvableTitlePlayerAccountIds;
  private final UniquePriorityBlockingQueue<UniquePriorityBlockingQueueItem<String>>
      playFabIdsWithoutTitlePlayerAccountIds;

  private final int resolveIdsBatchSize;
  private final MeterRegistry meterRegistry;
  private final Counter counterSearchSteamVanitySuccess;
  private final Counter counterSearchFail;
  private final Counter counterSearchPlayFabIdSuccess;
  private final Counter counterSearchSteamIdSuccess;
  private final Counter counterSearchEpicGamesIdSuccess;
  private final HashMap<String, AtomicInteger> playersByPlatformGauges;

  @Autowired
  public PlayerIdentityServiceImpl(
      final PlayerIdentityRepository repository,
      final PlayFabService playFabService,
      final SteamVanityUrlService steamVanityUrlService,
      final MeterRegistry meterRegistry,
      @Value("${scribe.player-identity.resolve-ids-per-second-batch-size}")
          final int resolveIdsBatchSize) {
    this.repository = repository;
    this.playFabService = playFabService;
    this.steamVanityUrlService = steamVanityUrlService;

    this.meterRegistry = meterRegistry;
    this.unresolvedTitlePlayerAccountIds =
        meterRegistry.gaugeCollectionSize(
            "scribe.queue",
            Tags.of("service", "player_identity", "type", "unresolved_title_player_account_id"),
            new UniquePriorityBlockingQueue<>(
                new PriorityBlockingQueue<>(500, new PriorityComparator())));
    this.playFabIdsWithoutTitlePlayerAccountIds =
        meterRegistry.gaugeCollectionSize(
            "scribe.queue",
            Tags.of(
                "service",
                "player_identity",
                "type",
                "play_fab_ids_without_title_player_account_ids"),
            new UniquePriorityBlockingQueue<>(
                new PriorityBlockingQueue<>(500, new PriorityComparator())));
    this.unresolvableTitlePlayerAccountIds =
        meterRegistry.gaugeCollectionSize(
            "scribe.queue",
            Tags.of("service", "player_identity", "type", "unresolvable_title_player_account_id"),
            new HashSet<>());

    this.resolveIdsBatchSize = resolveIdsBatchSize;
    counterSearchSteamVanitySuccess =
        Counter.builder("scribe.profile.search")
            .description("profile searches executed")
            .tags("success", "true", "identifier", "steam_vanity_id")
            .register(meterRegistry);
    counterSearchSteamIdSuccess =
        Counter.builder("scribe.profile.search")
            .description("profile searches executed")
            .tags("success", "true", "identifier", "steam_id")
            .register(meterRegistry);
    counterSearchEpicGamesIdSuccess =
        Counter.builder("scribe.profile.search")
            .description("profile searches executed")
            .tags("success", "true", "identifier", "epic_games_id")
            .register(meterRegistry);
    counterSearchPlayFabIdSuccess =
        Counter.builder("scribe.profile.search")
            .description("profile searches executed")
            .tags("success", "true", "identifier", "playfab_id")
            .register(meterRegistry);
    counterSearchFail =
        Counter.builder("scribe.profile.search")
            .description("profile searches executed")
            .tags("success", "false", "identifier", "unknown")
            .register(meterRegistry);
    playersByPlatformGauges = new HashMap<>();
  }

  @Override
  @Async
  @Scheduled(
      fixedDelay = SCHEDULED_TITLE_PLAYER_ACCOUNT_IDS_RESOLUTION_SECONDS,
      initialDelay = SCHEDULED_TITLE_PLAYER_ACCOUNT_IDS_RESOLUTION_INITIAL_DELAY_SECONDS,
      timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  public void resolveTitlePlayerAccountIds() {
    if (!unresolvedTitlePlayerAccountIds.isEmpty()) {
      final int batchSize =
          Math.min(
              Math.max(1, unresolvedTitlePlayerAccountIds.size() / PROFILE_BATCH_LOOKUP_SIZE),
              resolveIdsBatchSize);
      for (int i = 0; i < batchSize; i++) {
        final HashSet<String> idsToResolve = new HashSet<>();
        final HashSet<UniquePriorityBlockingQueueItem<String>> queueItems = new HashSet<>();
        Iterables.limit(unresolvedTitlePlayerAccountIds, PROFILE_BATCH_LOOKUP_SIZE)
            .iterator()
            .forEachRemaining(
                item -> {
                  idsToResolve.add(item.getItem());
                  queueItems.add(item);
                });

        try {
          final List<PlayerIdentity> identities =
              playFabService.fetchProfilesByTitlePlayerAccountIds(idsToResolve);

          for (PlayerIdentity identity : identities) {
            try {
              repository.save(identity);
            } catch (CannotAcquireLockException e) {
              LOG.error("Could not aquire lock for insert {}", identity.getPlayFabId(), e);
            } catch (Exception e) {
              LOG.error("Could not insert {}", identity.getPlayFabId(), e);
            }
          }

          trackUnresolvableTitlePlayerAccountIds(idsToResolve, identities);
          unresolvedTitlePlayerAccountIds.removeAll(queueItems);
        } catch (PlayFabServiceException e) {
          LOG.error(e);
        }
      }
    }
  }

  @Override
  @Async
  @Scheduled(
      fixedDelay = SCHEDULED_PLAY_FAB_IDS_WITHOUT_TITLE_PLAYER_ACCOUNT_IDS_RESOLUTION_SECONDS,
      initialDelay =
          SCHEDULED_PLAY_FAB_IDS_WITHOUT_TITLE_PLAYER_ACCOUNT_IDS_RESOLUTION_INITIAL_DELAY_SECONDS,
      timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  public void resolvePlayFabIdsWithoutTitlePlayerAccountIds() {
    if (!playFabIdsWithoutTitlePlayerAccountIds.isEmpty()) {
      final UniquePriorityBlockingQueueItem<String> playFabIdItem =
          playFabIdsWithoutTitlePlayerAccountIds.poll();

      try {
        final Optional<PlayerIdentity> playerIdentity =
            playFabService.checkPlayerExists(playFabIdItem.getItem());

        if (playerIdentity.isPresent()) {
          try {
            repository.save(playerIdentity.get());
          } catch (CannotAcquireLockException e) {
            LOG.error("Could not aquire lock for insert {}", playFabIdItem.getItem(), e);
          } catch (Exception e) {
            LOG.error("Could not insert {}", playFabIdItem.getItem(), e);
          }
        } else {
          LOG.warn("Unable to determine TitlePlayerAccountID for {}", playFabIdItem.getItem());
        }
      } catch (PlayFabServiceException e) {
        LOG.error(e);
      }
    }
  }

  @Override
  @Async
  @Scheduled(
      fixedDelay = SCHEDULED_PLAY_FAB_IDS_WITHOUT_TITLE_PLAYER_ACCOUNT_IDS_CHECK_SECONDS,
      initialDelay =
          SCHEDULED_PLAY_FAB_IDS_WITHOUT_TITLE_PLAYER_ACCOUNT_IDS_CHECK_INITIAL_DELAY_SECONDS,
      timeUnit = TimeUnit.SECONDS)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  public void checkPlayFabIdsWithoutTitlePlayerAccountIds() {
    final Set<String> playFabIdsWithoutTitlePlayerAccountIds =
        repository.findPlayFabIdsWithoutTitlePlayerAccountId();
    this.playFabIdsWithoutTitlePlayerAccountIds.addAll(
        playFabIdsWithoutTitlePlayerAccountIds.stream()
            .map(id -> new UniquePriorityBlockingQueueItem<>(1, id))
            .collect(Collectors.toSet()));
  }

  private void trackUnresolvableTitlePlayerAccountIds(
      HashSet<String> idsToResolve, List<PlayerIdentity> identities) {
    final Set<String> resolvedIds =
        identities.stream()
            .map(PlayerIdentity::getTitlePlayerAccountId)
            .collect(Collectors.toSet());
    final Set<String> unresolvedIds =
        idsToResolve.stream()
            .filter(
                idToResolve ->
                    resolvedIds.stream().noneMatch(resolvedId -> resolvedId.equals(idToResolve)))
            .collect(Collectors.toSet());
    unresolvableTitlePlayerAccountIds.addAll(unresolvedIds);
    unresolvableTitlePlayerAccountIds.removeAll(resolvedIds);
  }

  private Set<String> filterResolvedTitlePlayerAccountIds(
      final Set<String> titlePlayerAccountIds, int priority) {
    final Set<String> resolvedTitlePlayerAccountIds =
        repository.findExistingPlayerAccountIds(titlePlayerAccountIds);
    titlePlayerAccountIds.removeAll(resolvedTitlePlayerAccountIds);
    titlePlayerAccountIds.removeAll(unresolvableTitlePlayerAccountIds);

    this.unresolvedTitlePlayerAccountIds.addAll(
        titlePlayerAccountIds.stream()
            .map(item -> new UniquePriorityBlockingQueueItem<>(priority, item))
            .collect(Collectors.toSet()));
    return resolvedTitlePlayerAccountIds;
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  public Optional<PlayerIdentity> findByAny(
      final String any, final String platform, final String platformAccountId) {
    if (platform != null
        && Arrays.stream(Platform.values()).anyMatch((t) -> t.name().equals(platform))
        && platformAccountId != null
        && !platformAccountId.isBlank()) {
      Optional<PlayerIdentity> byPlatformId;

      if (Platform.STEAM.getName().equals(platform)) {
        byPlatformId = findBySteamId(platformAccountId);
      } else {
        byPlatformId = findByPlatformAccountId(platform, platformAccountId);
      }

      if (byPlatformId.isPresent()) {
        if (Platform.STEAM.getName().equals(platform)) counterSearchSteamIdSuccess.increment();
        else if (Platform.EPIC.getName().equals(platform))
          counterSearchEpicGamesIdSuccess.increment();
      } else {
        counterSearchFail.increment();
      }

      return byPlatformId;
    }

    if (any.contains(" ")) {
      try {
        final String steamId = steamVanityUrlService.resolveVanityUrl(any);
        final Optional<PlayerIdentity> byPlatformAccountId = findBySteamId(steamId);
        counterSearchSteamVanitySuccess.increment();
        return byPlatformAccountId;
      } catch (final PlayerNotFoundException e) {
        counterSearchFail.increment();
        return Optional.empty();
      }
    }

    if (any.length() == 17 && any.matches("\\d{17}")) {
      final Optional<PlayerIdentity> potentialSteamAccountId = findBySteamId(any);
      if (potentialSteamAccountId.isPresent()) {
        counterSearchSteamIdSuccess.increment();
        return potentialSteamAccountId;
      }
    }

    if (any.length() == 32 && any.matches("[0-9a-f]{32}")) {
      final Optional<PlayerIdentity> potentialEpicGamesAccountId =
          findByPlatformAccountId(Platform.EPIC.getName(), any);
      if (potentialEpicGamesAccountId.isPresent()) {
        counterSearchEpicGamesIdSuccess.increment();
        return potentialEpicGamesAccountId;
      }
    }

    try {
      final String steamId = steamVanityUrlService.resolveVanityUrl(any);
      final Optional<PlayerIdentity> byPlatformAccountId = findBySteamId(steamId);
      counterSearchSteamVanitySuccess.increment();
      return byPlatformAccountId;
    } catch (final PlayerNotFoundException e) {
      if (any.matches("[0-9a-fA-F]{12,16}")) {
        final Optional<PlayerIdentity> byPlayFabId = findByPlayFabId(any);
        if (byPlayFabId.isPresent()) {
          counterSearchPlayFabIdSuccess.increment();
          return byPlayFabId;
        }
      }
    }

    counterSearchFail.increment();
    return Optional.empty();
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  public Optional<PlayerIdentity> findByPlayFabId(final String playFabId) {
    try {
      final Optional<PlayerIdentity> fromDb = repository.findByPlayFabId(playFabId);
      if (fromDb.isPresent()) {
        return fromDb;
      } else {
        final Optional<PlayerIdentity> optionalPlayerIdentity =
            playFabService.checkPlayerExists(playFabId);

        if (optionalPlayerIdentity.isPresent()) {
          repository.save(optionalPlayerIdentity.get());

          return optionalPlayerIdentity;
        }

        return Optional.empty();
      }
    } catch (final Exception e) {
      LOG.error(String.format("PlayerId: %s", playFabId), e);
      return Optional.empty();
    }
  }

  @Override
  public Set<PlayerIdentity> findByPlayFabIdIn(Set<String> playFabIds) {
    final Set<PlayerIdentity> playerIdentities = repository.findByPlayFabIdIn(playFabIds);

    Set<String> unresolvedPlayerIds =
        playFabIds.stream()
            .filter(
                playFabId ->
                    playerIdentities.stream()
                        .noneMatch(identity -> identity.getPlayFabId().equals(playFabId)))
            .collect(toSet());

    for (String unresolvedPlayerId : unresolvedPlayerIds) {
      final Optional<PlayerIdentity> optionalPlayerIdentity;
      try {
        optionalPlayerIdentity = playFabService.checkPlayerExists(unresolvedPlayerId);
        if (optionalPlayerIdentity.isPresent()) {
          final PlayerIdentity identity = optionalPlayerIdentity.get();
          repository.save(identity);
          playerIdentities.add(identity);
        }
      } catch (PlayFabServiceException e) {
        LOG.error("Failed to check player from leaderboard {}: {}", unresolvedPlayerId, e);
      }
    }

    return playerIdentities;
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  public Optional<PlayerIdentity> findByPlatformAccountId(
      final String platform, final String platformAccountId) {
    final Optional<PlayerIdentity> identity =
        repository.findByPlatformAndPlatformAccountId(platform, platformAccountId);

    if (identity.isPresent()) return identity;

    try {
      final PlayFabClientModels.GenericServiceId genericServiceId =
          new PlayFabClientModels.GenericServiceId();
      genericServiceId.ServiceName = platform;
      genericServiceId.UserId = platformAccountId;

      final ArrayList<PlayFabClientModels.GenericServiceId> genericIds =
          new ArrayList<>(List.of(genericServiceId));

      final Optional<PlayFabClientModels.GenericPlayFabIdPair> genericPlayFabIdPair =
          playFabService.fetchPlayFabIdsByGenericIds(genericIds).stream()
              .findAny()
              .filter(pair -> pair.GenericId.UserId.equals(platformAccountId));
      if (genericPlayFabIdPair.isPresent()) {
        final PlayFabClientModels.GenericPlayFabIdPair idPair = genericPlayFabIdPair.get();

        if (idPair.PlayFabId == null) {
          throw new PlayerNotFoundException(idPair.GenericId.UserId);
        }

        PlayerIdentity newIdentity =
            new PlayerIdentity(
                idPair.PlayFabId, idPair.GenericId.ServiceName, idPair.GenericId.UserId);
        newIdentity = repository.save(newIdentity);
        return Optional.of(newIdentity);
      }
    } catch (final PlayFabServiceException e) {
      LOG.error("Error in findByPlatformAccountId:", e);
    }

    return Optional.empty();
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  public Optional<PlayerIdentity> findBySteamId(final String steamId) {
    final Optional<PlayerIdentity> identity =
        repository.findByPlatformAndPlatformAccountId(Platform.STEAM.getName(), steamId);

    if (identity.isPresent()) return identity;

    try {
      final Optional<PlayFabClientModels.SteamPlayFabIdPair> steamPlayFabIdPair =
          playFabService.fetchPlayFabIdsBySteamIds(Collections.singletonList(steamId)).stream()
              .findAny()
              .filter(pair -> pair.SteamStringId.equals(steamId));
      if (steamPlayFabIdPair.isPresent()) {
        final PlayFabClientModels.SteamPlayFabIdPair idPair = steamPlayFabIdPair.get();

        if (idPair.PlayFabId == null) {
          throw new PlayerNotFoundException(idPair.SteamStringId);
        }

        PlayerIdentity newIdentity =
            new PlayerIdentity(idPair.PlayFabId, Platform.STEAM.getName(), idPair.SteamStringId);
        newIdentity = repository.save(newIdentity);
        return Optional.of(newIdentity);
      }
    } catch (final PlayFabServiceException e) {
      LOG.error("Error in findByPlatformAccountId:", e);
    }

    return Optional.empty();
  }

  public record TitlePlayerAccountIdResolutionResult(
      Set<PlayerIdentity> resolved, Set<String> unresolved) {}

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  public TitlePlayerAccountIdResolutionResult findByTitlePlayerAccountIds(
      Set<String> titlePlayerAccountIds, int priority) {
    final Set<PlayerIdentity> identities =
        repository.findByTitlePlayerAccountIdIn(titlePlayerAccountIds);

    final Set<String> resolvedIdentities =
        identities.stream()
            .map(PlayerIdentity::getTitlePlayerAccountId)
            .collect(Collectors.toSet());

    titlePlayerAccountIds.removeAll(resolvedIdentities);
    titlePlayerAccountIds.removeAll(unresolvableTitlePlayerAccountIds);
    this.unresolvedTitlePlayerAccountIds.addAll(
        titlePlayerAccountIds.stream()
            .map(item -> new UniquePriorityBlockingQueueItem<>(priority, item))
            .collect(Collectors.toSet()));
    return new TitlePlayerAccountIdResolutionResult(identities, titlePlayerAccountIds);
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  @Transactional
  public void updateLastSeenRanked(Set<String> titlePlayerAccountIds, Instant lastSeen) {
    final Set<String> resolvedTitlePlayerAccountIds =
        filterResolvedTitlePlayerAccountIds(
            titlePlayerAccountIds, RANKED_TITLE_PLAYER_ACCOUNT_ID_RESOLUTION_PRIORITY);
    repository.updateLastSeenRanked(resolvedTitlePlayerAccountIds, lastSeen);
  }

  @Override
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  @Transactional
  public Set<String> updateLastSeenCasual(Set<String> titlePlayerAccountIds, Instant lastSeen) {
    final Set<String> resolvedTitlePlayerAccountIds =
        filterResolvedTitlePlayerAccountIds(
            titlePlayerAccountIds, CASUAL_TITLE_PLAYER_ACCOUNT_ID_RESOLUTION_PRIORITY);

    repository.updateLastSeenCasual(resolvedTitlePlayerAccountIds, lastSeen);

    return resolvedTitlePlayerAccountIds;
  }

  @Override
  @Async
  @Scheduled(
      fixedRate = SCHEDULED_ONLINE_PLAYERS_BY_PLATFORM_UPDATE_MINUTES,
      timeUnit = TimeUnit.MINUTES)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "player_identity"})
  public void updateOnlinePlayersByPlatform() {
    final List<PlayerIdentity.PlatformDistributionResult> platformDistributionResults =
        repository.determinePlatformDistribution(
            SCHEDULED_ONLINE_PLAYERS_BY_PLATFORM_UPDATE_MINUTES);

    for (PlayerIdentity.PlatformDistributionResult result : platformDistributionResults) {
      final String platform = result.platform();
      if (!playersByPlatformGauges.containsKey(platform))
        playersByPlatformGauges.put(
            platform,
            meterRegistry.gauge(
                "scribe.online.players",
                Tags.of("platform", platform),
                new AtomicInteger(result.playerCount())));
      playersByPlatformGauges.get(platform).set(result.playerCount());
    }
  }
}
