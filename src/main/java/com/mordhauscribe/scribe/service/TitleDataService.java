package com.mordhauscribe.scribe.service;

import java.util.concurrent.TimeUnit;

import io.micrometer.core.annotation.Timed;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

public interface TitleDataService {
  int SCHEDULED_TITLE_DATA_UPDATE_DELAY_MINUTES = 10;

  @Async
  @Scheduled(fixedDelay = SCHEDULED_TITLE_DATA_UPDATE_DELAY_MINUTES, timeUnit = TimeUnit.MINUTES)
  @Timed(
      value = "scribe.service.time",
      description = "Time taken for a service method",
      extraTags = {"service", "title_data"})
  void updateTitleData();

  boolean getPlayerBanStatus(String playFabId);

  Long getPlayerGlobalBanStatus(String playFabId);

  Long getPlayerOfficialBanStatus(String playFabId);

  Long getPlayerGlobalMuteStatus(String playFabId);

  Long getPlayerOfficialMuteStatus(String playFabId);
}
