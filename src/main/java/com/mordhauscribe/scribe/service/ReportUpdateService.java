package com.mordhauscribe.scribe.service;

import com.mordhauscribe.scribe.model.entity.PlayerIdentity;
import io.micrometer.core.annotation.Timed;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;

public interface ReportUpdateService {
  int INITIAL_REPORT_UPDATE_WAIT_SECONDS = 10;
  int SCHEDULED_QUEUE_PROCESSING_DELAY_SECONDS = 1;
  int REGULAR_UPDATE_PRIORITY = 1;
  int INITIAL_UPDATE_PRIORITY = 2;
  int RETRIED_UPDATE_PRIORITY = 3;
  int LEADERBOARD_INIT_UPDATE_PRIORITY = 4;

  int REGULAR_UDPATE_RETRIES = 2;
  // each retry exponentially increases wait time
  // (retry#^2 * RETRY_EXPONENTIAL_BACKOFF_WAIT_SECONDS)
  int RETRY_EXPONENTIAL_BACKOFF_WAIT_SECONDS = 120;
  int RETRY_CHECK_RATE_SECONDS = 5;

  @Async
  @Scheduled(fixedDelay = SCHEDULED_QUEUE_PROCESSING_DELAY_SECONDS, timeUnit = TimeUnit.SECONDS)
  @Timed(
    value = "scribe.service.time",
    description = "Time taken for a service method",
    extraTags = {"service", "report_update"})
  void processQueueItems() throws InterruptedException;

  @Async
  @Scheduled(fixedDelay = RETRY_CHECK_RATE_SECONDS, timeUnit = TimeUnit.SECONDS)
  @Timed(
    value = "scribe.service.time",
    description = "Time taken for a service method",
    extraTags = {"service", "report_update"})
  void processRetryBacklogQueueItems();

  @Async
  @Timed(
    value = "scribe.service.time",
    description = "Time taken for a service method",
    extraTags = {"service", "player_report"})
  CompletableFuture<ReportUpdateServiceImpl.FetchReportsResult> fetchReports(PlayerIdentity identity);

  @Timed(
    value = "scribe.service.time",
    description = "Time taken for a service method",
    extraTags = {"service", "report_update"})
  void queueUpdates(Set<PlayerIdentity> identities);

  @Timed(
    value = "scribe.service.time",
    description = "Time taken for a service method",
    extraTags = {"service", "report_update"})
  void queueUpdates(Set<PlayerIdentity> identities, int priority, int retries);
}
