package com.mordhauscribe.scribe.dto;

import java.sql.Timestamp;

public class SteamTicketProviderResponse {
  private String ticket;
  private Timestamp timestamp;
  private String error;

  public String getTicket() {
    return ticket;
  }

  public void setTicket(final String ticket) {
    this.ticket = ticket;
  }

  public Timestamp getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(final Timestamp timestamp) {
    this.timestamp = timestamp;
  }

  public String getError() {
    return error;
  }

  public void setError(final String error) {
    this.error = error;
  }
}
