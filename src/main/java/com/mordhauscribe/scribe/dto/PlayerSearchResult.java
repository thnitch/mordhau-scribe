package com.mordhauscribe.scribe.dto;

public class PlayerSearchResult {
  private final String playFabId;

  public PlayerSearchResult(final String playFabId) {
    this.playFabId = playFabId;
  }

  public String getPlayFabId() {
    return playFabId;
  }
}
