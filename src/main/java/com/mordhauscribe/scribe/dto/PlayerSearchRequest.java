package com.mordhauscribe.scribe.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

public class PlayerSearchRequest {

  @NotBlank
  @Size(max = 64)
  private String name;

  @Size(max = 32)
  private String platform;

  @Size(max = 32)
  private String platformAccountId;

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getPlatform() {
    return platform;
  }

  public void setPlatform(String platform) {
    this.platform = platform;
  }

  public String getPlatformAccountId() {
    return platformAccountId;
  }

  public void setPlatformAccountId(String platformAccountId) {
    this.platformAccountId = platformAccountId;
  }
}
