package com.mordhauscribe.scribe.dto;

public class ResolveVanityUrlResponse {
  ResolveVanityUrlResponseContent response;

  public ResolveVanityUrlResponseContent getResponse() {
    return response;
  }

  public void setResponse(final ResolveVanityUrlResponseContent response) {
    this.response = response;
  }

  @Override
  public String toString() {
    return "ResolveVanityUrlResponse{" + "response=" + response + '}';
  }

  public static class ResolveVanityUrlResponseContent {
    String steamid;
    Integer success;

    public String getSteamid() {
      return steamid;
    }

    public void setSteamid(final String steamid) {
      this.steamid = steamid;
    }

    public Integer getSuccess() {
      return success;
    }

    public void setSuccess(final Integer success) {
      this.success = success;
    }

    @Override
    public String toString() {
      return "ResolveVanityUrlResponseContent{"
          + "steamid='"
          + steamid
          + '\''
          + ", success="
          + success
          + '}';
    }
  }
}
