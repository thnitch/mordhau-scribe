package com.mordhauscribe.scribe.model;

import java.time.Instant;

public class PlayerHistoryEntry {
  private final Instant timestamp;
  private final Integer mmr;

  public PlayerHistoryEntry(final Instant timestamp, final Integer mmr) {
    this.timestamp = timestamp;
    this.mmr = mmr;
  }

  public Instant getTimestamp() {
    return timestamp;
  }

  public Integer getMmr() {
    return mmr;
  }
}
