package com.mordhauscribe.scribe.model;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class Leaderboard {
  private final List<LeaderboardEntry> entries;
  private final Instant timestamp;
  private final QueueType queueType;
  private Instant lastChange;
  @JsonIgnore private List<SnapshotDeltas> deltas;

  @JsonIgnore private Map<Long, SnapshotDeltas> deltaMap;

  @JsonInclude(Include.NON_NULL)
  private SnapshotDeltas defaultDelta;

  @JsonInclude(Include.NON_NULL)
  private SortedSet<Long> availableDeltas;

  private long playerCountInGame;
  private long playerCountInQueue;

  public Leaderboard(
      final List<LeaderboardEntry> entries,
      final Instant timestamp,
      final QueueType queueType) {
    this.entries = entries;
    this.timestamp = timestamp;
    this.queueType = queueType;
  }

  public List<LeaderboardEntry> getEntries() {
    return entries;
  }

  public Instant getTimestamp() {
    return timestamp;
  }

  public List<SnapshotDeltas> getDeltas() {
    return deltas;
  }

  public void setDeltas(final List<SnapshotDeltas> deltas) {
    this.deltas = deltas;
  }

  public SortedSet<Long> getAvailableDeltas() {
    return availableDeltas;
  }

  public void setAvailableDeltas(final SortedSet<Long> availableDeltas) {
    this.availableDeltas = availableDeltas;
  }

  public SnapshotDeltas getDefaultDelta() {
    return defaultDelta;
  }

  public void setDefaultDelta(final SnapshotDeltas defaultDelta) {
    this.defaultDelta = defaultDelta;
  }

  public Map<Long, SnapshotDeltas> getDeltaMap() {
    return deltaMap;
  }

  public void setDeltaMap(final Map<Long, SnapshotDeltas> deltaMap) {
    this.deltaMap = deltaMap;
  }

  public Instant getLastChange() {
    return lastChange;
  }

  public void setLastChange(final Instant lastChange) {
    this.lastChange = lastChange;
  }

  public Leaderboard copyWithDefaultDelta(final Long duration) {
    final Leaderboard copy = new Leaderboard(this.getEntries(), this.getTimestamp(), queueType);
    copy.setLastChange(this.getLastChange());
    copy.setAvailableDeltas(this.getAvailableDeltas());
    copy.setDeltas(this.getDeltas());
    copy.setDeltaMap(this.getDeltaMap());
    copy.setDefaultDelta(
        deltas.stream()
            .filter(delta -> delta.getDuration() == duration)
            .findAny()
            .orElse(new SnapshotDeltas(Map.of(), null, null, duration)));
    copy.setPlayerCountInGame(this.getPlayerCountInGame());
    copy.setPlayerCountInQueue(this.getPlayerCountInQueue());
    return copy;
  }

  public QueueType getQueueType() {
    return queueType;
  }

  public long getPlayerCountInGame() {
    return playerCountInGame;
  }

  public void setPlayerCountInGame(long playerCountInGame) {
    this.playerCountInGame = playerCountInGame;
  }

  public long getPlayerCountInQueue() {
    return playerCountInQueue;
  }

  public void setPlayerCountInQueue(long playerCountInQueue) {
    this.playerCountInQueue = playerCountInQueue;
  }
}
