package com.mordhauscribe.scribe.model;

import com.mordhauscribe.scribe.model.entity.PlayerIdentity;

public class ChangedEntry {
  private final PlayerIdentity playerIdentity;
  private final Integer mmr;
  private final Integer position;
  private final Outcome outcome;

  private ChangedEntry(
      final PlayerIdentity playerIdentity,
      final Integer mmr,
      final Integer position,
      final Outcome outcome) {
    this.playerIdentity = playerIdentity;
    this.mmr = mmr;
    this.position = position;
    this.outcome = outcome;
  }

  public static ChangedEntry ofDropout(final PlayerIdentity playerIdentity) {
    return new ChangedEntry(playerIdentity, null, null, Outcome.UNDEFINED);
  }

  public static ChangedEntry of(final LeaderboardEntry entry, final Outcome outcome) {
    return new ChangedEntry(
        new PlayerIdentity(entry.getPlayFabId(), entry.getPlatform(), entry.getPlatformAccountId()),
        entry.getMmr(),
        entry.getPosition(),
        outcome);
  }

  public PlayerIdentity getPlayerIdentity() {
    return playerIdentity;
  }

  public Integer getMmr() {
    return mmr;
  }

  public Integer getPosition() {
    return position;
  }

  public Outcome getOutcome() {
    return outcome;
  }
}
