package com.mordhauscribe.scribe.model;

public enum Outcome {
  WIN,
  LOSS,
  UNDEFINED
}
