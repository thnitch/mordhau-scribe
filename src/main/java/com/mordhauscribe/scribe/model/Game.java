package com.mordhauscribe.scribe.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import java.util.Objects;

public class Game {
  private final String region;
  @JsonIgnore private final String lobbyID;
  @JsonIgnore private final String version;
  @JsonIgnore private final long runTime;
  @JsonIgnore private final String serverName;
  @JsonIgnore private final String serverIPV4Address;
  private final String mapName;
  private final boolean isOfficial;
  private final boolean isModded;
  private final String gameMode;
  private final String queueName;
  private final String Location;
  @JsonIgnore private final int maxPlayers;
  @JsonIgnore private final int reservedSlots;
  @JsonIgnore private List<String> players;

  public Game(
      String region,
      String lobbyID,
      String version,
      long runTime,
      String serverName,
      String serverIPV4Address,
      String mapName,
      boolean isOfficial,
      boolean isModded,
      String gameMode,
      String queueName,
      String Location,
      int maxPlayers,
      int reservedSlots,
      List<String> players) {
    this.region = region;
    this.lobbyID = lobbyID;
    this.version = version;
    this.runTime = runTime;
    this.serverName = serverName;
    this.serverIPV4Address = serverIPV4Address;
    this.mapName = mapName;
    this.isOfficial = isOfficial;
    this.isModded = isModded;
    this.gameMode = gameMode;
    this.queueName = queueName;
    this.Location = Location;
    this.maxPlayers = maxPlayers;
    this.reservedSlots = reservedSlots;
    this.players = players;
  }

  public String getRegion() {
    return region;
  }

  @JsonIgnore
  public String getLobbyID() {
    return lobbyID;
  }

  @JsonIgnore
  public String getVersion() {
    return version;
  }

  @JsonIgnore
  public long getRunTime() {
    return runTime;
  }

  @JsonIgnore
  public String getServerName() {
    return serverName;
  }

  @JsonIgnore
  public String getServerIPV4Address() {
    return serverIPV4Address;
  }

  public String getMapName() {
    return mapName;
  }

  public boolean isOfficial() {
    return isOfficial;
  }

  public boolean isModded() {
    return isModded;
  }

  public String getGameMode() {
    return gameMode;
  }

  public String getQueueName() {
    return queueName;
  }

  public String getLocation() {
    return Location;
  }

  @JsonIgnore
  public int getMaxPlayers() {
    return maxPlayers;
  }

  @JsonIgnore
  public int getReservedSlots() {
    return reservedSlots;
  }

  @JsonIgnore
  public List<String> getPlayers() {
    return players;
  }

  public void setPlayers(List<String> players) {
    this.players = players;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) return true;
    if (obj == null || obj.getClass() != this.getClass()) return false;
    var that = (Game) obj;
    return Objects.equals(this.region, that.region)
        && Objects.equals(this.lobbyID, that.lobbyID)
        && Objects.equals(this.version, that.version)
        && this.runTime == that.runTime
        && Objects.equals(this.serverName, that.serverName)
        && Objects.equals(this.serverIPV4Address, that.serverIPV4Address)
        && Objects.equals(this.mapName, that.mapName)
        && this.isOfficial == that.isOfficial
        && this.isModded == that.isModded
        && Objects.equals(this.gameMode, that.gameMode)
        && Objects.equals(this.queueName, that.queueName)
        && Objects.equals(this.Location, that.Location)
        && this.maxPlayers == that.maxPlayers
        && this.reservedSlots == that.reservedSlots
        && Objects.equals(this.players, that.players);
  }
}
