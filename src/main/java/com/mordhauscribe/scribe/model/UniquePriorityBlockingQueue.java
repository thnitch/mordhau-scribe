package com.mordhauscribe.scribe.model;

import com.google.common.util.concurrent.ForwardingBlockingQueue;
import java.time.Instant;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

public class UniquePriorityBlockingQueue<E> extends ForwardingBlockingQueue<E> {
  private final PriorityBlockingQueue<E> delegate;

  public UniquePriorityBlockingQueue(PriorityBlockingQueue<E> delegate) {
    this.delegate = delegate;
  }

  @Override
  public boolean addAll(Collection<? extends E> collection) {
    final List<? extends E> nonDuplicates =
        collection.stream().filter(item -> !delegate.contains(item)).toList();
    return super.addAll(nonDuplicates);
  }

  @Override
  public boolean add(E element) {
    if (delegate.contains(element)) return false;

    return super.add(element);
  }

  @Override
  public boolean offer(E element) {
    if (delegate.contains(element)) return false;
    return super.offer(element);
  }

  @Override
  protected BlockingQueue<E> delegate() {
    return delegate;
  }

  public static final class UniquePriorityBlockingQueueItem<T> {
    private int priority;
    private int retries;
    private int timesRetried;
    private Instant retryAfter;
    private final T item;

    public UniquePriorityBlockingQueueItem(int priority, T item) {
      this.priority = priority;
      this.item = item;
    }
    
    public UniquePriorityBlockingQueueItem(int priority, int retries, T item) {
      this.priority = priority;
      this.retries = retries;
      this.item = item;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      UniquePriorityBlockingQueueItem<T> that = (UniquePriorityBlockingQueueItem<T>) o;

      return item.equals(that.item);
    }

    public T getItem() {
      return item;
    }

    public int getPriority() {
      return priority;
    }

    public void setPriority(int priority) {
      this.priority = priority;
    }

    public int getRetries() {
      return retries;
    }

    public void decrementRetries() {
      this.retries--;
      this.timesRetried++;
    }

    public int getTimesRetried() {
      return timesRetried;
    }

    public Instant getRetryAfter() {
      return retryAfter;
    }

    public void setRetryAfter(Instant retryAfter) {
      this.retryAfter = retryAfter;
    }
  }

  public static class PriorityComparator implements Comparator<UniquePriorityBlockingQueueItem> {
    @Override
    public int compare(UniquePriorityBlockingQueueItem o1, UniquePriorityBlockingQueueItem o2) {
      return Integer.compare(o1.getPriority(), o2.getPriority());
    }
  }
}
