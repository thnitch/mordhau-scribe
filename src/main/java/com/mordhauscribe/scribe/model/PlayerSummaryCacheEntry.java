package com.mordhauscribe.scribe.model;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public record PlayerSummaryCacheEntry(PlayerSummary playerSummary, Instant timestamp) {
  public static final int CACHE_ENTRY_TTL_HOURS = 2;
  public static final int CACHE_ENTRY_MAX_AGE_HOURS = 6;

  public boolean canBeUpdated() {
    return this.timestamp.isBefore(
        Instant.now().minus(CACHE_ENTRY_TTL_HOURS, ChronoUnit.HOURS));
  }

  public boolean mustBeRemoved() {
    return this.timestamp.isBefore(
        Instant.now().minus(CACHE_ENTRY_MAX_AGE_HOURS, ChronoUnit.HOURS));
  }
}
