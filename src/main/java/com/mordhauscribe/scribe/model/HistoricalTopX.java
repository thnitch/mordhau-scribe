package com.mordhauscribe.scribe.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import java.time.Instant;
import java.util.List;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public record HistoricalTopX(
    Instant timestamp, List<String> names, List<List<PlayerHistoryEntry>> leaderboardSnapshots) {}
