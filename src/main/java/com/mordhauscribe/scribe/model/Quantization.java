package com.mordhauscribe.scribe.model;

import java.time.Duration;

import com.mordhauscribe.scribe.service.LeaderboardService;

public enum Quantization {
  UPDATEINTERVAL(Duration.ofSeconds(LeaderboardService.SCHEDULED_LEADERBOARD_UPDATE_DELAY_SECONDS)),
  DAY(Duration.ofDays(1)),
  LIFETIME(Duration.ofDays(Integer.MAX_VALUE));

  private final Duration duration;

  Quantization(final Duration duration) {
    this.duration = duration;
  }

  public Duration getDuration() {
    return duration;
  }
}
