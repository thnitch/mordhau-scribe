package com.mordhauscribe.scribe.model;

public enum QueueType {
  SOLO("DuelRankLeaderboard", "Duel"),
  TEAM("TeamfightRankLeaderboard", "Teamfight");

  private final String cloudFunctionParam;
  private final String queueName;

  QueueType(final String cloudFunctionParam, String queueName) {
    this.cloudFunctionParam = cloudFunctionParam;
    this.queueName = queueName;
  }

  public String getCloudFunctionParam() {
    return cloudFunctionParam;
  }

  public String getQueueName() {
    return queueName;
  }
}
