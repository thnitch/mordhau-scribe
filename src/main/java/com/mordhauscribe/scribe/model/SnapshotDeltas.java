package com.mordhauscribe.scribe.model;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class SnapshotDeltas {
  private final Map<String, SnapshotDelta> deltas;
  private final Instant startTime;
  private final Instant endTime;
  private final long duration;

  public SnapshotDeltas(
      final Map<String, SnapshotDelta> deltas,
      final Instant startTime,
      final Instant endTime,
      final long duration) {
    final boolean atLeastOneChange =
        deltas.values().stream()
            .anyMatch(
                delta ->
                    delta == null || delta.mmr() != 0 || delta.position() != 0 || delta.newEntry());
    this.deltas = atLeastOneChange ? deltas : new HashMap<>();
    this.startTime = startTime;
    this.endTime = endTime;
    this.duration = duration;
  }

  public SnapshotDeltas(
          final Instant startTime, final Instant endTime, final long duration) {
    this.deltas = new HashMap<>();
    this.startTime = startTime;
    this.endTime = endTime;
    this.duration = duration;
  }

  public Map<String, SnapshotDelta> getDeltas() {
    return deltas;
  }

  public Instant getStartTime() {
    return startTime;
  }

  public Instant getEndTime() {
    return endTime;
  }

  public long getDuration() {
    return duration;
  }
}
