package com.mordhauscribe.scribe.model;

public record PlayFabAccountInfo(String PlayFabId, String EntityId, String Platform, String PlatformAccountId, String Name, String Type) {}
