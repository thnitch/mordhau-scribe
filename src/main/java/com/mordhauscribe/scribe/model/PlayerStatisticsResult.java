package com.mordhauscribe.scribe.model;

import com.mordhauscribe.scribe.model.entity.LatestPlayerReport;
import com.mordhauscribe.scribe.model.entity.PlayerIdentity;
import java.util.Optional;

public record PlayerStatisticsResult(Optional<PlayerIdentity> updatedPlayerIdentity, LatestPlayerReport latestPlayerReport) {}
