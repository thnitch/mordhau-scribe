package com.mordhauscribe.scribe.model.nativequeryresult;

import java.time.Instant;

public class ReconstructedReport {

  private Long id;
  private String playFabId;
  private String platform;
  private String platformAccountId;
  private String name;
  private Instant gameTimestamp;
  private Integer mmr;
  private Integer position;

  public ReconstructedReport(Long id, String playFabId, String platform, String platformAccountId, String name, Instant gameTimestamp, Integer mmr, Integer position) {
    this.id = id;
    this.playFabId = playFabId;
    this.platform = platform;
    this.platformAccountId = platformAccountId;
    this.name = name;
    this.gameTimestamp = gameTimestamp;
    this.mmr = mmr;
    this.position = position;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getPlayFabId() {
    return playFabId;
  }

  public void setPlayFabId(String playFabId) {
    this.playFabId = playFabId;
  }

  public String getPlatform() {
    return platform;
  }

  public void setPlatform(String platform) {
    this.platform = platform;
  }

  public String getPlatformAccountId() {
    return platformAccountId;
  }

  public void setPlatformAccountId(String platformAccountId) {
    this.platformAccountId = platformAccountId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Instant getGameTimestamp() {
    return gameTimestamp;
  }

  public void setGameTimestamp(Instant gameTimestamp) {
    this.gameTimestamp = gameTimestamp;
  }

  public Integer getMmr() {
    return mmr;
  }

  public void setMmr(Integer mmr) {
    this.mmr = mmr;
  }

  public Integer getPosition() {
    return position;
  }

  public void setPosition(Integer position) {
    this.position = position;
  }
}
