package com.mordhauscribe.scribe.model;

import com.playfab.PlayFabClientModels.CollectionFilter;
import com.playfab.PlayFabClientModels.Container_Dictionary_String_String;
import java.util.ArrayList;
import java.util.HashMap;

public class PlayFabRankedGamesFilter extends CollectionFilter {
  public PlayFabRankedGamesFilter() {
    ArrayList<Container_Dictionary_String_String> includes = new ArrayList<>();
    Container_Dictionary_String_String duelFilterData = new Container_Dictionary_String_String();
    duelFilterData.Data = new HashMap<>();
    duelFilterData.Data.put("Visibility", "1");
    duelFilterData.Data.put("IsOfficial", "true");
    duelFilterData.Data.put("QueueName", "Duel");

    Container_Dictionary_String_String teamfightFilterData = new Container_Dictionary_String_String();
    teamfightFilterData.Data = new HashMap<>();
    teamfightFilterData.Data.put("Visibility", "1");
    teamfightFilterData.Data.put("IsOfficial", "true");
    teamfightFilterData.Data.put("QueueName", "Teamfight");

    includes.add(duelFilterData);
    includes.add(teamfightFilterData);
    this.Includes = includes;
  }
}
