package com.mordhauscribe.scribe.model.entity;

import com.mordhauscribe.scribe.model.QueueType;
import jakarta.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@Embeddable
public class LatestPlayerReport implements Serializable {

  @Column(nullable = false)
  private Instant updatedPlayerReportAt;

  private Integer duelRank;
  private Integer duelRankSamples;
  private Instant duelRankTimestamp;

  private Integer teamfightRank;
  private Integer teamfightRankSamples;
  private Instant teamfightRankTimestamp;

  public LatestPlayerReport(
      final Instant updatedPlayerReportAt,
      final Integer duelRank,
      final Integer duelRankSamples,
      final Instant duelRankTimestamp,
      final Integer teamfightRank,
      final Integer teamfightRankSamples,
      final Instant teamfightRankTimestamp) {
    this.updatedPlayerReportAt = updatedPlayerReportAt;
    this.duelRank = duelRank;
    this.duelRankSamples = duelRankSamples;
    this.duelRankTimestamp = duelRankTimestamp;
    this.teamfightRank = teamfightRank;
    this.teamfightRankSamples = teamfightRankSamples;
    this.teamfightRankTimestamp = teamfightRankTimestamp;
  }

  public LatestPlayerReport() {}

  public Instant getUpdatedPlayerReportAt() {
    return updatedPlayerReportAt;
  }

  public void setUpdatedPlayerReportAt(Instant updatedPlayerReportAt) {
    this.updatedPlayerReportAt = updatedPlayerReportAt;
  }

  public Integer getDuelRank() {
    return duelRank;
  }

  public void setDuelRank(Integer duelRank) {
    this.duelRank = duelRank;
  }

  public Integer getDuelRankSamples() {
    return duelRankSamples;
  }

  public void setDuelRankSamples(Integer duelRankSamples) {
    this.duelRankSamples = duelRankSamples;
  }

  public Instant getDuelRankTimestamp() {
    return duelRankTimestamp;
  }

  public void setDuelRankTimestamp(Instant duelRankTimestamp) {
    this.duelRankTimestamp = duelRankTimestamp;
  }

  public Integer getTeamfightRank() {
    return teamfightRank;
  }

  public void setTeamfightRank(Integer teamfightRank) {
    this.teamfightRank = teamfightRank;
  }

  public Integer getTeamfightRankSamples() {
    return teamfightRankSamples;
  }

  public void setTeamfightRankSamples(Integer teamfightRankSamples) {
    this.teamfightRankSamples = teamfightRankSamples;
  }

  public Instant getTeamfightRankTimestamp() {
    return teamfightRankTimestamp;
  }

  public void setTeamfightRankTimestamp(Instant teamfightRankTimestamp) {
    this.teamfightRankTimestamp = teamfightRankTimestamp;
  }

  public Map<QueueType, Report> generateReports(
      final PlayerIdentity playerIdentity,
      final LatestPlayerReport other,
      Map<QueueType, Report> latestReports) {
    final HashMap<QueueType, Report> newReports = new HashMap<>();

    if (this.duelRank != null) {
      final Report latestDuelReport = latestReports.get(QueueType.SOLO);

      Instant latestGameTimestamp = null;
      if (latestDuelReport != null) {
        latestGameTimestamp = latestDuelReport.getGameTimestamp();
      }

      if (latestDuelReport != null
          && other != null
          && other.duelRankTimestamp != null
          && (latestGameTimestamp == null
              || other.duelRankTimestamp.isAfter(latestGameTimestamp))) {
        latestGameTimestamp = other.duelRankTimestamp;
      }

      if (latestGameTimestamp == null || this.duelRankTimestamp.isAfter(latestGameTimestamp)) {
        newReports.put(
            QueueType.SOLO,
            new Report(
                playerIdentity,
                QueueType.SOLO,
                this.getUpdatedPlayerReportAt(),
                this.getDuelRank(),
                this.getDuelRankSamples(),
                this.getDuelRankTimestamp()));
      }
    }

    if (this.teamfightRank != null) {
      final Report latestTeamfightReport = latestReports.get(QueueType.TEAM);

      Instant latestGameTimestamp = null;
      if (latestTeamfightReport != null) {
        latestGameTimestamp = latestTeamfightReport.getGameTimestamp();
      }

      if (latestTeamfightReport != null
        && other != null
        && other.teamfightRankTimestamp != null
        && (latestGameTimestamp == null
        || other.teamfightRankTimestamp.isAfter(latestGameTimestamp))) {
        latestGameTimestamp = other.teamfightRankTimestamp;
      }

      if (latestGameTimestamp == null || this.teamfightRankTimestamp.isAfter(latestGameTimestamp)) {
        newReports.put(
          QueueType.TEAM,
          new Report(
            playerIdentity,
            QueueType.TEAM,
            this.getUpdatedPlayerReportAt(),
            this.getTeamfightRank(),
            this.getTeamfightRankSamples(),
            this.getTeamfightRankTimestamp()));
      }
    }

    return newReports;
  }
}
