package com.mordhauscribe.scribe.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mordhauscribe.scribe.model.QueueType;
import com.mordhauscribe.scribe.model.nativequeryresult.ReconstructedReport;
import jakarta.persistence.*;
import java.time.Instant;
import java.util.Objects;

@Entity
@Table(
    indexes = {
      @Index(name = "player_id_index", columnList = "player_id"),
      @Index(name = "queue_type_index", columnList = "queueType"),
      @Index(name = "game_timestamp_index", columnList = "gameTimestamp"),
      @Index(name = "timestamp_index", columnList = "timestamp"),
      @Index(name = "mmr_index", columnList = "mmr")
    })
@SqlResultSetMappings({
  @SqlResultSetMapping(
      name = "ReportMapping",
      entities = {
        @EntityResult(
            entityClass = Report.class,
            fields = {
              @FieldResult(column = "id", name = "id"),
              @FieldResult(column = "player_id", name = "playerIdentity"),
              @FieldResult(column = "timestamp", name = "timestamp"),
              @FieldResult(column = "game_timestamp", name = "gameTimestamp"),
              @FieldResult(column = "mmr", name = "mmr"),
              @FieldResult(column = "games", name = "games"),
              @FieldResult(column = "queue_type", name = "queueType")
            })
      }),
  @SqlResultSetMapping(
      name = "ReconstructedReportMapping",
      classes =
          @ConstructorResult(
              targetClass = ReconstructedReport.class,
              columns = {
                @ColumnResult(name = "id", type = Long.class),
                @ColumnResult(name = "player_id", type = String.class),
                @ColumnResult(name = "platform", type = String.class),
                @ColumnResult(name = "platform_account_id", type = String.class),
                @ColumnResult(name = "name", type = String.class),
                @ColumnResult(name = "game_timestamp", type = Instant.class),
                @ColumnResult(name = "mmr", type = Integer.class),
                @ColumnResult(name = "position", type = Integer.class)
              })),
  @SqlResultSetMapping(
      name = "RankDistribution",
      classes = {
        @ConstructorResult(
            targetClass = Report.RankDistributionResult.class,
            columns = {
              @ColumnResult(name = "rank", type = Integer.class),
              @ColumnResult(name = "player_count", type = Integer.class)
            })
      }),
  @SqlResultSetMapping(
      name = "WinLoss",
      classes = {
        @ConstructorResult(
            targetClass = Report.WinLossResult.class,
            columns = {
              @ColumnResult(name = "player_id", type = String.class),
              @ColumnResult(name = "wins", type = Integer.class),
              @ColumnResult(name = "losses", type = Integer.class),
              @ColumnResult(name = "unknown", type = Integer.class)
            })
      }),
})
@NamedNativeQueries({
  @NamedNativeQuery(
      name = "Report.constructLeaderboard",
      query =
          "SELECT" +
            "     id," +
            "     player_identity.player_id," +
            "     player_identity.platform," +
            "     player_identity.platform_account_id," +
            "     player_identity.name," +
            "     mmr," +
            "     ROW_NUMBER() over (order by mmr DESC, game_timestamp) AS position," +
            "     game_timestamp" +
            " FROM (" +
            "          SELECT" +
            "              id," +
            "              player_id," +
            "              mmr," +
            "              game_timestamp," +
            "              timestamp," +
            "              ROW_NUMBER() OVER(PARTITION BY player_id ORDER BY game_timestamp DESC) AS rn" +
            "          FROM" +
            "              report" +
            "          WHERE queue_type = :queueType" +
            "            AND game_timestamp BETWEEN :startDate AND :endDate" +
            "      ) latest_report join player_identity on latest_report.player_id = player_identity.player_id" +
            " WHERE rn = 1" +
            "   AND mmr IS NOT NULL" +
            " ORDER BY mmr DESC, game_timestamp" +
            "     LIMIT :limit ;",
      resultSetMapping = "ReconstructedReportMapping"),
  @NamedNativeQuery(
      name = "Report.constructAllTimeLeaderboard",
      query =
        "SELECT" +
          "     id," +
          "     player_identity.player_id," +
          "     player_identity.platform," +
          "     player_identity.platform_account_id," +
          "     player_identity.name," +
          "     mmr," +
          "     ROW_NUMBER() over (order by mmr DESC, game_timestamp) AS position," +
          "     game_timestamp" +
          " FROM (" +
          "          SELECT" +
          "              id," +
          "              player_id," +
          "              mmr," +
          "              game_timestamp," +
          "              timestamp," +
          "              ROW_NUMBER() OVER(PARTITION BY player_id ORDER BY game_timestamp DESC) AS rn" +
          "          FROM" +
          "              report" +
          "          WHERE queue_type = :queueType" +
          "      ) latest_report join player_identity on latest_report.player_id = player_identity.player_id" +
          " WHERE rn = 1" +
          "   AND mmr IS NOT NULL" +
          " ORDER BY mmr DESC, game_timestamp" +
          "     LIMIT :limit ;",
      resultSetMapping = "ReconstructedReportMapping"),
  @NamedNativeQuery(
      name = "Report.constructPeakMmrLeaderboard",
      query =
          "WITH ranked_reports AS (SELECT *,"
              + "                                ROW_NUMBER() OVER (PARTITION BY player_id ORDER BY mmr DESC, game_timestamp) AS rn"
              + "                         FROM report"
              + "                         where queue_type = :queueType)"
              + " SELECT id,"
              + "        ranked_reports.player_id AS player_id,"
              + "        platform,"
              + "        platform_account_id,"
              + "        name,"
              + "        mmr,"
              + "        ROW_NUMBER() over (order by ranked_reports.mmr DESC, game_timestamp) AS position,"
              + "        ranked_reports.game_timestamp,"
              + "        queue_type"
              + " FROM ranked_reports"
              + "          LEFT JOIN player_identity"
              + "                    ON ranked_reports.player_id = player_identity.player_id"
              + " WHERE ranked_reports.rn = 1"
              + " ORDER BY ranked_reports.mmr DESC, game_timestamp"
              + " LIMIT :limit ;",
      resultSetMapping = "ReconstructedReportMapping"),
  @NamedNativeQuery(
      name = "Report.closestBeforeOrFirstSince",
      query =
          "SELECT report.*"
              + " FROM report"
              + "          LEFT JOIN (SELECT player_id, MAX(game_timestamp) AS firstDate"
              + "                     FROM report"
              + "                     WHERE queue_type = :queueType"
              + "                       AND player_id in :playerIds"
              + "                       AND report.game_timestamp < :startDate"
              + "                     GROUP BY player_id) closestDate"
              + "                    ON closestDate.player_id = report.player_id"
              + " WHERE queue_type = :queueType"
              + "   AND report.player_id IN :playerIds"
              + "   AND report.game_timestamp >= COALESCE(firstDate, :startDate)"
              + " GROUP BY player_id;",
      resultSetMapping = "ReportMapping"),
  @NamedNativeQuery(
      name = "Report.latest",
      query =
          "select report.*"
              + "from report"
              + "          left join (select queue_type, max(game_timestamp) as game_timestamp"
              + "                     from report"
              + "                     where player_id = :playerId"
              + "                     group by queue_type) as latest on latest.queue_type = report.queue_type"
              + " where player_id = :playerId and report.game_timestamp = latest.game_timestamp and report.queue_type = latest.queue_type;",
      resultSetMapping = "ReportMapping"),
  @NamedNativeQuery(
      name = "Report.determineDuelRankDistribution",
      resultSetMapping = "RankDistribution",
      query =
          "select rank_group as rank, count(*) as player_count "
              + "from (select case"
              + "                 when duel_rank between 0 and 100 then (0)"
              + "                 when duel_rank between 100 and 200 then (100)"
              + "                 when duel_rank between 200 and 300 then (200)"
              + "                 when duel_rank between 300 and 400 then (300)"
              + "                 when duel_rank between 400 and 500 then (400)"
              + "                 when duel_rank between 500 and 600 then (500)"
              + "                 when duel_rank between 600 and 700 then (600)"
              + "                 when duel_rank between 700 and 800 then (700)"
              + "                 when duel_rank between 800 and 900 then (800)"
              + "                 when duel_rank between 900 and 1000 then (900)"
              + "                 when duel_rank between 1000 and 1100 then (1000)"
              + "                 when duel_rank between 1100 and 1200 then (1100)"
              + "                 when duel_rank between 1200 and 1300 then (1200)"
              + "                 when duel_rank between 1300 and 1400 then (1300)"
              + "                 when duel_rank between 1400 and 1500 then (1400)"
              + "                 when duel_rank between 1500 and 1600 then (1500)"
              + "                 when duel_rank between 1600 and 1700 then (1600)"
              + "                 when duel_rank between 1700 and 1800 then (1700)"
              + "                 when duel_rank between 1800 and 1900 then (1800)"
              + "                 when duel_rank between 1900 and 2000 then (1900)"
              + "                 when duel_rank between 2000 and 2100 then (2000)"
              + "                 when duel_rank between 2100 and 2200 then (2100)"
              + "                 when duel_rank between 2200 and 2300 then (2200)"
              + "                 when duel_rank between 2300 and 2400 then (2300)"
              + "                 when duel_rank between 2400 and 2500 then (2400)"
              + "                 when duel_rank between 2500 and 2600 then (2500)"
              + "                 when duel_rank between 2600 and 2700 then (2600)"
              + "                 when duel_rank between 2700 and 2800 then (2700)"
              + "                 when duel_rank between 2800 and 2900 then (2800)"
              + "                 when duel_rank between 2900 and 3000 then (2900)"
              + "                 when duel_rank between 3000 and 3100 then (3000)"
              + "                 when duel_rank between 3100 and 3200 then (3100)"
              + "                 when duel_rank between 3200 and 3300 then (3200)"
              + "                 when duel_rank between 3300 and 3400 then (3300)"
              + "                 when duel_rank between 3400 and 3500 then (3400)"
              + "                 when duel_rank between 3500 and 3600 then (3500)"
              + "                 when duel_rank between 3600 and 3700 then (3600)"
              + "                 when duel_rank between 3700 and 3800 then (3700)"
              + "                 when duel_rank between 3800 and 3900 then (3800)"
              + "                 when duel_rank between 3900 and 4000 then (3900)"
              + "                 end as rank_group"
              + "      from player_identity where duel_rank is not null) as rank_distribution group by rank_group order by rank_group;"),
  @NamedNativeQuery(
      name = "Report.determineTeamfightRankDistribution",
      resultSetMapping = "RankDistribution",
      query =
          "select rank_group as rank, count(*) as player_count "
              + "from (select case"
              + "                 when teamfight_rank between 0 and 100 then (0)"
              + "                 when teamfight_rank between 100 and 200 then (100)"
              + "                 when teamfight_rank between 200 and 300 then (200)"
              + "                 when teamfight_rank between 300 and 400 then (300)"
              + "                 when teamfight_rank between 400 and 500 then (400)"
              + "                 when teamfight_rank between 500 and 600 then (500)"
              + "                 when teamfight_rank between 600 and 700 then (600)"
              + "                 when teamfight_rank between 700 and 800 then (700)"
              + "                 when teamfight_rank between 800 and 900 then (800)"
              + "                 when teamfight_rank between 900 and 1000 then (900)"
              + "                 when teamfight_rank between 1000 and 1100 then (1000)"
              + "                 when teamfight_rank between 1100 and 1200 then (1100)"
              + "                 when teamfight_rank between 1200 and 1300 then (1200)"
              + "                 when teamfight_rank between 1300 and 1400 then (1300)"
              + "                 when teamfight_rank between 1400 and 1500 then (1400)"
              + "                 when teamfight_rank between 1500 and 1600 then (1500)"
              + "                 when teamfight_rank between 1600 and 1700 then (1600)"
              + "                 when teamfight_rank between 1700 and 1800 then (1700)"
              + "                 when teamfight_rank between 1800 and 1900 then (1800)"
              + "                 when teamfight_rank between 1900 and 2000 then (1900)"
              + "                 when teamfight_rank between 2000 and 2100 then (2000)"
              + "                 when teamfight_rank between 2100 and 2200 then (2100)"
              + "                 when teamfight_rank between 2200 and 2300 then (2200)"
              + "                 when teamfight_rank between 2300 and 2400 then (2300)"
              + "                 when teamfight_rank between 2400 and 2500 then (2400)"
              + "                 when teamfight_rank between 2500 and 2600 then (2500)"
              + "                 when teamfight_rank between 2600 and 2700 then (2600)"
              + "                 when teamfight_rank between 2700 and 2800 then (2700)"
              + "                 when teamfight_rank between 2800 and 2900 then (2800)"
              + "                 when teamfight_rank between 2900 and 3000 then (2900)"
              + "                 when teamfight_rank between 3000 and 3100 then (3000)"
              + "                 when teamfight_rank between 3100 and 3200 then (3100)"
              + "                 when teamfight_rank between 3200 and 3300 then (3200)"
              + "                 when teamfight_rank between 3300 and 3400 then (3300)"
              + "                 when teamfight_rank between 3400 and 3500 then (3400)"
              + "                 when teamfight_rank between 3500 and 3600 then (3500)"
              + "                 when teamfight_rank between 3600 and 3700 then (3600)"
              + "                 when teamfight_rank between 3700 and 3800 then (3700)"
              + "                 when teamfight_rank between 3800 and 3900 then (3800)"
              + "                 when teamfight_rank between 3900 and 4000 then (3900)"
              + "                 end as rank_group"
              + "      from player_identity where teamfight_rank is not null) as rank_distribution group by rank_group order by rank_group;"),
  @NamedNativeQuery(
      name = "Report.determineWinLoss",
      resultSetMapping = "WinLoss",
      query =
          "with with_previous as (select player_id,"
              + "                               queue_type,"
              + "                               game_timestamp,"
              + "                               mmr,"
              + "                               LAG(mmr, 1) OVER ("
              + "                                   PARTITION BY player_id, queue_type"
              + "                                   ORDER BY queue_type, game_timestamp"
              + "                                   ) previous_mmr"
              + "                        from report"
              + "                        where player_id in :playerIds"
              + "                          and queue_type = :queueType"
              + "                          and game_timestamp <= :beforeTimestamp"
              + "                        order by game_timestamp)"
              + " select player_id,"
              + "        COALESCE(SUM(IF(mmr > previous_mmr, 1, 0)), 0)                         AS wins,"
              + "        COALESCE(SUM(IF(mmr < previous_mmr, 1, 0)), 0)                         AS losses,"
              + "        COALESCE(SUM(IF(mmr = previous_mmr or previous_mmr is null, 1, 0)), 0) AS unknown"
              + " from with_previous"
              + " group by player_id;"),
})
public class Report {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne(optional = false)
  @JoinColumn(name = "player_id")
  private PlayerIdentity playerIdentity;

  @Column(nullable = false)
  @Enumerated(EnumType.STRING)
  private QueueType queueType;

  @Column(nullable = false)
  private Instant timestamp;

  @Column(nullable = false)
  private Integer mmr;

  private Integer games;

  @Column(nullable = false)
  private Instant gameTimestamp;

  public Report(
      PlayerIdentity playerIdentity,
      QueueType queueType,
      Instant timestamp,
      Integer mmr,
      Integer games,
      Instant gameTimestamp) {
    this.playerIdentity = playerIdentity;
    this.queueType = queueType;
    this.timestamp = timestamp;
    this.mmr = mmr;
    this.games = games;
    this.gameTimestamp = gameTimestamp;
  }

  public Report() {}

  @JsonIgnore
  public Long getId() {
    return id;
  }

  @JsonIgnore
  public PlayerIdentity getPlayerIdentity() {
    return playerIdentity;
  }

  public QueueType getQueueType() {
    return queueType;
  }

  @JsonIgnore
  public Instant getTimestamp() {
    return timestamp;
  }

  public Integer getMmr() {
    return mmr;
  }

  public Integer getGames() {
    return games;
  }

  public Instant getGameTimestamp() {
    return gameTimestamp;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public void setPlayerIdentity(PlayerIdentity playerIdentity) {
    this.playerIdentity = playerIdentity;
  }

  public void setQueueType(QueueType queueType) {
    this.queueType = queueType;
  }

  public void setTimestamp(Instant timestamp) {
    this.timestamp = timestamp;
  }

  public void setMmr(Integer mmr) {
    this.mmr = mmr;
  }

  public void setGames(Integer games) {
    this.games = games;
  }

  public void setGameTimestamp(Instant gameTimestamp) {
    this.gameTimestamp = gameTimestamp;
  }

  public record RankDistributionResult(Integer rank, Integer playerCount) {}

  public record WinLossResult(String playerId, Integer wins, Integer losses, Integer unknown) {}

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    final Report that = (Report) o;

    if (id != null && Objects.equals(id, that.getId())) return true;
    return playerIdentity.equals(that.playerIdentity)
        && queueType.equals(that.queueType)
        && gameTimestamp.equals(that.gameTimestamp)
        && mmr.equals(that.mmr)
        && games.equals(that.games);
  }

  @Override
  public int hashCode() {
    return 31;
  }
}
