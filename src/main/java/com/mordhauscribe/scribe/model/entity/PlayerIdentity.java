package com.mordhauscribe.scribe.model.entity;

import jakarta.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

@Entity
@Table(
    indexes = {
      @Index(name = "title_player_account_id_index", columnList = "title_player_account_id"),
      @Index(name = "duel_rank_index", columnList = "duelRank"),
      @Index(name = "duel_rank_timestamp_index", columnList = "duelRankTimestamp"),
      @Index(name = "teamfight_rank_index", columnList = "teamfightRank"),
      @Index(name = "teamfight_rank_timestamp_index", columnList = "teamfightRankTimestamp")
    })
@SqlResultSetMappings({
  @SqlResultSetMapping(
      name = "platformDistribution",
      classes = {
        @ConstructorResult(
            targetClass = PlayerIdentity.PlatformDistributionResult.class,
            columns = {
              @ColumnResult(name = "platform", type = String.class),
              @ColumnResult(name = "player_count", type = Integer.class)
            })
      })
})
@NamedNativeQueries({
  @NamedNativeQuery(
      name = "PlayerIdentity.determinePlatformDistribution",
      resultSetMapping = "platformDistribution",
      query =
          "select platform, count(*) as player_count from player_identity "
              + "where last_seen_casual > (DATE_ADD(NOW(), INTERVAL -:withinLastXMinutes MINUTE)) "
              + "or last_seen_ranked > (DATE_ADD(NOW(), INTERVAL -:withinLastXMinutes MINUTE)) "
              + "GROUP BY platform")
})
public class PlayerIdentity implements Serializable {

  @Id
  @Column(name = "player_id")
  private String playFabId;

  @Column(name = "title_player_account_id")
  private String titlePlayerAccountId;

  private String platform;

  private String platformAccountId;

  private String name;

  private Instant lastSeenRanked;

  private Instant lastSeenCasual;

  @Embedded
  @Access(AccessType.PROPERTY)
  @AssociationOverride(name = "playerIdentity", joinColumns = @JoinColumn(insertable = false, updatable = false))
  private LatestPlayerReport latestPlayerReport;

  public PlayerIdentity() {}

  public PlayerIdentity(
      final String playFabId, final String platform, final String platformAccountId) {
    this.playFabId = playFabId;
    this.platform = platform;
    this.platformAccountId = platformAccountId;
  }

  public PlayerIdentity(final String playFabId, final String titlePlayerAccountId) {
    this.playFabId = playFabId;
    this.titlePlayerAccountId = titlePlayerAccountId;
  }

  public PlayerIdentity(
      String playFabId,
      String titlePlayerAccountId,
      String platform,
      String platformAccountId,
      String name) {
    this.playFabId = playFabId;
    this.titlePlayerAccountId = titlePlayerAccountId;
    this.platform = platform;
    this.platformAccountId = platformAccountId;
    this.name = name;
  }

  public void applyUpdateFromIdentiy(PlayerIdentity updatedPlayerIdentity) {
    if (!updatedPlayerIdentity.getTitlePlayerAccountId().isEmpty())
      this.titlePlayerAccountId = updatedPlayerIdentity.getTitlePlayerAccountId();
    if (updatedPlayerIdentity.getPlatform() != null
        && !updatedPlayerIdentity.getPlatform().isEmpty())
      this.platform = updatedPlayerIdentity.getPlatform();
    if (updatedPlayerIdentity.getPlatformAccountId() != null
        && !updatedPlayerIdentity.getPlatformAccountId().isEmpty())
      this.platformAccountId = updatedPlayerIdentity.getPlatformAccountId();
    if (updatedPlayerIdentity.getName() != null && !updatedPlayerIdentity.getName().isEmpty())
      this.name = updatedPlayerIdentity.getName();
  }

  public String getPlayFabId() {
    return playFabId;
  }

  public String getPlatform() {
    return platform;
  }

  public String getPlatformAccountId() {
    return platformAccountId;
  }

  public String getTitlePlayerAccountId() {
    return titlePlayerAccountId;
  }

  public void setTitlePlayerAccountId(String titlePlayerAccountId) {
    this.titlePlayerAccountId = titlePlayerAccountId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Instant getLastSeenRanked() {
    return lastSeenRanked;
  }

  public void setLastSeenRanked(Instant lastSeenRanked) {
    this.lastSeenRanked = lastSeenRanked;
  }

  public Instant getLastSeenCasual() {
    return lastSeenCasual;
  }

  public void setLastSeenCasual(Instant lastSeenCasual) {
    this.lastSeenCasual = lastSeenCasual;
  }

  public LatestPlayerReport getLatestPlayerReport() {
    return latestPlayerReport;
  }

  public void setLatestPlayerReport(LatestPlayerReport latestPlayerReport) {
    this.latestPlayerReport = latestPlayerReport;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    final PlayerIdentity that = (PlayerIdentity) o;

    return Objects.equals(playFabId, that.getPlayFabId());
  }

  @Override
  public int hashCode() {
    return 31;
  }

  public record PlatformDistributionResult(String platform, Integer playerCount) {}
}
