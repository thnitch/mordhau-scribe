package com.mordhauscribe.scribe.model;

public enum Platform {
  STEAM("Steam"),
  EPIC("Epic");

  private final String name;

  Platform(final String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
