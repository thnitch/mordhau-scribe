package com.mordhauscribe.scribe.model;

import com.playfab.PlayFabClientModels.CollectionFilter;
import com.playfab.PlayFabClientModels.Container_Dictionary_String_String;
import java.util.ArrayList;
import java.util.HashMap;

public class PlayFabCasualGamesFilter extends CollectionFilter {
  public PlayFabCasualGamesFilter() {
    ArrayList<Container_Dictionary_String_String> excludes = new ArrayList<>();

    Container_Dictionary_String_String casualFilterData = new Container_Dictionary_String_String();
    casualFilterData.Data = new HashMap<>();
    casualFilterData.Data.put("Visibility", "1");

    excludes.add(casualFilterData);
    this.Excludes = excludes;
  }
}
