package com.mordhauscribe.scribe.model;

import java.util.Map;

public record TitleData(Map<String, Long> globalBannedPlayers, Map<String, Long> officialBannedPlayers, Map<String, Long> globalMutedPlayers, Map<String, Long> officialMutedPlayers) {
}
