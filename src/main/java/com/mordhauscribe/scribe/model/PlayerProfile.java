package com.mordhauscribe.scribe.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.mordhauscribe.scribe.model.entity.Report;
import java.util.List;
import java.util.Map;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public record PlayerProfile(
    String playFabId,
    String platform,
    String platformAccountId,
    String name,
    String avatarUrl,
    Game game,
    List<Report> playerReports,
    @JsonInclude(Include.NON_NULL) Map<String, Long> casualStats,
    @JsonInclude(Include.NON_NULL) Integer duelLeaderboardPosition,
    @JsonInclude(Include.NON_NULL) Double duelTopXPercent,
    @JsonInclude(Include.NON_NULL) Integer teamfightLeaderboardPosition,
    @JsonInclude(Include.NON_NULL) Double teamfightTopXPercent,
    @JsonInclude(Include.NON_NULL) Map<Long, SnapshotDelta> duelLeaderboardDeltas,
    @JsonInclude(Include.NON_NULL) Map<Long, SnapshotDelta> teamfightLeaderboardDeltas,
    @JsonInclude(Include.NON_NULL) String countryCode,
    @JsonInclude(Include.NON_NULL) Long globalBan,
    @JsonInclude(Include.NON_NULL) Long officialBan,
    @JsonInclude(Include.NON_NULL) Long globalMute,
    @JsonInclude(Include.NON_NULL) Long officialMute) {
}
