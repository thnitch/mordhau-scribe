package com.mordhauscribe.scribe.model;

import com.playfab.PlayFabClientModels.CollectionFilter;
import com.playfab.PlayFabClientModels.Container_Dictionary_String_String;
import java.util.ArrayList;
import java.util.HashMap;

public class PlayFabOfficialGamesFilter extends CollectionFilter {
  public PlayFabOfficialGamesFilter() {
    ArrayList<Container_Dictionary_String_String> includes = new ArrayList<>();
    Container_Dictionary_String_String officialFilterData = new Container_Dictionary_String_String();
    officialFilterData.Data = new HashMap<>();
    officialFilterData.Data.put("IsOfficial", "true");

    includes.add(officialFilterData);
    this.Includes = includes;
  }
}
