package com.mordhauscribe.scribe.model;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LeaderboardEntry {
  private String playFabId;
  private String platform;
  private String platformAccountId;
  private Integer position;
  private Integer mmr;

  private String name;

  private String avatar;

  private String countryCode;

  // TODO: delete
  private PlayerSummary playerSummary;

  @JsonInclude(Include.NON_NULL)
  private Instant timestamp;

  @JsonInclude(value = Include.NON_DEFAULT)
  private boolean banned;

  public LeaderboardEntry(
      final String playFabId,
      final String name,
      final String platform,
      final String platformAccountId,
      final Integer position,
      final Integer mmr,
      final Instant timestamp,
      final boolean banned) {
    this.playFabId = playFabId;
    this.name = name;
    this.platform = platform;
    this.platformAccountId = platformAccountId;
    this.position = position;
    this.mmr = mmr;
    this.timestamp = timestamp;
    this.banned = banned;
  }

  public LeaderboardEntry(
      final String playFabId,
      final String name,
      final String platform,
      final String platformAccountId,
      final Integer position,
      final Integer mmr) {
    this.playFabId = playFabId;
    this.name = name;
    this.platform = platform;
    this.platformAccountId = platformAccountId;
    this.position = position;
    this.mmr = mmr;
  }

  @JsonProperty("PlayFabId")
  public String getPlayFabId() {
    return playFabId;
  }

  public void setPlayFabId(final String playFabId) {
    this.playFabId = playFabId;
  }

  @JsonProperty("Platform")
  public String getPlatform() {
    return platform;
  }

  public void setPlatform(final String platform) {
    this.platform = platform;
  }

  @JsonProperty("PlatformAccountId")
  public String getPlatformAccountId() {
    return platformAccountId;
  }

  public void setPlatformAccountId(final String platformAccountId) {
    this.platformAccountId = platformAccountId;
  }

  @JsonProperty("Position")
  public Integer getPosition() {
    return position;
  }

  public void setPosition(final Integer position) {
    this.position = position;
  }

  @JsonProperty("MMR")
  public Integer getMmr() {
    return mmr;
  }

  public void setMmr(final Integer mmr) {
    this.mmr = mmr;
  }

  @JsonProperty("PlayerSummary")
  public PlayerSummary getPlayerSummary() {
    return playerSummary;
  }

  public void setPlayerSummary(final PlayerSummary playerSummary) {
    this.playerSummary = playerSummary;
  }

  public Instant getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(final Instant timestamp) {
    this.timestamp = timestamp;
  }

  public boolean isBanned() {
    return banned;
  }

  public void setBanned(final boolean banned) {
    this.banned = banned;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }
}
