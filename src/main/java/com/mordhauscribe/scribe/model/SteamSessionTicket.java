package com.mordhauscribe.scribe.model;

import java.time.Instant;

public record SteamSessionTicket(String ticket, Instant timestamp) {
}
