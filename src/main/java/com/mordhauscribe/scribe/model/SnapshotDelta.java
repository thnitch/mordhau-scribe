package com.mordhauscribe.scribe.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public record SnapshotDelta(boolean newEntry, int position, int mmr, int wins, int losses) {
}
