package com.mordhauscribe.scribe.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.lukaspradel.steamapi.data.json.playersummaries.Player;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public record PlayerSummary(
        @JsonIgnore String steamId,
        String name,
        @JsonIgnore String avatar,
        String avatarMedium,
        @JsonIgnore String avatarFull,
        String countryCode) {

  public static PlayerSummary fromSteamPlayerSummary(final Player player) {
    return new PlayerSummary(
            player.getSteamid(),
            player.getPersonaname(),
            player.getAvatar(),
            player.getAvatarmedium(),
            player.getAvatarfull(),
            player.getLoccountrycode());
  }
}
