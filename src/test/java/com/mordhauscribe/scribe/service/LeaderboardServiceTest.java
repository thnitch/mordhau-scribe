//package com.mordhauscribe.scribe.service;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.when;
//
//import com.mordhauscribe.scribe.config.DeltaConfig;
//import com.mordhauscribe.scribe.model.ChangedEntry;
//import com.mordhauscribe.scribe.model.Leaderboard;
//import com.mordhauscribe.scribe.model.LeaderboardEntry;
//import com.mordhauscribe.scribe.model.Outcome;
//import com.mordhauscribe.scribe.model.QueueType;
//import java.time.Instant;
//import java.util.ArrayList;
//import java.util.List;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.context.ApplicationEventPublisher;
//
//@ExtendWith(MockitoExtension.class)
//class LeaderboardServiceTest {
//
//  LeaderboardServiceImpl testling;
//  @Mock PlayFabService playFabService;
//  @Mock ApplicationEventPublisher applicationEventPublisher;
//  @Mock SteamPlayerSummariesService steamPlayerSummariesService;
//  @Mock CurrentGamesService currentGamesService;
//  @Mock QueueStatisticsService queueStatisticsService;
//  @Mock DeltaService deltaService;
//  @Mock ReportUpdateService playerReportService;
//  @Mock PlayerIdentityService playerIdentityService;
//  @Mock ReportService reportService;
//  @Mock TitleDataService titleDataService;
//  @Mock DeltaConfig deltaConfig;
//  private Leaderboard oldBoard;
//  private Leaderboard newBoard;
//
//  @BeforeEach
//  void setUp() {
//    newBoard = generateLeaderboard();
//    oldBoard = generateLeaderboard();
//
//    when(reportService.findLatestTimestamp(any())).thenReturn(Instant.now());
//    testling =
//        new LeaderboardServiceImpl(
//            applicationEventPublisher,
//            playFabService,
//            steamPlayerSummariesService,
//            deltaService,
//            reportService,
//            playerReportService,
//            playerIdentityService,
//            titleDataService,
//            currentGamesService,
//            queueStatisticsService,
//            deltaConfig,
//            true);
//  }
//
//  @Test
//  void determineChanges_withoutChange_empty() {
//    assertTrue(testling.determineChanges(newBoard, oldBoard).isEmpty(), "No change");
//  }
//
//  @Test
//  void determineChanges_withMmrChange_win() {
//    final LeaderboardEntry leaderboardEntry = newBoard.getEntries().get(0);
//    leaderboardEntry.setMmr(leaderboardEntry.getMmr() + 1);
//
//    final List<ChangedEntry> changedEntries = testling.determineChanges(newBoard, oldBoard);
//    assertAll(
//        "One mmr change, win",
//        () -> assertEquals(changedEntries.size(), 1),
//        () ->
//            assertEquals(
//                changedEntries.get(0).getPlayerIdentity().getPlayFabId(),
//                leaderboardEntry.getPlayFabId()),
//        () -> assertEquals(changedEntries.get(0).getOutcome(), Outcome.WIN));
//  }
//
//  @Test
//  void determineChanges_withMmrChange_loss() {
//    final LeaderboardEntry leaderboardEntry = newBoard.getEntries().get(0);
//    leaderboardEntry.setMmr(leaderboardEntry.getMmr() - 1);
//
//    final List<ChangedEntry> changedEntries = testling.determineChanges(newBoard, oldBoard);
//    assertAll(
//        "One mmr change, loss",
//        () -> assertEquals(changedEntries.size(), 1),
//        () ->
//            assertEquals(
//                changedEntries.get(0).getPlayerIdentity().getPlayFabId(),
//                leaderboardEntry.getPlayFabId()),
//        () -> assertEquals(changedEntries.get(0).getOutcome(), Outcome.LOSS));
//  }
//
//  @Test
//  void determineChanges_positionChange_empty() {
//    newBoard.getEntries().get(0).setPosition(1);
//    final List<ChangedEntry> changedEntries = testling.determineChanges(newBoard, oldBoard);
//
//    assertTrue(changedEntries.isEmpty(), "No change");
//  }
//
//  @Test
//  void determineChanges_playerRemoved_undefined() {
//    final LeaderboardEntry leaderboardEntry = newBoard.getEntries().get(0);
//    newBoard.getEntries().remove(leaderboardEntry);
//
//    final List<ChangedEntry> changedEntries = testling.determineChanges(newBoard, oldBoard);
//
//    assertAll(
//        "Player removed, undefined",
//        () -> assertEquals(changedEntries.size(), 1),
//        () ->
//            assertEquals(
//                changedEntries.get(0).getPlayerIdentity().getPlayFabId(),
//                leaderboardEntry.getPlayFabId()),
//        () -> assertEquals(changedEntries.get(0).getOutcome(), Outcome.UNDEFINED));
//  }
//
//  @Test
//  void determineChanges_playerAdded_undefined() {
//    final LeaderboardEntry leaderboardEntry =
//        new LeaderboardEntry("101", "Name", "Steam", "101", 101, 101);
//    newBoard.getEntries().add(leaderboardEntry);
//
//    final List<ChangedEntry> changedEntries = testling.determineChanges(newBoard, oldBoard);
//
//    assertAll(
//        "Player added, undefined",
//        () -> assertEquals(changedEntries.size(), 1),
//        () ->
//            assertEquals(
//                changedEntries.get(0).getPlayerIdentity().getPlayFabId(),
//                leaderboardEntry.getPlayFabId()),
//        () -> assertEquals(changedEntries.get(0).getOutcome(), Outcome.UNDEFINED));
//  }
//
//  private Leaderboard generateLeaderboard() {
//    final List<LeaderboardEntry> entries = new ArrayList<>();
//
//    for (int i = 0; i < 3; i++) {
//      final Integer I = i;
//      final LeaderboardEntry entry =
//          new LeaderboardEntry(I.toString(), "Name", "Steam", I.toString(), I, i);
//      entries.add(entry);
//    }
//
//    return new Leaderboard(entries, Instant.now(), QueueType.SOLO);
//  }
//}
